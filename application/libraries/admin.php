<?php

class Admin{
    
    const SYS_ADMIN_CONFIG      = 'SYS_ADMIN_CONFIG';
    const FLASH_MESSAGE_ALERT   = 'FLASH_MESSAGE_ALERT';

    /**
     *
     * @var CI_Controller 
     */
    public $controller          = null;
    private $data               = Array();
    private $breadcrumb         = Array();

    public function __construct( $controller=null ) {
        $this->controller       = $controller;
        //$this->controller->uri->segment(1);
    }


    public function view($view='', $data=Array()){
        $this->getTemplate($view, $data);
    }
    
    
    public function url($uri=''){
        return base_url().'admin/'.$uri;
    }
    
    
    public function loginView($data=array()){
        $this->controller->load->view('admin/login', $data);
    }

    public function getView($view, $data=array()){
        $this->controller->load->view('admin/view/'.$view, $data);
    }

    private function getTemplate($view_body='', $data=Array()){
        $this->controller->load->model('admin/setting_model');
        $this->data             = $data;
        $this->data['db']       = $this->controller->db;
        ob_start();
        $this->getView($view_body, $this->data);
        $view_body              = ob_get_clean();
        $this->controller->load->view(  'admin/template',
                                        Array(
                                            'controller'        => &$this->controller,
                                            'setting_model'     => &$this->controller->setting_model,
                                            'navBarHeader'      => $this->getNavBarHeader(),
                                            'navBarTool'        => $this->getNavBarTool(),
                                            'leftMenu'          => $this->getLeftMenu(),
                                            'breadcrumb'        => $this->getBreadCrumbTemplate(),
                                            'body'              => $view_body,
                                            'footer'            => $this->footer()
                                        ));
    }
    
    private function getNavBarTool(){
        ob_start();
        $this->controller->load->view(  'admin/navBarTool', $this->data);
        return ob_get_clean();
    }
    
    private function getNavBarHeader(){
        ob_start();
        $this->controller->load->view(  'admin/navBarHeader', $this->data);
        return ob_get_clean();
    }    

    private function getLeftMenu(){
        $this->data['uri']          = $this->controller->uri;
        ob_start();
        $this->controller->load->view(  'admin/leftMenu', $this->data);
        return ob_get_clean();
    }
    
    private function getBreadCrumbTemplate(){
        $this->data['breadcrumb']   = $this->getBreadcrumb();
        
        ob_start();
        $this->controller->load->view(  'admin/breadcrumb', $this->data);
        return ob_get_clean();
    }
    
    private function footer(){
        ob_start();
        $this->controller->load->view(  'admin/footer', $this->data);
        return ob_get_clean();
    }
    
    
    
    public function checkLogin(){
        if( !$this->controller->session->userdata('id')  || !$this->controller->session->userdata('username') ){
            return false;
        }
        return true;
    }
    
    
    public function getConfig(){
        if( !@$_ENV[Admin::SYS_ADMIN_CONFIG] ){
            $sysConfig = $this->controller->db->query('SELECT * FROM system_configuration LIMIT 1')->result();
            $_ENV[Admin::SYS_ADMIN_CONFIG] = &$sysConfig[0];
        }
        return $_ENV[Admin::SYS_ADMIN_CONFIG];
    }
    
    
    
    
    public function addBreadcrumb($name, $url){
        $this->breadcrumb[$name]        = $url;
    }
    public function getBreadcrumb($name=''){
        if( $name ){
            return $this->breadcrumb[$name];
        }else{
            return $this->breadcrumb;
        }
    }
    
    
    
    
    /**
     * 
     * @return boolean
     */
    public function isSiteRefered(){
        if( strstr($this->controller->agent->referrer(), base_url()) ){
            return true;
        }
        return false;
    }
    
    
    /**
     * 
     * @param String $filepath
     * @param array $data
     * @return String HTML
     */
    public function getViewHtml($filepath, $data = array()){
        ob_start();
        $this->controller->load->view( $filepath, $data);
        return ob_get_clean();
    }
    
    
    public function setFlashMessageAlert($message){
        $this->controller->session->set_flashdata(
                Admin::FLASH_MESSAGE_ALERT, 
                $message
            );
    }
    
    public function getFlashMessageAlert(){
        return $this->controller->session->flashdata(Admin::FLASH_MESSAGE_ALERT);
    }
    
    public function getAlertView_success($message){
        $data = array(
            'type' => 'success',
            'heading' => 'Success!',
            'message' => &$message
        );
        return $this->getViewHtml('admin/view/message_alert', $data);
    }
    
    public function getAlertView_error($message){
        $data = array(
            'type' => 'danger',
            'heading' => 'Error!',
            'message' => &$message
        );
        return $this->getViewHtml('admin/view/message_alert', $data);
    }
    
    public function getAlertView_warning($message){
        $data = array(
            'type' => 'warning',
            'heading' => 'Warning!',
            'message' => &$message
        );
        return $this->getViewHtml('admin/view/message_alert', $data);
    }
    
    public function getAlertView_info($message){
        $data = array(
            'type' => 'info',
            'heading' => 'Info!',
            'message' => &$message
        );
        return $this->getViewHtml('admin/view/message_alert', $data);
    }

}


/**
 * 
 * @return \Admin
 */
function getAdmin( $CI_INSTANCE=null ){
    if( !@$_ENV['SYSTEM_ADMIN_INSTANCE'] ){
        $_ENV['SYSTEM_ADMIN_INSTANCE'] = new Admin( $CI_INSTANCE ? $CI_INSTANCE : get_instance() );
    }
    return $_ENV['SYSTEM_ADMIN_INSTANCE'];
}