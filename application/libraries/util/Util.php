<?php
    class Util{        
        public function compareDate($date,$age){
            $age = intval($age);
            $age = intval($age*86400);
            $datenow = strtotime("now");
            $datepost = strtotime($date);
            $compair = intval($datenow - $datepost);
            $result = ($compair < $age || $datenow<$datepost )?true:false;
            return $result;
        }
        public function getNewImage($create_date, $comparedDay=15){
            if($this->compareDate($create_date, $comparedDay)){
                return '<img src="'.base_url().'images/new.gif" />';
            }
        }
        
        
        public function eventDisplayDate($startDate,$endDate)
	{
		$m = array("","มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
		$startDateArr=split('-',$startDate,3);
		$endDateArr=split('-',$endDate,3);
		$startmonth = $m[intval($startDateArr[1])];
		$endmonth = $m[intval($endDateArr[1])];
		$startyear = intval($startDateArr[0]+543);
		$endyear = intval($endDateArr[0]+543);
		$startyear = substr($startyear,2,2);
		$endyear = substr($endyear,2,2);
		$startDateArr[2] = intval($startDateArr[2]);
		$endDateArr[2] = intval($endDateArr[2]);
		if($startDateArr[0]<>$endDateArr[0]){
			return "{$startDateArr[2]} {$startmonth} {$startyear} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[1]<>$endDateArr[1]){
			return "{$startDateArr[2]} {$startmonth} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[2]<>$endDateArr[2]){
			return "{$startDateArr[2]} - {$endDateArr[2]} {$endmonth} {$endyear}";
		}
		if($startDateArr[2]==$endDateArr[2]){
			return "{$startDateArr[2]} {$startmonth} {$startyear}";
		}
		return "Invalid date input";
	}
	
	
	public function eventDisplayDate_eng($startDate,$endDate)
	{
		$m = array("","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$startDateArr=split('-',$startDate,3);
		$endDateArr=split('-',$endDate,3);
		$startmonth = $m[intval($startDateArr[1])];
		$endmonth = $m[intval($endDateArr[1])];
		$startyear = intval($startDateArr[0]);
		$endyear = intval($endDateArr[0]);
		$startyear = substr($startyear,2,2);
		$endyear = substr($endyear,2,2);
		$startDateArr[2] = intval($startDateArr[2]);
		$endDateArr[2] = intval($endDateArr[2]);
		
		if($startDateArr[0]<>$endDateArr[0]){
			return "{$startmonth} {$startDateArr[2]}, {$startyear} - {$endmonth} {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[1]<>$endDateArr[1]){
			return "{$startmonth} {$startDateArr[2]} - {$endmonth} {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[2]<>$endDateArr[2]){
			return "{$endmonth} {$startDateArr[2]} - {$endDateArr[2]}, {$endyear}";
		}
		if($startDateArr[2]==$endDateArr[2]){
			return "{$startmonth} {$startDateArr[2]}, {$startyear}";
		}
		return "Invalid date input";
	}
        
        
    }
    
    
    /**
     * @return Util 
     */
    function getUtilObject(){
        if(!isset($_ENV['system-util'])){
            $obj                    = new Util();
            $_ENV['system-util']    = &$obj;
        }
        return $_ENV['system-util'];
    }
	
	
	
function get_subString($subject,$max_length=false)
{
	$subject = strip_tags($subject);			
	
	if($max_length){
		if(iconv_strlen($subject) > $max_length){
					$subject = iconv_substr($subject,0,$max_length,"UTF-8")."...";
	}else{
		$subject=$subject;
	  }
	}	
		
	return $subject;

}
    
    
    /**
     *
     * @param DATETIME $date
     * @param Int $age
     * @return String 
     */
    function compareDate($date,$age){
        $obj                    = getUtilObject();
        return $obj->compareDate($date,$age);
    }
    
    
    /**
     *
     * @param DATETIME $create_date
     * @param int $comparedDay
     * @return String HTML
     * @example <img src="news.gif" /> 
     */
    function getNewImage($create_date, $comparedDay=15){
        $obj                    = getUtilObject();
        return $obj->getNewImage($create_date, $comparedDay);
    }
    
    
    function getExtImage($extension){
        return base_url().'images/fileicons_16/'.$extension.'.png';
    }
	
 
	 // create date 08-07-56
	function strsub_message($str,$length){
		$str = strip_tags($str);

		$l = strlen($str);
		$len = 0;
		
		for ($i = 0; $i < $l; ++$i)
			if ((ord($str[$i]) & 0xC0) != 0x80) ++$len;

				if($len > $length){
				// echo $str;
					$str = iconv_substr($str,0,$length,"UTF-8")."...";
				// $str = substr($str,0,$length)."...";
				}
		return $str;
}

    
    
    
    function eventDisplayDate($startDate,$endDate){
        $obj                    = getUtilObject();
        return $obj->eventDisplayDate($startDate,$endDate);
    }
    
    
    
    function eventDisplayDate_eng($startDate,$endDate){
        $obj                    = getUtilObject();
        return $obj->eventDisplayDate_eng($startDate,$endDate);
    }
    
    
    
    function utf8_substr($str,$start_p,$len_p) {
        if(strlen($str) < $len_p){
            return $str;	
        }
        preg_match_all("/./u", $str, $ar); 
        if(func_num_args() >= 3) { 
            $end = func_get_arg(2); 
            return join("",array_slice($ar[0],$start_p,$len_p)) . "..."; 
        } else { 
            return join("",array_slice($ar[0],$start_p)) . "..."; 
        } 
    } 
    
    
    
    function substr_utf8( $str, $start_p , $len_p){
        $ret =  preg_replace(   '#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start_p.'}'.
                                '((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len_p.'}).*#s',
                                '$1' , 
                                $str );
        if(strlen($str) > $len_p){
                $ret .= "...";
        }
        return $ret;
    };
    
    
    
    function getQueryString(){
        return $_SERVER['QUERY_STRING'];
    }
    
    
    
    /**
     *
     * @param String $haystack
     * @param Array $replacementArray 
     * @return String
     */
    function getPrepareText( $haystack, $replacementArray=Array() ){
        $len                = count($replacementArray);
        $key                = array_keys($replacementArray);
        for( $i=0;  $i<$len;  $i++ ){
            $haystack       = str_replace("@{$key[$i]}", $replacementArray[$key[$i]], $haystack);
        }
        return $haystack;
    }
    
    
    function is_iOS(){
        $userAgent                          = strtolower($_SERVER['HTTP_USER_AGENT']);
        if(strstr($userAgent, 'iphone') || strstr($userAgent, 'ipad') || strstr($userAgent, 'ipod') ){
            return true;
        }
        return false;
    }

	
    function showCategoryName(&$id){
        $id					= explode("-", $id);
        $tableName				= $id[1];
        $id                                 = $id[0];
        $db					= getDBO();
        $db->setQuery(" SELECT cate_name FROM {$tableName} WHERE id='{$id}' ");
        $rs					= $db->loadAssocList();
        $id					= @$rs[0]['cate_name']?$rs[0]['cate_name']:'-';
    }

    function countContentViaCategory($cateId, $tblName){
            $db													= getDBO();
            $db->setQuery(" SELECT count(id) AS total FROM {$tblName} WHERE status='1' AND cid='{$cateId}'  ");
            $rs													= $db->loadAssocList();
            return $rs[0]['total'];
    }


    function getAllCategory( $currentId, $tableName, $currentParentId=-1, $cateId=-1){        
        $db					= getDBO();
        $db->setQuery("SELECT id FROM  {$tableName} WHERE status='1' AND parent_id='{$currentId}' ORDER BY id DESC ");
        $rs					= $db->loadAssocList();
        $len					= count($rs);
        if(count($len)){
            $output				= Array();
            $store				= Array();
            for( $i=0;  $i<$len;  $i++ ){
                    $store[]			= Array(
                            'id'	=> $rs[$i]['id'],
                            'level'	=> 1
                    );
            }
            while( count($store)>0 ){
                    $buf				= array_pop($store);
                    $output[]		= $buf;
                    $db->setQuery("SELECT id FROM  {$tableName} WHERE status='1' AND parent_id='{$buf['id']}'   ORDER BY id DESC ");
                    $rs					= $db->loadAssocList();
                    $len					= count($rs);
                    for( $i=0;   $i<$len;  $i++ ){
                            $row			= Array(
                                    'id'	=> $rs[$i]['id'],
                                    'level'	=> ($buf['level']+1)
                            );
                            array_push($store, $row );
                    }
            }
            $html				= "";
            $len				= count($output);
            $html				.='
                    <select name="parent_id" id="parent_id" style="min-width:300px;" class="required">
                            <option  value="">'.lang('select category', 'select category').'</option>
            ';
            for(  $i=0;   $i<$len;   $i++  ){
                $db->setQuery("SELECT id, cate_name FROM {$tableName}  WHERE id='{$output[$i]['id']}' ");
                $rs				= $db->loadAssocList();
                $rs				= $rs[0];
                $tab				= '';
                for($loop=0;$loop<($output[$i]['level']-1);$loop++){$tab.='&mdash;';}
                $selected			= $rs['id']==$currentParentId ? ' selected="selected" ' : '' ;
                $disabled			= $rs['id']==$cateId ? ' disabled="disabled" ' : '' ;
                if($rs['cate_name']=='main'){
                    $rs['cate_name']            = 'หมวดหลัก';
                }
                $html					.='
                        <option value="'.$rs['id'].'" '.$selected.' '.$disabled.'  >&nbsp;'.$tab.' '.$rs['cate_name'].'</option>
                ';		
            }
            return $html;
        }else{
            return "";
        }
    }



    function getAllCID( $currentId, $tableName, $cateId=-1){        
        return str_replace('parent_id', 'cid', getAllCategory($currentId, $tableName, $cateId));
    }





    /* Webboard  */
    function getSubDetail(&$detail){
        $detail                 = utf8_substr(strip_tags($detail), 0, 60);
    }

    
    
        
    
    function getMonthName(&$index, $lang='th'){
        $month 			= array(    0	=>	array(	"th"	=> 	"มกราคม",
                                                                "en"	=> 	"January"),
                                            1	=>	array(	"th"    =>	"กุมภาพันธ์"	,
                                                                "en"	=>	"February"),
                                            2	=>	array(	"th"	=>	"มีนาคม",
                                                                "en"	=>	"March"),
                                            3	=>	array(	"th"	=>	"เมษายน",
                                                                "en"	=>	"April"),
                                            4	=>	array(	"th"	=>	"พฤษภาคม",
                                                                "en"	=>	"May"),
                                            5	=>	array(	"th"	=>	"มิถุนายน",
                                                                "en"	=>	"June"),
                                            6	=>	array(	"th"	=>	"กรกฎาคม",
                                                                "en"	=>	"July"),
                                            7	=>	array(	"th"	=>	"สิงหาคม",
                                                                "en"	=>	"August"),
                                            8	=>	array(	"th"	=>	"กันยายน",
                                                                "en"	=>	"September"),
                                            9	=>	array(	"th"	=>	"ตุลาคม",
                                                                "en"	=>	"October"),
                                            10	=>	array(	"th"	=>	"พฤศจิกายน",
                                                                "en"	=>	"November"),
                                            11	=>	array(	"th"	=>	"ธันวาคม",
                                                                "en"	=>	"December")
                                            );
        $index =   $month[$index-1][strtolower($lang)];
    }

    
    function getRecommend(&$val){
        if($val==1){
            $val        = '<span style="color:blue">ข่าวเด่น</span>';
        }else{
            $val        = '-';
        }
    }
    
    
    
    function dumpAll($value='', $file='output.txt'){
        $f              = fopen($file, 'w');
        ob_start();
        echo "VALUE";
        echo "\n";
        var_dump($value);
        echo "REQUEST";
        echo "\n";
        var_dump($_REQUEST);
        echo "FILES";
        echo "\n";
        var_dump($_FILES);
        echo "SERVER";
        echo "\n";
        var_dump($_SERVER);
        $output         = ob_get_clean();
        fwrite($f, $output);
        fclose($f);
    }
    
    
    
    
    function setPicContent($number){
        @$_ENV['system-pic-content']            = $number;
    }
    function getPicContent(){
        return @$_ENV['system-pic-content'];
    }
    
	// crate date 08-07-56
	function get_local_name($local_id){
         $db                 = getDBO();
         $query = "
             SELECT     *
             FROM       local_tambon
             WHERE      tambon_id = '{$local_id}'
        ";
        $db->setQuery($query);
        $rs = $db->loadAssocList();
        return $rs[0]['tambon_address']."/";
     }
    
  
    function checkSpamText($text){
        if( !@$_ENV['system-validate-spam'] ){
            $db                 = getDBO();
            $db->setQuery(" SELECT          * 
                            FROM            cpanel_spam_filter 
                            WHERE           checked='1'
                            ");
            $filter             = $db->loadAssocList();
            $word               = Array();
            if( $filter ){
                foreach($filter AS      $v){
                    $v['subject']           = trim($v['subject']);
                    $word[]         = Array(
                        'subject'           => $v['subject'],
                        'found'             => $v['found'],
                        'insensitive'       => $v['insensitive'],
                        'check_by_detail'   => $v['check_by_detail'],
                        'check_by_name'     => $v['check_by_name']
                    );
                }
                $_ENV['system-validate-spam']   = &$word;
            }
        }
        $spam                   = $_ENV['system-validate-spam'];
        $len                    = count($spam);
        if( $len>0 ){
            foreach( $spam  AS      $v){
                $word           = $v['subject'];
                $found          = '';
                if( $v['insensitive'] ){
                    $found      = @stristr($text, $word);
                }else{
                    $found      = @strstr($text, $word);
                }
                if($found){
                    return true;
                }
                
            }
        }
        return false;
    }
    
    
    
    
    
    if(!function_exists('date_diff')) {
        class DateInterval {
          public $y;
          public $m;
          public $d;
          public $h;
          public $i;
          public $s;
          public $invert;
          public $days;

          public function format($format) {
            $format = str_replace('%R%y', 
              ($this->invert ? '-' : '+') . $this->y, $format);
            $format = str_replace('%R%m', 
               ($this->invert ? '-' : '+') . $this->m, $format);
            $format = str_replace('%R%d', 
               ($this->invert ? '-' : '+') . $this->d, $format);
            $format = str_replace('%R%h', 
               ($this->invert ? '-' : '+') . $this->h, $format);
            $format = str_replace('%R%i', 
               ($this->invert ? '-' : '+') . $this->i, $format);
            $format = str_replace('%R%s', 
               ($this->invert ? '-' : '+') . $this->s, $format);

            $format = str_replace('%y', $this->y, $format);
            $format = str_replace('%m', $this->m, $format);
            $format = str_replace('%d', $this->d, $format);
            $format = str_replace('%h', $this->h, $format);
            $format = str_replace('%i', $this->i, $format);
            $format = str_replace('%s', $this->s, $format);

            return $format;
          }
        }

        function date_diff(DateTime $date1, DateTime $date2) {

          $diff = new DateInterval();

          if($date1 > $date2) {
            $tmp = $date1;
            $date1 = $date2;
            $date2 = $tmp;
            $diff->invert = 1;
          } else {
            $diff->invert = 0;
          }

          $diff->y = ((int) $date2->format('Y')) - ((int) $date1->format('Y'));
          $diff->m = ((int) $date2->format('n')) - ((int) $date1->format('n'));
          if($diff->m < 0) {
            $diff->y -= 1;
            $diff->m = $diff->m + 12;
          }
          $diff->d = ((int) $date2->format('j')) - ((int) $date1->format('j'));
          if($diff->d < 0) {
            $diff->m -= 1;
            $diff->d = $diff->d + ((int) $date1->format('t'));
          }
          $diff->h = ((int) $date2->format('G')) - ((int) $date1->format('G'));
          if($diff->h < 0) {
            $diff->d -= 1;
            $diff->h = $diff->h + 24;
          }
          $diff->i = ((int) $date2->format('i')) - ((int) $date1->format('i'));
          if($diff->i < 0) {
            $diff->h -= 1;
            $diff->i = $diff->i + 60;
          }
          $diff->s = ((int) $date2->format('s')) - ((int) $date1->format('s'));
          if($diff->s < 0) {
            $diff->i -= 1;
            $diff->s = $diff->s + 60;
          }

          $start_ts   = $date1->format('U');
          $end_ts   = $date2->format('U');
          $days     = $end_ts - $start_ts;
          $diff->days  = round($days / 86400);

          if (($diff->h > 0 || $diff->i > 0 || $diff->s > 0))
            $diff->days += ((bool) $diff->invert)
              ? 1
              : -1;

          return $diff;

        }

      }
    
      
      
    function loadController($controller, $defineControlleName=false){
        error_reporting(E_ALL);
        $CI                 = &get_instance();
        $filePath           = APPPATH . 'controllers/' . $controller . '.php';
        if( !@$CI->{$controller}  && file_exists($filePath) ){
            require_once( $filePath);
            $ctrler         = new $controller();
            if( $defineControlleName ){
                $controller         = $defineControlleName;
                $CI->{$controller}  = &$ctrler;
            }else{
                $CI->{$controller}  = &$ctrler;
            }
            return $CI->{$controller};
        }
        return null;
    }
      
    
    
    
    
    function isVDOFromYoutube($link){
        if(strstr(trim($link), "youtu") ){
            return true;
        }
        return false;
    }
    
    
    
    function isVDOFromVimeo($link){
        if(strstr(trim($link), "vimeo") ){
            return true;
        }
        return false;
    }
    
    
    function getVDOIdFromYoutube($link){
        /*
        $output = explode("/", $link);
        $id = @$output[count($output)-1];
        if(strstr($id, "v=") ){
            $id = explode('v=', $id);
            $id = @$id[1];
            if(strstr($id, '&') ){
                $id  = explode('&', $id);
                $id  = $index[0];
            }
        }
        return $id;
         * 
         */
        $output = explode('?', $link);
        if(count($output) > 1){
            $urls = explode('&', $output[1]);
            foreach($urls AS $url){
                if(substr($url, 0, 2) == 'v='){
                    $youtubeId = str_replace('v=', '', $url);
                    return $youtubeId;
                }
            }
        }
        return '';
    }
    
    
    function getYoutubeEmbeded($id, $width, $height){
        return '<iframe width="'.$width.'" height="'.$height.'" src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
    }
    
    
    function getVDOIdFromVimeo($link){
        $output = explode("/", $link);
        $id = @$output[count($output)-1];
        return $id;
    }
    
    function getVimeoEmbeded($id, $width, $height){
        return '<iframe src="https://player.vimeo.com/video/'.$id.'" '
                . 'width="'.$width.'" height="'.$height.'" '
                . 'frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>'
                . '</iframe> ';
    }
    
    
    
    function getYoutubeThumbnail($id, $size=0){
        return "http://img.youtube.com/vi/{$id}/{$size}.jpg";
    }
    
    
    
    define("VIMEO_THUMBNAIL_SIZE_SMALL", 'thumbnail_small');
    define("VIMEO_THUMBNAIL_SIZE_MEDIUM", 'thumbnail_medium');
    define("VIMEO_THUMBNAIL_SIZE_LARGE", 'thumbnail_large');
    function getVimeoThumbnail($id, $size=VIMEO_THUMBNAIL_SIZE_MEDIUM){
//        $hash = @unserialize(file_get_contents("http://vimeo.com/api/v2/video/{$id}.php"));
//        $thumbnail = $hash[0][$size];
        
        $ch = curl_init("http://vimeo.com/api/v2/video/{$id}.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $xml_raw = curl_exec($ch);
        curl_close($ch);
        $hash       = @unserialize(simplexml_load_string($xml_raw));
        $thumbnail = $hash[0][$size];
        
        return $thumbnail;
    }
    
    
    function isTeamLoggedIn() {
        $CI     = &get_instance();
        $teamLoggedInId = $CI->session->userdata(KEY_TEAM_LOGGED_IN);
        if (!$teamLoggedInId || !is_numeric($teamLoggedInId)) {
            return false;
        }
        return true;
    }
    
    function getThaiDayOfWeekByEng($dayName) {
        switch(strtolower($dayName)) {
            case 'mon': 
                return 'จ';
            case 'tue': 
                return 'อ';
            case 'wed': 
                return 'พ';
            case 'thu': 
                return 'พฤ';
            case 'fri': 
                return 'ศ';
            case 'sat': 
                return 'ส';
            case 'sun': 
                return 'อ';
            case 'sunday': 
                return 'อาทิตย์';
            case 'monday': 
                return 'จันทร์';
            case 'tuesday': 
                return 'อังคาร';
            case 'wednesday': 
                return 'พุธ';
            case 'thursday': 
                return 'พฤหัส';
            case 'friday': 
                return 'ศุกร์';
            case 'saturday': 
                return 'เสาร์';
        }
        return $dayName;
    }
    
    function getYoutubeIdFromUrl($url){
        $youtubeId = '';
        $youtubeLink = trim($url);
        if( $youtubeLink ){
            $youtubeUrls = explode('?', $youtubeLink);
            if(count($youtubeUrls)>1){
               $youtubeUrls = explode('&', $youtubeUrls[1]);
               foreach($youtubeUrls AS $youtubeUrl){
                   if(substr($youtubeUrl, 0, 2)=='v='){
                       $youtubeId = str_replace('v=', '', $youtubeUrl);
                       return $youtubeId;
                   }
               }
            }
        }

        return $youtubeId;
    }
    
    function currentUrl(){
        $CI =& get_instance();
        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
    }
?>