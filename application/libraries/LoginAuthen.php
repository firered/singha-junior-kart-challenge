<?php
class LoginAuthen {
    
    
    const COOKIE_NAME = "NGUDNGID_WEBLOGIN_USR_ID";
    const COOKIE_EXPIRE_DATE = 2592000; // 30 days
    public $CI;
    
    public function __construct() {
        $this->CI                 = &get_instance();
    }
    
    
    public function login($userId){
        $cookie_name = LoginAuthen::COOKIE_NAME;
        $cookie_value = "{$userId}";
        setcookie($cookie_name, $cookie_value, time() + LoginAuthen::COOKIE_EXPIRE_DATE, "/");
        
        $this->CI->db->set("lastlogin_date", date("Y-m-d H:i:s"));
        $this->CI->db->set("lastlogin_ip", $this->CI->input->ip_address());
        $this->CI->db->where("id", $userId);
        $this->CI->db->update("users");
    }
    
    
    
    public function getUserIdByCookie(){
        return @$_COOKIE[LoginAuthen::COOKIE_NAME];
    }
    
    
    public function logout(){
        $cookie_name = LoginAuthen::COOKIE_NAME;
        $cookie_value = null;
        setcookie($cookie_name, $cookie_value, time() + LoginAuthen::COOKIE_EXPIRE_DATE, "/");
    }
    
    
    
    
    
    public function checkUser($username, $password){
        $this->CI->db->select('*');
        $this->CI->db->where("username", $username);
        $this->CI->db->where("password", md5($password));
        $rs = $this->CI->db->get("users")->result_array();
        return $rs ? $rs[0]['id'] : false;
    }
    
    
    
    public function getUserDataById($userId){
        $this->CI->db->select("*");
        $this->CI->db->where("id", $userId);
        return $this->CI->db->get("users")->row_array();
    }
    
    
}





/**
 * 
 * @return \LoginAuthen
 */
function getLoginAuthenObj(){
    if( !@$_SESSION['AUTHEN_OBJ'] ){
        $_SESSION['AUTHEN_OBJ'] = new LoginAuthen();
    }
    return $_SESSION['AUTHEN_OBJ'];
}



function isLogedIn(){
    $authen = getLoginAuthenObj();
    return $authen->getUserIdByCookie() ? true : false;
}


function getLogedInUserIdByCookie(){
    $authen = getLoginAuthenObj();
    return $authen->getUserIdByCookie();
}



function getUserDataByCookie(){
    $authen = getLoginAuthenObj();
    $userId = getLogedInUserIdByCookie();
    return $authen->getUserDataById($userId);
}



function logInByCurrentUserId(){
    $authen = getLoginAuthenObj();
    if( $authen->getUserIdByCookie() ){
        $authen->login($authen->getUserIdByCookie());
    }
}



