<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;

$whitelist = array(
    '127.0.0.1',
    '::1',
    'localhost',
    '192.168.10.16',
    '192.168.43.24'
);
// REMOTE_ADDR
if(in_array($_SERVER['SERVER_ADDR'], $whitelist)){
    $db['default']['hostname'] = 'localhost';
    $db['default']['username'] = 'root';
    $db['default']['password'] = '';
    $db['default']['database'] = 'contango_juniorkart';
} else {
//    $db['default']['hostname']  = 'localhost';
//    $db['default']['username']  = '';
//    $db['default']['password']  = '';
//    $db['default']['database']  = 'contango_juniorkart';
} 


$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;





//'email'    => 'admin@singhajuniorkartchallenge.com',
//'password' => 'F3VEBYaE',

/* End of file database.php */
/* Location: ./application/config/database.php */