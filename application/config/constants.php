<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',                            'rb');
define('FOPEN_READ_WRITE',			'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',	'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',			'ab');
define('FOPEN_READ_WRITE_CREATE',		'a+b');
define('FOPEN_WRITE_CREATE_STRICT',		'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',	'x+b');

define('USER_LOGIN_HOME_ID', 'user_login_home_id');
define("RECAPTCHA_SITE_KEY", "");
define("RECAPTCHA_SECRET_KEY", "");

define("FACEBOOK_APP_ID", "189642621410210");

define('EMAIL_USER', '');
define('EMAIL_PASSWORD', '');

define('KEY_TEAM_LOGGED_IN', 'team_log_id');
define('MAX_IMAGE_UPLOAD_SIZE_MB', 2);
define('FACEBOOK_CHAT_PAGE_ID', '');

define('MAX_FILE_UPLOAD_SIZE', 1024*1024*50); // byte

// GMAIL account => leo.fan.league.1@gmail.com: leo.fan.league.1%#


/* End of file constants.php */
/* Location: ./application/config/constants.php */