<div id="body" style="min-height: 80vh;">
    <div class="container">
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        <div class="row">
            <div class="col-12 text-left"
                 style="padding-left: 10px;">
                <?php
                    $method = $this->uri->segment(2, 'photo');
                ?>
                <a href="<?php echo site_url('gallery/photo');?>" style="margin: 5px;">
                    <img src="assets/images/gallery/<?php echo $method == 'photo' ? 'photo_active' : 'photo_inactive' ;?>.png" />
                </a>
                &nbsp;&nbsp;
                <img src="assets/images/gallery/separator.png"  style="margin: 5px;" />
                &nbsp;&nbsp;
                <a href="<?php echo site_url('gallery/vdo');?>" style="margin: 5px;">
                    <img src="assets/images/gallery/<?php echo $method == 'vdo' ? 'vdo_active' : 'vdo_inactive' ;?>.png" />
                </a>
            </div>
        </div>
        <?php
            switch (strtolower($method)) {
                case 'vdo': 
                    $this->load->view('gallery/view_vdo');
                    break;
                case 'photo':
                default :
                    $this->load->view('gallery/view_photo');
                    break;
            }
        ?>
    </div>
</div>


<style>
    .select-custom{
        background: url(assets/images/gallery/year_bg.png) no-repeat;
        width: 198px;
        justify-content: center;
        display: inline-flex;
        height: 50px;
    }
    .year-select{
        font-family: 'Cooper Std';
        padding-left: 30px;
        background: url(assets/images/gallery/year_bg.png) no-repeat;
        width: 198px;
        justify-content: center;
        display: inline-flex;
        height: 50px;
        
/*        -webkit-text-stroke: 1px #770303;
        paint-order: stroke fill;*/
        
        border: none;
    }
    .select-custom .filter {
        background: none;
        width: auto;
        border: none;
        font-size: 20px;
        outline: none;
        text-overflow: ellipsis;
        font-family: 'db_heaventbold';
        -webkit-text-stroke: 1px #770303;
        paint-order: stroke fill;
        color: white;
        align-self: center;
        text-decoration: none;
    }
    .select-custom:hover::before {
        color: rgba(255, 255, 255, 0.6);
        background-color: rgba(255, 255, 255, 0.2);
    }
    .select-custom select option {
        padding: 30px;
    }
</style>



<script>
    $(function(){
        $('.year-select').change(function() {
            window.location = '<?php echo current_url()?>?year='+ this.value;
        });
    });
</script>




