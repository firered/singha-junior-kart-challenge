<div class="row">
    <div class="col-12 text-right">
        <?php
            $photoYears = $this->main_model->getVDOYears();
        ?>
        <select class="year-select">
            <option value="0">FILTER : All</option>
            <?php
                foreach ($photoYears AS $year) {
                ?>
                    <option value="<?php echo $year;?>" <?php echo @$_REQUEST['year']==$year?'selected="selected"':''; ?>>FILTER : <?php echo $year;?></option>
                <?php
                }
            ?>
        </select>
        <p>&nbsp;</p>
    </div>
</div>


<div class="row">
    <div id="_owl-itemList">
        <?php
            $limit = 9;
            $start = @$_REQUEST['page'] ? ($_REQUEST['page']-1)*$limit : 0;
            $exceptId = 0;
            $currentYear = @$_REQUEST['year'] ? $_REQUEST['year'] : 0;
            $photos = $this->main_model->getVDOList($exceptId, $currentYear, $start, $limit);
            for($i = 0; $i<count($photos); $i++) {
                $photo = $photos[$i];
                shortThaiDate($photo['create_date']);
            ?>
                <div class="col-md-4">
                    <div class="item boxItem">
                        <div class="wrapBox">
                            <div class="bgBorderBox">
                                <div class="borderBox">
                                    <div class="borderInsetBox">
                                        <div class="content">
                                            <a href="<?php echo site_url("gallery/vdoDetailAjax/{$photo['id']}"); ?>"
                                               class="">
                                                <img src="<?php echo base_url($photo['display_image']); ?>" class="img-responsive">
                                                <div class="b_content">
                                                    <h3><?php echo $photo['subject']; ?></h3>
                                                    <label><?php echo $photo['create_date']; ?></label>
                                                    <em>see more</em>
                                                </div>
                                            </a>
                                            <a class="dimm modalButton"
                                                data-toggle="modal"
                                               data-src="<?php echo site_url("gallery/vdoDetailAjax/{$photo['id']}"); ?>" 
                                                data-height='1500' data-width='100%'
                                                data-target="#myModal">
                                                <img src="assets/images/vdo_play_btn.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                </div>
            <?php
            }
        ?>
    </div>
</div>

<p>&nbsp;</p>
<?php
    $year = @$_REQUEST['year'];
    $currentPage = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
    $total = $this->main_model->countVDO(0, $year);
    $totalPages = ceil($total / $limit);
?>
<div class="row">
    <div class="col-12 pagination"
         style="justify-content: center;
                display: flex;">
        <?php
            if ($currentPage > 1) {
                $nextPage = $currentPage-1;
            ?>
                <a href="<?php echo site_url("gallery/vdo?year={$year}&page={$nextPage}");?>"><img src="assets/images/pagination-back.png" /></a>
            <?php
            }
        ?>
        <ul class="breadcrumb">
        <?php
            for ($i=0;  $i<$totalPages;  $i++) {
                $pageNo = $i+1;
            ?>
                <li class="breadcrumb-item <?php echo ($currentPage==$pageNo) ? 'active' : '';?>">
                    <a href="<?php echo site_url("gallery/vdo?year={$year}&page={$pageNo}");?>"><?php echo $i+1;?></a>
                </li>
            <?php
            }
        ?>
        </ul>
        <?php
            if ($currentPage < $totalPages) {
                $nextPage = $currentPage+1;
            ?>
                <a href="<?php echo site_url("gallery/vdo?year={$year}&page={$nextPage}");?>"><img src="assets/images/pagination-next.png" /></a>
            <?php
            }
        ?>
    </div>
</div>


<style>
    .pagination .breadcrumb{
        background: none;
    }
    .pagination .breadcrumb > li + li:before{
        content: "|";
    }
    .pagination .breadcrumb-item.active > a{
        background-color: #0c2340;
    }
    .pagination .breadcrumb > li > a {
        padding: 2px 5px;
        color: #0c2340;
        font-weight: bold;
    }
    .pagination .breadcrumb-item.active > a{
        color: #fbad23;
    }
    .dimm{
        position: absolute !important;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.1);
        justify-content: center;
        display: flex !important;
    }
    .dimm > img{
        width: 30%;
        height: auto;
        align-self: center;
    }
</style>
