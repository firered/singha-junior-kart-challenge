<div id="body" style="min-height: 80vh;">
    <div class="container">
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumb ">
                    <li class="breadcrumb-item"><a href="<?php echo site_url('gallery/');?>">GALLERY</a></li> 
                    <li class="breadcrumb-item"><a href="<?php echo site_url('gallery/photo');?>">PHOTO</a></li>
                    <li class="breadcrumb-item"><?php echo utf8_substr($rs['subject'], 0, 100);?></li>
                </ul>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12 text-right">
                <a href="<?php echo site_url("gallery/downloadImages/{$rs['id']}");?>" target="_blank"><img src="assets/images/btn-download.png" /></a>
                <p>&nbsp;</p>
            </div>
        </div>
        
        <div class="row description">
            <div class="col-12">
                <?php echo $rs['description'];?>
            </div>
        </div>
        
        <div class="row grid">
            <?php
                foreach ($rs['_image'] AS $image) {
                ?>
                    <div class="col-sm-4 col-md-3 grid-item">
                        <a data-toggle="modal" 
                           data-src="<?php echo site_url("gallery/photoDetailAjax/{$rs['id']}/?image={$image['filepath']}"); ?>" 
                           data-height='600' data-width='100%'
                           data-target="#myModal"
                           class="modalButton">
                            <img src="<?php echo base_url($image['filepath']);?>" />
                        </a>
                    </div>
                <?php
                }
            ?>
        </div>
        
        <div class="row description">
            <div class="col-md-6 text-left">
                <a href="javascript:window.history.back();" class="btn-back">
                    <img src="assets/images/ico_back.png" />&nbsp;BACK
                </a>
            </div>
            
            <div class="col-md-6  text-right">
                <a href="javascript:gotoTop();" class="btn-gototop" >
                    GO TO TOP
                </a>
            </div>
        </div>
    </div>
</div>


<style>
    .grid-item img {
        max-width: 100%;
    }
    .grid-item{
        margin-bottom: 10px;
        border-radius: 3px;
    }
    
    .btn-back{
        color: #0c2340;
        justify-content: center;
        display: flex;
        width: fit-content;
    }
    .btn-back > img {
        align-self: center;
    }
    .btn-back a{
        color: #0c2340;
        display: flex;
        align-self: center;
    }
    .btn-gototop{
        float: right;
        color: #0c2340;
    }
</style>
<script src="js/masonry.pkgd.min.js"></script>
<script>
     $(function(){
        $('.grid').masonry({
            itemSelector: '.grid-item',
            transitionDuration: '0.2s',
        });
        $('#myModal').on('show.bs.modal', function () {
            $(this).find('.modal-body').css({
                   width:'auto', //probably not needed
                   height:'auto', //probably not needed 
                   'max-height':'100%'
            });
         });
    });
</script>