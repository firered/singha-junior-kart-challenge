<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Singha challenge</title>
    <base href="<?php echo base_url('files');?>/" />
	<link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
    <link href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css"type="text/css" rel="stylesheet" >
	<link href="assets/css/web.css" type="text/css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    

</head>

<body class="layout_popup">
	
    <div class="boxPopup">
           <div class="wrapBox">
                <div class="bgBorderBox">
                     <div class="borderBox">
                          <div class="borderInsetBox">
                                <div class="content content_aboutus">
                                        <div class="wrapContent_aboutus">
                                        	<div id="aboutusMCustomScrollbar" class="mCustomScrollbar">
                                                    <div class="body" style="padding: 15px;">
                                                    <div><?php echo $rs['subject']; ?></div>
                                                    <center>
                                                        <video width="100%" 
                                                               preload="auto" 
                                                               poster="<?php echo $rs['display_image'] ? base_url($rs['display_image']) : ''; ?>" 
                                                               data-setup="{}"
                                                               controls
                                                               style="border-radius: 8px;
                                                                        margin-top: 10px;
                                                                        max-width: 800px;">
                                                            <source src="<?php echo $rs['vdo_mp4'] ? base_url($rs['vdo_mp4']) : ''; ?>" type="video/mp4">
                                                            <source src="<?php echo $rs['vdo_webm'] ? base_url($rs['vdo_webm']) : ''; ?>" type="video/webm">
                                                            <source src="<?php echo $rs['vdo_ogv'] ? base_url($rs['vdo_ogv']) : ''; ?>" type="video/ogg">
                                                          Your browser does not support the video tag.
                                                        </video>
                                                    </center>
                                                    
                                                    <div><?php echo $rs['description']; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                     </div>
                </div>
          </div>
    </div>
                    
<script src="assets/js/jquery.min.js"></script>
<script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>
<style>
    .breadcrumb{
        background: none;
        font-family: 'Cooper Std', "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        text-align: left;
    }
    .breadcrumb-item,
    .breadcrumb-item a{
        color: #ffaa00;
        text-shadow:    2px 2px 0 #770303,
                        -1px -1px 0 #770303,
                        1px -1px 0 #770303,
                        -1px 1px 0 #770303,
                        1px 1px 0 #770303;
    }
    .breadcrumb-item a{
        text-decoration: none;
    }
    .grid-item img {
        max-width: 100%;
    }
    .grid-item{
        margin-bottom: 10px;
        border-radius: 3px;
    }
</style>
<script>
   $(document).ready(function(){
		$("#aboutusMCustomScrollbar").mCustomScrollbar({
			axis:"y",
		});
	});
</script>

</body>
</html>
