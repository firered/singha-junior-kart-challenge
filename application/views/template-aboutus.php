
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Singha challenge</title>
    <base href="<?php echo base_url('files');?>/" />
	<link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
    <link href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css"type="text/css" rel="stylesheet" >
	<link href="assets/css/web.css" type="text/css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    

</head>

<body class="layout_popup">
	
    <div class="boxPopup">
           <div class="wrapBox">
                <div class="bgBorderBox">
                     <div class="borderBox">
                          <div class="borderInsetBox">
                                <div class="content content_aboutus">
                                        <div class="wrapContent_aboutus">
                                        	<div id="aboutusMCustomScrollbar" class="mCustomScrollbar">
                                                <div class="head"><img src="assets/images/popup/aboutus/logo.png" class="img-responsive" /></div>
                                                <div class="body">
                                                    <p>เปิดประสบการณ์การค้นหาสุดยอดนักแข่งโกคาร์ทระดับเยาวชนที่ยิ่งใหญ่ที่สุดของประเทศไทยกับ <br>Singha Kart Cup 2019 by Kimi Raikkonen</p>
                                                    <p>พบกับบันไดขั้นแรกสู่ความสำเร็จที่ยิ่งใหญ่ของวงการมอเตอร์สปอร์ต จากตัวจริงที่จะมาให้ความรู้กันแบบใกล้ชิด <br> ทั้งแชมป์อินเตอร์เนชั่นแนลอย่าง มิสเตอร์ คาร์โล แวน แดม ร่วมด้วยนักแข่งหัวใจสิงห์ แชมป์ประเทศไทย <br> ติณห์ ศรีตรัย และ แบงค์ กันตศักดิ์ กุศิริ </p>
                                                    
                                                    <p>“นี่คือโอกาสที่เยาวชนไทยจะได้สัมผัสประสบการณ์ด้านความเร็วระดับโลก ที่รับรองว่าหาจากที่ไหนไม่ได้” </p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                     </div>
                </div>
          </div>
    </div>
                    
<script src="assets/js/jquery.min.js"></script>
<script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
   $(document).ready(function(){
		$("#aboutusMCustomScrollbar").mCustomScrollbar({
			axis:"y",
		});
	});
</script>

</body>
</html>
