<div id="body"
     style="min-height: 80vh;">
    <div class="container">
        <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
        
        <div class="row">
            <div class="col-12 text-right">
                <?php
                    $photoYears = $this->main_model->getNewsYears();
                ?>
                <select class="year-select">
                    <option value="0">FILTER : All</option>
                    <?php
                        foreach ($photoYears AS $year) {
                        ?>
                            <option value="<?php echo $year;?>" <?php echo @$_REQUEST['year']==$year?'selected="selected"':''; ?>>FILTER : <?php echo $year;?></option>
                        <?php
                        }
                    ?>
                </select>
                <p>&nbsp;</p>
            </div>
        </div>


        <div class="row">
            <div id="_owl-itemList">
                <?php
                    $limit = 9;
                    $start = @$_REQUEST['page'] ? ($_REQUEST['page']-1)*$limit : 0;
                    $exceptId = 0;
                    $currentYear = @$_REQUEST['year'] ? $_REQUEST['year'] : 0;
                    $rs = $this->main_model->getNews($exceptId, $currentYear, $start, $limit);
                    for($i = 0; $i<count($rs); $i++) {
                        $row = $rs[$i];
                        shortThaiDate($row['create_date']);
                    ?>
                        <div class="col-md-4">
                            <div class="item boxItem">
                                <div class="wrapBox">
                                    <div class="bgBorderBox">
                                        <div class="borderBox">
                                            <div class="borderInsetBox">
                                                <div class="content">
                                                    <a data-toggle="modal"  
                                                        data-src="<?php echo site_url("news/show/{$row['id']}")?>" 
                                                        data-target="#myModal" 
                                                        data-height='1000' 
                                                        data-width='100%'  
                                                        class="modalButton" >
                                                        <img src="<?php echo base_url($row['display_image']); ?>" class="img-responsive">
                                                        <div class="b_content">
                                                            <h3><?php echo $row['subject']; ?></h3>
                                                            <label><?php echo $row['create_date']; ?></label>
                                                            <em>see more</em>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                        </div>
                    <?php
                    }
                ?>
            </div>
        </div>

        <p>&nbsp;</p>
        <?php
            $year = @$_REQUEST['year'];
            $currentPage = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
            $total = $this->main_model->countNews(0, $year);
            $totalPages = ceil($total / $limit);
        ?>
        <div class="row">
            <div class="col-12 pagination"
                 style="justify-content: center;
                        display: flex;">
                <?php
                    if ($currentPage > 1) {
                        $nextPage = $currentPage-1;
                    ?>
                        <a href="<?php echo site_url("news?year={$year}&page={$nextPage}");?>"><img src="assets/images/pagination-back.png" /></a>
                    <?php
                    }
                ?>
                <ul class="breadcrumb">
                <?php
                    for ($i=0;  $i<$totalPages;  $i++) {
                        $pageNo = $i+1;
                    ?>
                        <li class="breadcrumb-item <?php echo ($currentPage==$pageNo) ? 'active' : '';?>">
                            <a href="<?php echo site_url("news?year={$year}&page={$pageNo}");?>"><?php echo $i+1;?></a>
                        </li>
                    <?php
                    }
                ?>
                </ul>
                <?php
                    if ($currentPage < $totalPages) {
                        $nextPage = $currentPage+1;
                    ?>
                        <a href="<?php echo site_url("news?year={$year}&page={$nextPage}");?>"><img src="assets/images/pagination-next.png" /></a>
                    <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>


<style>
    .select-custom{
        background: url(assets/images/gallery/year_bg.png) no-repeat;
        width: 198px;
        justify-content: center;
        display: inline-flex;
        height: 50px;
    }
    .year-select{
        font-family: 'Cooper Std';
        padding-left: 30px;
        background: url(assets/images/gallery/year_bg.png) no-repeat;
        width: 198px;
        justify-content: center;
        display: inline-flex;
        height: 50px;
        
/*        -webkit-text-stroke: 1px #770303;
        paint-order: stroke fill;*/
        
        border: none;
    }
    .select-custom .filter {
        background: none;
        width: auto;
        border: none;
        font-size: 20px;
        outline: none;
        text-overflow: ellipsis;
        font-family: 'db_heaventbold';
        -webkit-text-stroke: 1px #770303;
        paint-order: stroke fill;
        color: white;
        align-self: center;
        text-decoration: none;
    }
    .select-custom:hover::before {
        color: rgba(255, 255, 255, 0.6);
        background-color: rgba(255, 255, 255, 0.2);
    }
    .select-custom select option {
        padding: 30px;
    }
    .pagination .breadcrumb{
        background: none;
    }
    .pagination .breadcrumb > li + li:before{
        content: "|";
    }
    .pagination .breadcrumb-item.active > a{
        background-color: #0c2340;
    }
    .pagination .breadcrumb > li > a {
        padding: 2px 5px;
        color: #0c2340;
        font-weight: bold;
    }
    .pagination .breadcrumb-item.active > a{
        color: #fbad23;
    }
</style>

<script>
    $(function(){
        $('.year-select').change(function() {
            window.location = '<?php echo current_url()?>?year='+ this.value;
        });
    });
</script>
