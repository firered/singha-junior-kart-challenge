<div class="col-xs-12 col-md-8">
    <div class="box boxNews" id="boxNews">
        <h2>news update</h2>
        <div class="wrapBox">
            <div class="bgBorderBox">
                <div class="borderBox">
                    <div class="borderInsetBox">
                        <div class="boxNewscontent">
                            <?php
                                $news = $this->main_model->getLastNews();
                                shortThaiDate($news['create_date']);
                            ?>
                            <a  data-toggle="modal"  
                                data-src="<?php echo site_url("news/show/{$news['id']}")?>" 
                                data-target="#myModal" 
                                data-height='1000' 
                                data-width='100%'  
                                class="modalButton"  >
                                <img src="<?php echo base_url($news['display_image']);?>" class="img-responsive" width="100%" />
                                <div class="b_content">
                                    <h3><?php echo $news['subject'];?></h3>
                                    <label><?php echo $news['create_date'];?></label>
                                    <em>see more</em>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>