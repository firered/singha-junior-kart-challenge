<div id="owl-itemList" class="owl-carousel">
    <?php
        $lastNews = $this->main_model->getLastNews();
        $news_ls = $this->main_model->getNews($lastNews['id']);
        foreach ($news_ls AS $news) {
            shortThaiDate($news['create_date']);
        ?>
            <div class="item boxItem">
                <div class="wrapBox">
                    <div class="bgBorderBox">
                        <div class="borderBox">
                            <div class="borderInsetBox">
                                <div class="content">
                                    <a  data-toggle="modal"  
                                        data-src="<?php echo site_url("news/show/{$news['id']}"); ?>"  
                                        data-target="#myModal" 
                                        data-height='1000'
                                         data-width='100%'  
                                         class="modalButton"  >
                                        <img src="<?php echo base_url($news['display_image']); ?>" class="img-responsive" />
                                        <div class="b_content">
                                            <h3><?php echo $news['subject']; ?></h3>
                                            <label><?php echo $news['create_date']; ?></label>
                                            <em>see more</em>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
    ?>
</div>

<center>
    <a href="<?php echo site_url('news');?>"><img src="assets/images/btn_more.png" /></a>
</center>