<div class="row">
    <?php
        $exceptId=0;
        $year = 0;
        $start=0;
        $limit = 3;
        $photos = $this->main_model->getPhoto($exceptId, $year, $start, $limit);
        for ($i=0;  $i<count($photos);  $i++) {
            $row = $photos[$i];
            shortThaiDate($row['create_date']);
        ?>
            <div class="col-md-4">
                <div class="box boxNews box-photo" id="boxNews ">
                    <?php
                        if ($i == 0) {
                            echo '<h2>PHOTO UPDATE</h2>';
                        }
                    ?>
                    <div class="wrapBox">
                        <div class="bgBorderBox">
                            <div class="borderBox">
                                <div class="borderInsetBox">
                                    <div class="boxNewscontent">
                                        <a href="<?php echo site_url("gallery/photoDetail/{$row['id']}");?>"
                                           class="modalButton">
                                            <img src="<?php echo base_url($row['display_image']);?>" class="img-responsive" width="100%">
                                            <div class="b_content">
                                                <h3><?php echo $row['subject']; ?></h3>
                                                <label><?php echo $row['create_date']; ?></label>
                                                <em>see more</em>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        <?php
        }
    ?>
</div>



<div class="row">
    <center>
        <a href="<?php echo site_url('gallery/photo');?>"><img src="assets/images/btn_more.png" /></a>
    </center>
</div>