<div id="body">
    <div class="container">
        <?php
            $this->load->view('home/banner');
        ?>
        <div class="row">
            <?php
                $this->load->view('home/clip_update');
                $this->load->view('home/activity_update');
            ?>
        </div>
        
        <?php
            $this->load->view('home/photo_update');
        ?>
        
        <div class="row">
            <?php
                $this->load->view('home/news_update');
                $this->load->view('home/facebook');
            ?>
        </div>
        
        
        <div class="row">
            <div class="col-xs-12">
                <?php
                    $this->load->view('home/news_slider');
                ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 boxfacebookMobile">
                    <div class="boxFacebook boxFace">
                    <h2>facebook</h2>
                    <div class="wrapBox">
                        <div class="bgBorderBox">
                            <div class="borderBox">
                                <div class="borderInsetBox">
                                    <div class="content">
                                        <div class="fb-page" data-href="https://www.facebook.com/SinghaKartCup/" data-tabs="timeline" data-height="300" data-width="500" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/singhajuniorkartchallenge/"><a href="https://www.facebook.com/singhajuniorkartchallenge/">Singha Junior Kart Challenge</a></blockquote></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>