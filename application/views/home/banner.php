<div class="banner row">
    <div id="owl-banner" class="owl-carousel owl-theme">
        <?php
            $banners = $this->main_model->getBanner();
            if ($banners) {
                foreach ($banners AS $banner) {
                ?>
                    <div class="item">
                        <a  data-toggle="modal"  
                            data-src="<?php echo site_url("hilight/show/{$banner['id']}");?>" 
                            data-target="#myModal"
                            data-height='1000'
                            data-width='100%' 
                            class="modalButton"  >
                            <img src="<?php echo base_url($banner['display_image_mobile']);?>" class="img-responsive img-mobile" />
                            <img src="<?php echo base_url($banner['display_image']);?>" class="img-responsive img-desktop" />
                        </a>
                    </div>
                <?php
                }
            }
        ?>
    </div>
</div>
