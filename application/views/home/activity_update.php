<div class="col-xs-12 col-md-6">
        <div class="box boxActivity">
        <h2>activity update</h2>

        <div class="wrapBox">
            <div class="bgBorderBox">
                <div class="borderBox">
                    <div class="borderInsetBox">
                        <div class="content">
                            <div id="activity_container">
                                <div id="tabs_container">
                                    <ul class="tabs_menu">
                                        <li data-href="#tab_1" >Score &amp; Ranking</li>
                                        <li data-href="#tab_2" class="current">INFORMATION</li>
                                        <li data-href="#tab_3" >Karting tracks</li>
                                    </ul>
                                    <div class="tabs">
                                        <div id="tab_1" class="tab_content">
                                            <?php
                                                $allowedTag = '<div><span><br><b><a><p><strong><img><table><tr><th><td><h1><h2><h3><h4><ul><li><strong><ins><script><tbody><theader><tfooter>';
                                                $activityScore = $this->main_model->getActivityScore();
                                                $activityScore['description'] = str_replace('src="files/', 'src="'.base_url().'files/', $activityScore['description']);
                                                echo strip_tags($activityScore['description'], $allowedTag);
                                            ?>
                                        </div>

                                        <div id="tab_2" class="tab_content">
                                            <?php
                                                $activityEvent = $this->main_model->getActivityEvent();
                                                $activityEvent['description'] = str_replace('src="files/', 'src="'.base_url().'files/', $activityEvent['description']);
                                                echo strip_tags($activityEvent['description'], $allowedTag);
                                            ?>
                                        </div>

                                        <div id="tab_3" class="tab_content">
                                            <?php
                                                $activityKartingTrack = $this->main_model->getActivityKartingTrack();
                                                $activityKartingTrack['description'] = str_replace('src="files/', 'src="'.base_url().'files/', $activityKartingTrack['description']);
                                                echo strip_tags($activityKartingTrack['description'], $allowedTag);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>