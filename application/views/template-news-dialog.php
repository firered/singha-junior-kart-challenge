
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Singha challenge</title>
    <base href="<?php echo base_url('files');?>/" />
	<link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
	<link href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" >
	<link href="assets/css/web.css" type="text/css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body class="layout_popup">
	
	<div class="boxPopup">
		   <div class="wrapBox">
				<div class="bgBorderBox">
					 <div class="borderBox">
						  <div class="borderInsetBox">
								<div class="content">
									<div class="content_news">
										<h3><?php echo $rs['subject']; ?></h3>
										<div id="newsMCustomScrollbar" class="mCustomScrollbar">
										<div class="wrapContent_news">
                                            <?php
                                                echo str_replace('src="files/', 'src="', $rs['description']);
                                            ?>
                                        </div>
									</div>
								</div>
							</div>
					 </div>
				</div>
		  </div>
	</div>
					
<script src="assets/js/jquery.min.js"></script>
<script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
   $(document).ready(function(){
		$("#newsMCustomScrollbar").mCustomScrollbar({
		axis:"y",
		});
	});
</script>
</body>
</html>
