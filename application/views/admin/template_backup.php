<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <head>
        <title>Administrator</title>
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <base href="<?=base_url()?>files/admin/" />
        
        <script src="js/jquery1.8.3.js"></script>
        <script src="js/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
        <link type="text/css" rel="stylesheet" href="js/jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />
        
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/fonts/style.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/main-responsive.css">
        <link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="assets/css/<?=$setting_model->getUserConfig('theme')?>.css" id="skin_color">
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css" />
        <link rel="stylesheet" href="assets/plugins/DataTables/media/css/DT_bootstrap.css" />
        <!--[if IE 7]>
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <![endif]-->
<!--        <script src="assets/js/jquery1_10_2.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>-->
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="assets/js/main.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        
        
        
        <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
        <script src="assets/js/table-data.js"></script>
        
        
        
        
        <link rel="stylesheet" href="assets/plugins/ladda-bootstrap/dist/ladda-themeless.min.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-social-buttons/social-buttons-3.css">
        <script src="assets/plugins/ladda-bootstrap/dist/spin.min.js"></script>
        <script src="assets/plugins/ladda-bootstrap/dist/ladda.min.js"></script>
        <script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js"></script>
        <script src="assets/js/ui-buttons.js"></script>
        
        
        
        <link rel="stylesheet" href="assets/plugins/select2/select2.css">
        <link rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css">
        <link rel="stylesheet" href="assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css">
        <link rel="stylesheet" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <link rel="stylesheet" href="assets/plugins/summernote/build/summernote.css">
        <!--<link rel="stylesheet" href="assets/plugins/ckeditor/contents.css">-->
        <link rel="stylesheet" href="assets/plugins/tinymce/js/tinymce/skins/lightgray/skin.min.css" />
        
        <script src="assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js"></script>
        <script src="assets/plugins/autosize/jquery.autosize.min.js"></script>
        <script src="assets/plugins/select2/select2.min.js"></script>
        <script src="assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
        <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
        <!--<script src="assets/plugins/bootstrap-colorpicker/js/commits.js"></script>-->
        <script src="assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
        <script src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script src="assets/plugins/summernote/build/summernote.min.js"></script>
        <!--<script src="assets/plugins/ckeditor/ckeditor.js"></script>-->
        <!--<script src="assets/plugins/ckeditor/adapters/jquery.js"></script>-->
        <!--<script src="assets/plugins/tinymce/js/tinymce/jquery.tinymce.min.js"></script>-->    
        <script src="assets/plugins/tinymce/js/tinymce/tinymce.min.js"></script>      
        <script src="assets/js/form-elements.js"></script>
        
        
        
        <script src="js/flaviusmatis-simplePagination/jquery.simplePagination.js"></script>
        <link rel="stylesheet" type="text/css" href="js/flaviusmatis-simplePagination/simplePagination.css" />
        
        
        
        
        <script>
            jQuery(document).ready(function() {
                Main.init();
                TableData.init();
                UIButtons.init();
                FormElements.init();
                tinymce.init({
                    selector        : 'textarea.ckeditor',
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste "
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"

                });
                $('[title]').tooltip({
                    animation   : true
                });
                setInterval(function(){
                    $.post( '<?=base_url().$controller->segment(1)?>/system/hasSession/',
                            function(data){
                                if(data!='1'){
                                    window.location.reload();
                                }
                            });
                }, 10000);
            });
            function updateSettingLeftNavBar(val){
                $.post( '<?=base_url()?>admin/setting/updateSetting/',
                        {
                            field   : 'navbar_small',
                            val     : val
                        },function(data){});
            }
            function appendImage(url, filePath){
                //var imgHtml = CKEDITOR.dom.element.createFromHtml("<img src=" + url + " alt='' align='right'/>");
                //CKEDITOR.instances.body.insertElement(imgHtml);
                //$('textarea.ckeditor').ckeditor().editor.insertHtml( "<img src=" + url + " alt='' align='right'/>" );
                tinymce.execCommand('mceInsertContent',false,"<img src=" + url + " alt='' align='right'/>");
            }
        </script>
        
        
        
        <style>
            .datepicker{
                z-index: 999 !important;
            }
        </style>
        
    </head>
    <?PHP
        $isSmallNav         = $setting_model->getUserConfig('navbar_small') ? 'navigation-small' : '' ;
    ?>
    <body class="<?=$isSmallNav?>"
          session="1">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                    <?=$navBarHeader?>
                    <?=$navBarTool?>
            </div>
        </div>
        
        <div class="main-container">
            <div class="navbar-content">
                <?=$leftMenu?>
            </div>
            <div class="main-content">
                <div class="container">
                    <?=$breadcrumb?>
                    <?=$body?>
                </div>
            </div>
        </div>

        <div class="footer clearfix">
            <?=$footer?>
        </div>
    </body>
</html>