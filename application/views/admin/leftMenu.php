<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler"> </div>
    </li>

<!--    <li class="heading">
        <h3 class="uppercase">Layouts</h3>
    </li>-->




    <?PHP
        $db->select('*');
        $db->order_by('seq');
        $menu_ls                = $db->get('system_menu')->result_array();
        $len_menu               = count($menu_ls);
        if( $len_menu ){
            $controllerName     = trim(strtolower($uri->segment(2)));
            $method             = trim(strtolower($uri->segment(3)));
            if( !$controllerName ){
                $controllerName = 'dashboard';
            }
            foreach( $menu_ls   AS $menu ){
                $query          = ""
                        . " SELECT      *"
                        . " FROM        system_submenu"
                        . " WHERE       menu_id='{$menu['id']}' AND"
                        . "             status='1' "
                        . " ORDER BY    seq ASC"
                        . "";
                $subMenu_ls     = $db->query($query)->result_array();
                $len_subMenu    = count($subMenu_ls);
                $class_open     = ($controllerName==strtolower($menu['url']))?'open':'';
                $class_active   = ($controllerName==strtolower($menu['url']) && $method=='')?'active':'';
                $icon           = @$menu['icon'] ? $menu['icon'] : 'icon-list-alt';
                if( !$len_subMenu ){
                ?>
                    <li class="nav-item <?php echo $class_active;?> <?php echo $class_open;?>">
                        <a href="<?php echo base_url()."admin/{$menu['url']}/";?>" class="nav-link nav-toggle">
                            <i class="<?php echo $icon;?>"></i>
                            <span class="title"><?php echo $menu['name'];?></span>
                            <span class="arrow <?php echo $class_open;?>"></span>
                        </a>
                    </li>
                <?php
                }else{
                    foreach( $subMenu_ls    AS $submenu ){
                        if(strstr($uri->segment(2).'/'.$uri->segment(3), $submenu['url'])){
                            $class_active   = 'active';
                            break;
                        }
                    }
                ?>
                    <li class="nav-item <?php echo $class_active;?> <?php echo $class_open;?>">
        
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="<?php echo $icon;?>"></i>
                            <span class="title"><?php echo $menu['name'];?></span>
                            <span class="arrow  <?php echo $class_open;?>"></span>
                        </a>

                        <ul class="sub-menu">

                            <?PHP
                                foreach( $subMenu_ls    AS $submenu ){
                                    $class_active       = (strstr($uri->segment(2).'/'.$uri->segment(3), $submenu['url'])) ? 'active' : '';
                                    $icon_subMenu       = $submenu['icon'] ? $submenu['icon'] : $icon;
                                ?>
                                    
                                    <li class="nav-item <?php echo $class_active;?>">
                                        <a href="<?php echo base_url()."admin/{$submenu['url']}/";?>" class="nav-link">
                                            <i class="<?php echo $icon_subMenu;?>"></i> <?php echo $submenu['name'];?> </a>
                                    </li>
                                <?php
                                }
                            ?>
                            
                        </ul>
                    </li>
                    
                    
                <?php
                }
            }   
        }
    ?>

    


    
    
</ul>
