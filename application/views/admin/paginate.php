<?PHP
    $limit              = $CI->limit;
    $totalRows          = $model->countTotalRow_history();
    $totalPage          = ceil($totalRows/$limit);
    $currentPage        = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
    $url                = base_url().$_SERVER['PATH_INFO'];
    $url                = str_replace('//admin', '/admin', $url);
    $minPage            = 1;
    $maxPage            = $totalPage;
?>
<div class="row">
    <div class="col-md-12">
        <div class="dataTables_paginate paging_bootstrap">
            <ul class="pagination">
                <?PHP
                    if( $currentPage<=$minPage ){
                    ?>
                        <li class="prev disabled"><a><i class="icon-double-angle-left"></i> </a></li>
                    <?php
                    }else{
                    ?>
                        <li class="prev"><a href="<?php echo "{$url}?page=".($currentPage-1);?>"><i class="icon-double-angle-left"></i> </a></li>
                    <?php
                    }
                    for( $i=0;  $i<$totalPage;  $i++ ){
                        $runningPage    = $i+1;
                        $pageURL        = "{$url}?page={$runningPage}";
                        $active         = $currentPage==$runningPage ? 'active' : '';
                    ?>
                        <li class="<?php echo $active;?>"><a href="<?php echo $pageURL;?>"><?php echo $runningPage;?></a></li>
                    <?php
                    }
                    if( $currentPage>=$maxPage ){
                    ?>
                        <li class="next disabled"><a><i class="icon-double-angle-right"></i> </a></li>
                    <?php
                    }else{
                    ?>
                        <li class="next">
                            <a href="<?php echo "{$url}?page=".($currentPage+1);?>"> 
                                <i class="icon-double-angle-right"></i>
                            </a>
                        </li>
                    <?php
                    }
                ?>
                
            </ul>
        </div>
    </div>
</div>
<br /><br /><br />