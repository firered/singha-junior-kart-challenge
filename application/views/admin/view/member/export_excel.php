<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
<body>
    
    <div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
        <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
            <tr>
                <td width="94" height="30" align="center" valign="middle" ><strong>ลำดับ</strong></td>
                <td width="200" align="center" valign="middle" ><strong>ชื่อ-นามสกุล</strong></td>
                <td width="181" align="center" valign="middle" ><strong>เบอร์โทร</strong></td>
                <td width="181" align="center" valign="middle" ><strong>อีเมล์</strong></td>
                <td width="250" align="center" valign="middle" ><strong>วันที่</strong></td>
            </tr>
            
            
            <?php
                $count = 1;
                if(@$rs  && is_array($rs)  &&  count($rs)>0){
                    foreach( $rs AS $row ){
                        $data = json_decode($row['data']);
                    ?>
                        <tr>
                            <td height="25" align="center" valign="middle" ><?php echo $count;?></td>
                            <td align="center" valign="middle" ><?php echo $data->register->name;?></td>
                            <td align="center" valign="middle"><?php echo "{$data->register->phone}";?></td>
                            <td align="center" valign="middle"><?php echo $data->register->email;?></td>
                            <td align="center" valign="middle"><?php echo $row['create_date'];?></td>
                        </tr>
                    <?php
                        $count++;
                    }
                }
            ?>
        </table>
    </div>
    <script>
    window.onbeforeunload = function(){return false;};
    setTimeout(function(){window.close();}, 10000);
    </script>
</body>
</html>