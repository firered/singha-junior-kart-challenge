<a class="btn btn-primary" href="javascript:;" onclick="exportData();">
    <i class="icon-align-right"></i> Export รายชื่อทั้งหมด
</a>
<a class="btn btn-primary" href="javascript:;" onclick="exportShareFacebook();">
    <i class="icon-align-right"></i> Export รายชื่อที่แชร์ Facebook
</a>
<span class="loadingContainer" style="display:none;">  
    &nbsp;&nbsp;
    <img src="../images/loading.gif" style="width: 50px;"/>
    &nbsp;Loading...
</span>
&nbsp;&nbsp;&nbsp;&nbsp;
<br /><br />
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th class="center" width="30">ID</th>
            <th>ชื่อ-นามสกุล</th>
            <th>โทรศัพท์</th>
            <th>อีเมล์</th>
            <th width="50">Share?</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                foreach( $rs    AS  $row ){
                    $data = json_decode($row['data']);
                    $fbIconColor = 'gray';
                    $fbIconTitle = 'ยังไม่แชร์';
                    if ($row['shared_fb_success'] == '1') {
                        $fbIconColor = '#3578E5';
                        $fbIconTitle = 'แชร์แล้ว';
                    }
                    
                ?>
                    <tr>
                        <td class="center"><?php echo $row['id']?></td>
                        <td><?php echo $data->register->name;?></td>
                        <td><?php echo $data->register->phone;?></td>
                        <td><?php echo $data->register->email;?></td>
                        <td>
                            <center>
                                <i class="fa fa-facebook-square"
                                   title="<?php echo $fbIconTitle;?>"
                                   style="  font-size: 24px;
                                            color: <?php echo $fbIconColor;?>;"></i>
                            </center>
                        </td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <!--<li class="divider"></li>-->
                                    <li>
                                        <a href="javascript:removeTopic(<?php echo $row['id'];?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php
                }
            }
        ?>
        
    </tbody>
</table>


<br /><div class="paginate"></div><br />
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?php echo $totalRow;?>,
            itemsOnPage : <?php echo $limit;?>,
            cssStyle    : 'light-theme',
            currentPage : <?php echo (@$_REQUEST['page']?$_REQUEST['page']:1);?>,
            hrefTextPrefix : '<?php echo current_url();?>/?page='
        });
    });
</script>


<script>
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2);?>/remove/'+id;
        }
    }
    
    function exportData(){
        window.location="<?php echo getAdmin()->url("member/export");?>";
        //$(".loadingContainer").show();
    }
    
    function exportShareFacebook() {
        window.location="<?php echo getAdmin()->url("member/exportShareFacebook");?>";
    }
</script>