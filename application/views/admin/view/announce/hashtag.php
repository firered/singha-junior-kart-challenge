<div class="row">
    <div class="col-md-12 margin-bottom-25">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-language font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Hashtag</span>
                    <span class="caption-helper">instagram</span>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-setting" 
                      role="form" method="post" 
                      action="<?php echo site_url("admin/ig/hashtag");?>" novalidate="novalidate">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>

                        <div class="form-group form-md-line-input">
                            <?php
                                $hashtagKeys = $this->ig_model->getHashtagKey();
                                $key = "";
                                if( $hashtagKeys  && is_array($hashtagKeys)  &&  count($hashtagKeys)>0 ){
                                    $key = implode(", ", $hashtagKeys);
                                }
                            ?>
                            <input type="text" class="form-control" 
                                   placeholder="Enter hashtag here" 
                                   name="hashtag" 
                                   maxlength="200" 
                                   value="<?php echo $key;?>" />
                            <label for="">Hashtag</label>
                            <span class="help-block">ตย. lifeismotion, newlightiscoming</span>
                        </div>
                    </div>
                    <div class="form-actions noborder">
                        <button type="submit" class="btn blue">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>