<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" 
                      enctype="multipart/form-data"
                      action="<?php echo $this->admin->url("{$this->cmd}/system_update");?>" 
                      method="post">
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                Logo
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div>
                                        <div style="color: red; font-weight: bold;">ตัวอย่างการแสดงผล</div>
                                        <img src="../images/circuit-image.jpg"
                                             style="max-width: 60%;"/>
                                    </div>
                                    <p></p>
                                    <input type="file"
                                           class="form-control" 
                                           name="circuit_logo"
                                           accept="image/*" />
                                    <?php
                                        $icon = getAdmin()->getConfig()->circuit_logo;
                                        if ($icon) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($icon);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                คำอธิบาย
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div>
                                        <div style="color: red; font-weight: bold;">ตัวอย่างการแสดงผล</div>
                                        <img src="../images/circuit-text.jpg"
                                             style="max-width: 60%;"/>
                                    </div>
                                    <p></p>
                                    <textarea
                                        class="form-control" 
                                        placeholder="Enter here"
                                        name="circuit_text"
                                        rows="5"><?php echo getAdmin()->getConfig()->circuit_text;?></textarea>
                                    <span class="help-block"> Description</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    });    
</script>