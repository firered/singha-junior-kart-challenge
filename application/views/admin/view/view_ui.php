<div id="modal-confirm-dialog" 
     class="modal fade" 
     tabindex="-1" 
     data-backdrop="static" 
     data-keyboard="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="modal-body-content"></p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn default btnClose">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn green btnOk">Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-basic-dialog" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn default btnClose" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    function showConfirmDialog(
                title,
                message,
                onOk,
                onCancel
            ){
        var modal = $('#modal-confirm-dialog');
        modal.find('.modal-title').html(title);
        modal.find('.modal-body-content').html(message);
        if( onCancel!=undefined
                && typeof onCancel === "function"){
            modal.find('.close, .btnClose').click(onCancel);
        }
        if( onOk!=undefined
                && typeof onOk === "function"){
            modal.find('.btnOk').click(onOk);
        }
        modal.modal('show');
    }
    
    function showDialog(
                title,
                message,
                onClose
            ){
        var modal = $('#modal-basic-dialog');
        modal.find('.modal-title').html(title);
        modal.find('.modal-body').html(message);
        if( onClose!=undefined
                && typeof onClose === "function"){
            modal.find('.close, .btnClose').click(onClose);
        }
        modal.modal('show');
    }
</script>