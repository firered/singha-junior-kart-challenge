<div class="alert alert-block alert-<?php echo $type;?> fade in">
    <button type="button" class="close" data-dismiss="alert"></button>
    <h4 class="alert-heading"><?php echo $heading;?>!</h4>
    <p>
        <?php echo $message;?>
    </p>
</div>