<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <input name="refer_url" type='hidden' value="<?=@$this->agent->referrer()?>" />
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หมวดหมู่
        </label>
        <div class="col-sm-5">
            <select class="form-control" name="cid">
                <option value="">กรุณาเลือกหมวดหมู่</option>
                <?PHP
                    $cate_ls            = $model->categorylist();
                    if( $cate_ls ){
                        foreach( $cate_ls       AS $cate ){
                            $selected   = @$cid==$cate['id']?'selected="selected"':'';
                        ?>
                            <option value="<?=$cate['id']?>" <?=$selected?> ><?=$cate['subject']?></option>
                        <?
                        }
                    }
                ?>
            </select>
        </div>
        <span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>
    </div>-->
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?php echo @$subject; ?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รายละเอียด
        </label>
        <div class="col-sm-9">
            <textarea name="description" class=" form-control tinymce" cols="10" rows="20"><?php echo @$description;?></textarea>
        </div>
    </div>

<!--    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                URL
        </label>
        <div class="col-sm-9">
            <input name="url" type="text" id="form-field-7" class="form-control" value="<?php echo @$url; ?>" />
        </div>
    </div>-->
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพ (Desktop)
        </label>
        <div class="col-sm-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: auto; height: 150px;">
                    <img src="<?php echo @$display_image ? base_url($display_image) : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>" alt="">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"> </div>
                <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="hidden"><input type="file" name="display_image" accept="image/*">
                    </span>&nbsp;
                    <div style="color: #4d68e3;">ขนาดภาพ <b>Desktop</b> ที่แนะนำ 1170x560px</div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพ (Mobile)
        </label>
        <div class="col-sm-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: auto; height: 150px;">
                    <img src="<?php echo @$display_image_mobile ? base_url($display_image_mobile) : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>" alt="">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"> </div>
                <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="hidden"><input type="file" name="display_image_mobile" accept="image/*">
                    </span>&nbsp;
                    <div style="color: #4d68e3;">ขนาดภาพ <b>Mobile</b> ที่แนะนำ 640x560px</div>
                </div>
            </div>
            
        </div>
    </div>
    
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label">
                หัวข้อเด่น
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="sticky">
                <option value="0" <?=(@$sticky=='0'?'selected="selected"':'')?>>ไม่ใช่</option>
                <option value="1" <?=(@$sticky=='1'?'selected="selected"':'')?>>ใช่</option>
            </select>
        </div>
    </div>-->
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-4">
            <select class="form-control search-select" name="status">
                <option value="1" <?php echo (@$status=='1'?'selected="selected"':'');?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?php echo (@$status=='0'?'selected="selected"':'');?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    
</form>
<script>
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function setDefaultImageMobile(url, filePath){
        $('input[name=display_image_mobile]').val(filePath);
        $('input[name=display_image_mobile]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function clearDisplayImageMobile(){
        $('input[name=display_image_mobile]').val('');
        $('input[name=display_image_mobile]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
</script>
