<a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/add/?cid=<?=$cid?>&task=<?=$uri->segment(3)?>">
    <i class="icon-plus"></i> เพิ่มข้อมูล
</a>&nbsp;

&nbsp;&nbsp;&nbsp;
<!--<a href="<?=  base_url().$uri->segment(1).'/'.$uri->segment(2)?>/category/" class="btn btn-blue">
    <i class="icon-circle-arrow-right"></i> จัดการหมวดหมู่
</a>-->
<br /><br />
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th class="center" width="30"></th>
            <!--<th width="120">หมวดหมู่</th>-->
            <th>หัวข้อ</th>
            <th class="hidden-xs" width="130">สร้างเมื่อ</th>
            <th class="hidden-xs" width="80">สถานะ</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                $count = 1;
                foreach( $rs    AS  $row ){
                    $method =  'edit';
                    $url        = base_url().$uri->segment(1).'/'.$uri->segment(2).'/'. $method.'/'.$row['id'].'/';
                    @shortThaiDate($row['create_date']);
                ?>
                    <tr>
                        <td class="center"><?php echo $count++;?></td>
<!--                        <th>
                            <?php
//                                $cateObj = $this->{$this->model}->getCategoryDetail($row['cid']);
//                                echo @$cateObj['subject']?$cateObj['subject']:'-';
                            ?>
                        </th>-->
                        <td><?=$row['subject']?></td>
                        <td class="hidden-xs" align="center"><?=@$row['create_date']?></td>
                        <td class="hidden-xs">
                            <?PHP
                                switch(@$row['status']){
                                    case 1  : 
                                        echo '<span class="label label-success">แสดงผล</span>';
                                        break;
                                    case 0  :
                                        echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                        break;
                                    default :
                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                        break;
                                }
                            ?>
                        </td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?=$url?>">
                                            <i class="icon-edit"></i> แก้ไขข้อมูล
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:removeTopic(<?=$row['id']?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?
                }
            }
        ?>
        
    </tbody>
</table>


<br /><div class="paginate"></div><br />
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?=$totalRow?>,
            itemsOnPage : <?=$limit?>,
            cssStyle    : 'light-theme',
            currentPage : <?=(@$_REQUEST['page']?$_REQUEST['page']:1)?>,
            hrefTextPrefix : '<?=current_url()?>/?page='
        });
    });
</script>


<script>
    $(function (){
        $('.iCheck-helper').click(function(){
            var chkBox      = $(this).parent().find('input[type=checkbox]');
            id              = chkBox.attr('topic-id');
            isSticky        = chkBox.prop('checked') ? '1' : '0';
            $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                    {
                        id      : id,
                        sticky  : isSticky
                    },function(data){});
        });
    });
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/remove/'+id;
        }
    }
    
    function setSticky( obj, id ){
        $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                {
                    id      : id,
                    sticky  : obj.checked ? '1' : '0' 
                },function(data){});
    }
//    1805120793043606
</script>