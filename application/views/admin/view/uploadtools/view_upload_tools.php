<?php
    $supportedLangCode = array();
    foreach( $supportedLanguages AS $_ ){
        $supportedLangCode[] = $_['system_languages_code'];
    }
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="icon-cloud-upload"></i>
                File/Image Upload Tools
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                </div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="tabbable">
                            <ul id="myTab" class="nav nav-tabs tab-bricky">
                                    <li class="active">
                                        <a href="#panel-upload-image" data-toggle="tab">
                                            <i class="green clip-images-2"></i> Upload Image
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#panel-upload-file" data-toggle="tab">
                                            <i class="green clip-file"></i> Upload File
                                        </a>
                                    </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane in active" id="panel-upload-image">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                                    <span class="btn btn-file btn-light-grey">
                                                        <i class="icon-folder-open-alt"></i> 
                                                        Select And Upload Image
                                                        <input type="file" 
                                                               onchange="uploadImage(this);" 
                                                               accept="image/*" 
                                                               multiple="multiple" />
                                                    </span>
                                                </div>
                                                <script>
                                                    function uploadImage(obj){
                                                        if( obj.files.length>0 ){
                                                            var keyName         = 'image';
                                                            var data            = new FormData();
                                                            jQuery.each(obj.files, 
                                                                        function(i, f) {
                                                                            data.append(keyName+'[]', f);
                                                                        });
                                                            data.append('table',    '<?php echo $table;?>');
                                                            data.append('uid',      '<?php echo $uid;?>');
                                                            data.append('type',     'i');
                                                            data.append('cmd',      '<?php echo $cmd;?>');
                                                            data.append('supported_languages', <?php echo json_encode($supportedLangCode);?>);
                                                            $.ajax({
                                                                url         : '<?php echo base_url().$uri->segment(1);?>/uploadTool/uploadImage/',
                                                                data        : data,
                                                                cache       : false,
                                                                contentType : false,
                                                                processData : false,
                                                                type        : 'POST',
                                                                success     : function(output){
                                                                    output      = $.parseJSON(output);
                                                                    if( output.ok.length>0 ){
                                                                        for( i=0;  i<output.ok.length;  i++ ){
                                                                            $('#panel-uploaded-image').append(output.ok[i].data);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }

                                                    }
                                                </script>                                      
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <i class="icon-list"></i>
                                                    ทั้งหมด
                                                    <div class="panel-tools">
                                                        <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="bs-example">
                                                        <div class="row" id="panel-uploaded-image">
                                                            <?PHP
                                                                $image              = $model->getAllImage($uid);
                                                                $len_img            = count($image);
                                                                if( $len_img ){
                                                                    foreach( $image     AS $img ){
                                                                        $this->load->view(  'admin/view/uploadtools/imageUploaded',
                                                                                            Array(
                                                                                                'id'        => $img['id'],
                                                                                                'url'       => base_url().$img['filepath'],
                                                                                                'filePath'  => $img['filepath']
                                                                                            ));
                                                                    }
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>




                                 <!-- File -->
                                <div class="tab-pane" id="panel-upload-file">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                                    <span class="btn btn-file btn-light-grey">
                                                        <i class="icon-folder-open-alt"></i> 
                                                        Select And Upload File
                                                        <input type="file" 
                                                               onchange="uploadFile(this);" 
                                                               accept="application/pdf,application/x-gzip,application/x-rar-compressed,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/vnd.openxmlformats-officedocument.presentationml.presentation" 
                                                               multiple="multiple" />
                                                    </span>
                                                </div>
                                                <script>
                                                    function uploadFile(obj){
                                                        if( obj.files.length>0 ){
                                                            var keyName         = 'file';
                                                            var data            = new FormData();
                                                            jQuery.each(obj.files, 
                                                                        function(i, f) {
                                                                            data.append(keyName+'[]', f);
                                                                        });
                                                            data.append('table',    '<?php echo $table;?>');
                                                            data.append('uid',      '<?php echo $uid;?>');
                                                            data.append('type',     'i');
                                                            data.append('cmd',      '<?php echo $cmd;?>');
                                                            data.append('supported_languages', <?php echo json_encode($supportedLangCode);?>);
                                                            $.ajax({
                                                                url         : '<?php echo base_url().$uri->segment(1);?>/uploadTool/uploadFile/',
                                                                data        : data,
                                                                cache       : false,
                                                                contentType : false,
                                                                processData : false,
                                                                type        : 'POST',
                                                                success     : function(output){
                                                                    output      = $.parseJSON(output);
                                                                    if( output.ok.length>0 ){
                                                                        for( i=0;  i<output.ok.length;  i++ ){
                                                                            $('#panel-uploaded-file').append(output.ok[i].data);
                                                                        }
                                                                    }
                                                                }
                                                            });
                                                        }

                                                    }
                                                </script>                                      
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <i class="icon-list"></i>
                                                    ทั้งหมด
                                                    <div class="panel-tools">
                                                        <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="bs-example">
                                                        <div class="row" id="panel-uploaded-file">
                                                            <?PHP
                                                                $file              = $model->getAllFile($uid);
                                                                $len_img            = count($file);
                                                                if( $len_img ){
                                                                    foreach( $file     AS $img ){
                                                                        $this->load->view(  'admin/view/uploadtools/fileUploaded',
                                                                                            Array(
                                                                                                'id'        => $img['id'],
                                                                                                'url'       => base_url().$img['filepath'],
                                                                                                'filePath'  => $img['filepath'],
                                                                                                'ext'       => $img['ext'],
                                                                                                'filename'  => $img['filename']
                                                                                            ));
                                                                    }
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>


                            </div>





                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>



<script>
    function setDefaultImage(url, filePath, languageCode){
        $('#tab_content'+languageCode+' input[name=display_image]').val(filePath);
        $('#tab_content'+languageCode+' input[name=display_image]').parent().find('img').attr('src', url);
    }
    
    function clearDisplayImage(languageCode){
        $('#tab_content'+languageCode+' input[name=display_image]').val('');
        $('#tab_content'+languageCode+' input[name=display_image]').parent().find('img').attr('src', '');
    }
    
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/';?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
    
    function appendImage(url, filePath, languageCode){
        //var imgHtml = CKEDITOR.dom.element.createFromHtml("<img src=" + url + " alt='' align='right'/>");
        //CKEDITOR.instances.body.insertElement(imgHtml);
        //$('textarea.ckeditor').ckeditor().editor.insertHtml( "<img src=" + url + " alt='' align='right'/>" );

        //tinymce.execCommand('mceInsertContent',false,"<img src=" + url + " alt='' />");
        tinymce.get('textarea'+languageCode)
                .execCommand(
                    'mceInsertContent',
                    false,'<img src="<?php echo base_url();?>' + filePath + '" width="400" alt="" />'
                );
    }
</script>