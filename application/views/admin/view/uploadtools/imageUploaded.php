<div class="col-sm-2" 
     img-id="" 
     style="min-height:190px;
            margin-bottom: 10px;">
    <div class="thumbnail">
        <img src="<?=$url?>" />
        <br />
        <textarea onblur="updateDesc(this, <?=$id?>);"
                  style="   width: 118px;
                            max-width: 118px;
                            margin-bottom: 5px;"><?=@$desc?></textarea>
        <br />
        <div>
            <button class="btn btn-danger btn-xs" type="button" onclick="javascript:removeImage(this, <?=$id?>);">ลบ</button>
            <div class="btn-group">
                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                    <i class="icon-cog"></i> <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:setDefaultImage('<?=$url?>','<?=$filePath?>');">
                            <i class="icon-edit"></i> เลือกเป็นภาพประจำหัวข้อ
                        </a>
                    </li>
                    <li>
                        <a href="javascript:appendImage('<?=$url?>','<?=$filePath?>');">
                            <i class="icon-edit"></i> เพิ่มในเนื้อหา
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?=base_url().$filePath?>" target="_blank">
                            <i class="icon-"></i> เปิดภาพในหน้าต่างใหม่
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
</div>