<div class="col-sm-2" 
     img-id="" 
     style="min-height:190px;">
    <div class="thumbnail" >
        <center>
            <i class="clip-file" style="font-size:32px;"></i><br />
            <a href="<?=$url?>" target="_blank"><?=$filename?></a>
        </center>
        <br />
        <div>
            <button class="btn btn-danger btn-xs" type="button" onclick="javascript:removeImage(this, <?=$id?>);">ลบ</button>
            <div class="btn-group">
                <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                    <i class="icon-cog"></i> <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu pull-right">
                    <!--                    
                    <li>
                        <a href="javascript:setDefaultImage('<?=$url?>','<?=$filePath?>');">
                            <i class="icon-edit"></i> เลือกเป็นภาพประจำหัวข้อ
                        </a>
                    </li>-->
                    <li class="divider"></li>
                </ul>
            </div>
        </div>
    </div>
    
</div>