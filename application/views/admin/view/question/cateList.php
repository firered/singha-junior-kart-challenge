<div class="row">
    <div class="col-md-12">
        <?php echo @$this->session->flashdata('msg');?>
        <a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/addCategory/">
            <i class="icon-plus"></i> เพิ่มหมวดหมู่
        </a>
        <br /><br />
        
        <table class="table table-bordered" >
            <thead>
                <tr>
                    <!--<th class="center" width="80">ลำดับ</th>-->
                    <th>หัวข้อ</th>
                    <th class="hidden-xs" width="130">สร้างเมื่อ</th>
                    <th class="hidden-xs" width="80">สถานะ</th>
                    <th class="" width="60">Tools</th>
                </tr>
            </thead>
            <tbody>
                <?PHP
                    $len                = count($rs);
                    if( $len ){
                        foreach( $rs    AS  $row ){
                            $url        = getAdmin()->url("{$this->cmd}/categoryEdit/{$row['id']}");
                            @shortThaiDate($row['create_date']);
                        ?>
                            <tr>
                                <!--<td class="center"><?php echo $row['seq'];?></td>-->
                                <td><?=$row['subject']?></td>
                                <td class="hidden-xs" align="center"><?=@$row['create_date']?></td>
                                <td class="hidden-xs">
                                    <?PHP
                                        switch(@$row['status']){
                                            case 1  : 
                                                echo '<span class="label label-success">แสดงผล</span>';
                                                break;
                                            case 0  :
                                                echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                                break;
                                            default :
                                                echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                                break;
                                        }
                                    ?>
                                </td>
                                <td class="hidden-xs">
                                    <div class="btn-group">
                                        <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                            <i class="icon-cog"></i> <span class="caret"></span>
                                        </a>
                                        <ul role="menu" class="dropdown-menu pull-right">
                                            <li>
                                                <a href="<?=$url?>">
                                                    <i class="icon-edit"></i> แก้ไขข้อมูล
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:removeCategory(<?=$row['id']?>);">
                                                    <i class="icon-remove"></i> ลบข้อมูล
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?
                        }
                    }
                ?>
        </table>
        
    </div>
</div>


<script>
    function removeCategory(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2);?>/removeCategory/'+id;
        }
    }
    
    $(function(){
        $("input[name=update-cid-seq]").blur(function(){
            var id = $(this).attr('cid');
            var seq = this.value;
            $.post("<?php echo site_url($uri->segment(1).'/'.$uri->segment(2));?>/updateCateSeq/",{
                cid : id,
                seq : seq
            });
        });
    });
    
</script>