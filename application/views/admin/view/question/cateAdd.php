
    
<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" 
    class="form-horizontal" 
    action="<?php echo $this->admin->url("{$this->cmd}/addCategory_post");?>"
    method="post"
    enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ลำดับ
        </label>
        <div class="col-sm-5">
            <input name="seq" type="number" id="form-field-7" class="form-control" value="<?=@$seq?>" />
        </div>
        <span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>
    </div>-->

    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รายละเอียด
        </label>
        <div class="col-sm-9">
            <textarea name="description" class=" form-control" cols="10" rows="20"><?=@$description?></textarea>
        </div>
    </div>



    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพที่ใช้ในการแชร์
        </label>
        <div class="col-sm-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: auto; height: 150px;">
                    <img src="<?php echo @$display_image ? base_url($display_image) : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";?>" alt="">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"> </div>
                <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="hidden"><input type="file" name="display_image" accept="image/*">
                    </span>&nbsp;
                    <div style="color: #4d68e3;">ขนาดภาพ <b>Desktop</b> ที่แนะนำ 1,200 x 630 pixels</div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="1" <?=(@$status=='1'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?=(@$status=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
</form>