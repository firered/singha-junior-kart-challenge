<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" 
                      enctype="multipart/form-data"
                      action="<?php echo $this->admin->url("{$this->cmd}/system_update");?>" 
                      method="post">
                    
                    <?php /*
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                Icon VDO (หน้า Home) Hero section
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="file"
                                           class="form-control" 
                                           name="website_home_vdo_icon"
                                           accept="image/*" />
                                    <!--<label for="">Icon VDO (หน้า Home) Hero section</label>-->
                                    <!--<span class="help-block">ชื่อเวปไซต์ บน Title bar</span>-->
                                    <?php
                                        $icon = getAdmin()->getConfig()->website_home_vdo_icon;
                                        if ($icon) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($icon);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                     * 
                     */ ?>
                    
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                สนามแข่งขัน
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="text"
                                           class="form-control" 
                                           name="result_score_race"
                                           placeholder="CIRCUIT DE JEREZ SPAIN"
                                           value="<?php echo getAdmin()->getConfig()->result_score_race;?>"/>
                                    <span class="help-block"> ชื่อสนามแข่งขัน</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                วันที่ผลการแข่งขัน
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="text"
                                           class="form-control datepicker" 
                                           name="result_score_date"
                                           placeholder=""
                                           value="<?php echo getAdmin()->getConfig()->result_score_date;?>"/>
                                    <span class="help-block"> วันที่</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                ภาพ AW ผลการแข่งขัน MOTOGP (Desktop)
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div>
                                        <div style="color: red; font-weight: bold;">ตัวอย่างการแสดงผล</div>
                                        <img src="../images/aw_result_score.jpg"
                                             style="max-width: 60%;"/>
                                    </div>
                                    <p></p>
                                    <input type="file"
                                           class="form-control" 
                                           name="result_score_aw_desktop"
                                           accept="image/*" />
                                    <?php
                                        $icon = getAdmin()->getConfig()->result_score_aw_desktop;
                                        if ($icon) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($icon);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                ภาพ AW ผลการแข่งขัน MOTOGP (Mobile)
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div>
                                        <div style="color: red; font-weight: bold;">ตัวอย่างการแสดงผล</div>
                                        <img src="../images/aw_result_score.jpg"
                                             style="max-width: 60%;"/>
                                    </div>
                                    <p></p>
                                    <input type="file"
                                           class="form-control" 
                                           name="result_score_aw_mobile"
                                           accept="image/*" />
                                    <?php
                                        $icon = getAdmin()->getConfig()->result_score_aw_mobile;
                                        if ($icon) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($icon);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
    });    
</script>