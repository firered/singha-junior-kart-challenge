<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>

<div class="row">
    <?php
        $supportedLanguages = $this->setting_model->getSystemSupportedLanguages();
        $_colors = array( 'purple', 'red', 'green', 'yellow', 'blue' );
        $colors = $_colors;
        foreach( $supportedLanguages  AS $supportedLanguage ){
            $languageCode   = $supportedLanguage['system_languages_code'];
            $language       = $this->setting_model->getLanguageFromCode($languageCode);
            //$color          = $colors[rand(0, count($colors)-1)];
            $color          = array_pop($colors);
            if( count($colors)<1 ){
                $colors = $_colors;
            }
        ?>
            <div class="col-md-12">
                <div class="portlet box <?php echo $color;?>">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-plus"></i> 
                            <?php echo "{$language['code']} - {$language['name']}";?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        
                        <form role="form" 
                              class="form-horizontal form-prevent-default" 
                              action="" 
                              method="post">
                            <input name="task"  type="hidden" value="<?php echo  @$_REQUEST['task'];?>" />
                            <input name="cid"  type="hidden" value="<?php echo @$_REQUEST['cid'];?>" />
                            <input name="uid"   type="hidden" value="<?php echo $uid;?>" />
                            <input name="refer_url" type='hidden' value="<?php echo @$this->agent->referrer();?>" />
                        
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_content<?php echo $language['code'];?>" data-toggle="tab">Content</a>
                                </li>
                                <li>
                                    <a href="#tab_seo<?php echo $language['code'];?>" data-toggle="tab">SEO</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- TAB : CONTENT -->
                                <div class="tab-pane fade active in" id="tab_content<?php echo $language['code'];?>">

                                        <?php
                                            if( $supportedLanguage['status']!='1' ){
                                            ?>
                                                <div class="note note-info">
                                                    <h4>Info!</h4>
                                                    <p>
                                                        ภาษา 
                                                        <span style="font-weight:bold; color: red;"><?php echo "{$language['code']} - {$language['name']}";?> </span>
                                                        ยังไม่เปิดใช้งาน ข้อมูลส่วนนี้จะยังไม่แสดงผลผ่านหน้าเวป
                                                    </p>
                                                </div>
                                            <?php
                                            }
                                        ?>

                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="form-field-7">
                                                หมวดหมู่
                                            </label>
                                            <div class="col-sm-5">
                                                <select class="form-control" name="cid">
                                                    <option value="">กรุณาเลือกหมวดหมู่</option>
                                                    <?PHP
                                                        $cate_ls            = $model->categorylist();
                                                        if( $cate_ls ){
                                                            foreach( $cate_ls       AS $cate ){
                                                                $selected   = @$_REQUEST['cid']==$cate['id']?'selected="selected"':'';
                                                            ?>
                                                                <option value="<?php echo $cate['id'];?>" <?php echo $selected;?> ><?php echo $cate['subject'];?></option>
                                                            <?php
                                                            }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
                                        </div>


                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-2 control-label">
                                                หัวข้อ
                                            </label>
                                            <div class="col-sm-9">
                                                <input name="subject" 
                                                        type="text" 
                                                        id="form-field-7"
                                                        class="form-control"
                                                        maxlength="255" />
                                                <span class="help-block">Max length 255 characters </span>
                                                <div class="form-control-focus"></div>
                                            </div>
                                        </div>


                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="form-field-7">
                                                    รายละเอียด
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea name="description" 
                                                          id="textarea<?php echo $language['code'];?>"
                                                          class="tinymce form-control" 
                                                          cols="10" 
                                                          rows="20"
                                                          maxlength="16777215"></textarea>
                                                <span class="help-inline col-sm-9"> <i class="icon-info-sign"></i> Max length 16,777,215 characters </span>
                                            </div>
                                        </div>


                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label" for="form-field-7">
                                                    ภาพประจำหัวข้อ
                                            </label>
                                            <div class="col-sm-9">
                                                <div class="fileupload-new thumbnail" style="width: 140px; height: 103px;">
                                                    <img src="<?php echo base_url();?>image/ratio/?width=140&height=100&defaultText=Display image" alt="">
                                                    <input type="hidden" name='display_image' value="" />
                                                </div>
                                                <button class="btn btn-danger btn-xs" 
                                                        onclick="clearDisplayImage('<?php echo $language['code'];?>');" 
                                                        type="button">
                                                    ลบรูปประจำหัวข้อ
                                                </button>
                                            </div>
                                        </div>


                                        <div class="form-group form-md-line-input">
                                            <label class="col-sm-2 control-label">
                                                    สถานะการแสดงผล
                                            </label>
                                            <div class="col-sm-9">
                                                <select class="form-control search-select" name="status">
                                                    <option value="1">แสดงผลหน้าเว็บ</option>
                                                    <option value="0">ไม่แสดง</option>
                                                </select>
                                            </div>
                                        </div>


                                    
                                </div>

                                <!-- TAB : SEO -->
                                <div class="tab-pane fade" id="tab_seo<?php echo $language['code'];?>">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" 
                                                   class="form-control" 
                                                   placeholder="Enter text here"
                                                   name="seo_keywords"
                                                   maxlength="512"
                                                   value=""/>
                                            <label for="">SEO Keywords</label>
                                            <span class="help-block">Max length 512 characters, สามารถใส่ได้หลายคำ และแบ่งคำโดยใช้เครื่องหมายจุลภาค “,”</span>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <input type="text" 
                                                   class="form-control" 
                                                   placeholder="Enter text here"
                                                   name="seo_description"
                                                   maxlength="512"
                                                   value=""/>
                                            <label for="">SEO Description</label>
                                            <span class="help-block">Max length 512 characters, ใช้สำหรับแสดงรายละเอียดสั้น ๆ ของหน้าเว็บไซต์ที่กำลังแสดงผลอยู่ ไม่ควรเขียนให้สั้น หรือ ยาวจนเกินไป ข้อความที่เขียนควรสัมพันธ์กับเนื้อหาของหน้านั้น ๆ</span>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        
                        </form>
                        
                        
                    </div>
                </div>
            </div>
        <?php
        }
    ?>
    
    
    <div class="col-md-12">
        <div class="btn-form">
            <button type="submit" 
                    class="btn btn-primary btn-lg" >
                บันทึก
            </button>
            <button type="button" 
                    class="btn btn-danger btn-lg" 
                    onclick="window.history.back();">
                ยกเลิกและย้อนกลับ
            </button>
        </div>
        <div class="clearfix"></div>
        <p>&nbsp;</p><p>&nbsp;</p>
    </div>
    
    
</div>


<style>
    .btn-form .btn{
        width: 49%;
        padding: 20px;
    }
    .btn-form .btn.btn-primary{ float: left; }
    .btn-form .btn.btn-danger{ float: right; }
    .btn-form{ text-align: center; }
</style>
    
    
<!--  Upload Zone  -->
<?php 
    getAdmin()->getView(
                'uploadtools/view_upload_tools',
                array(
                    'table' => $table,
                    'uid'   => $uid,
                    'cmd'   => $cmd,
                    'model' => $model,
                    'uri'   => $this->uri,
                    'supportedLanguages' => $supportedLanguages
                )
            ); 
?>
<!--  END Upload Zone  -->