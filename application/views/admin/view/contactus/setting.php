<div class="row">
   
    <div class="col-md-12">
        <form role="form" 
            enctype="multipart/form-data"
            action="<?php echo $this->admin->url("{$this->cmd}/system_update");?>" 
            method="post">
            
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                            <i class="icon-settings font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> About Us</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                ข้อความ About Us
                            </label>
                            <div class="col-sm-12">
                                <textarea name="aboutus_txt" 
                                          class=" form-control " 
                                          cols="10" rows="20"><?php echo getAdmin()->getConfig()->aboutus_txt;?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                            <i class="icon-settings font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Contact Us</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                Address
                            </label>
                            <div class="col-sm-12">
                                <textarea
                                    name="contact_address" 
                                    class=" form-control " 
                                    cols="4" rows="4" 
                                    maxlength="1024"><?php echo getAdmin()->getConfig()->contact_address;?></textarea>
                            </div>
                        </div>
                        
                        <p></p>
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                Telephone
                            </label>
                            <div class="col-sm-12">
                                <input type="text" 
                                        class="form-control" 
                                        placeholder="Enter text here"
                                        name="contact_phone"
                                        maxlength="1024"
                                        value="<?php echo getAdmin()->getConfig()->contact_phone;?>"/>
                            </div>
                        </div>
                        
                        <p></p>
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                Fax
                            </label>
                            <div class="col-sm-12">
                                <input type="text" 
                                        class="form-control" 
                                        placeholder="Enter text here"
                                        name="contact_fax"
                                        maxlength="1024"
                                        value="<?php echo getAdmin()->getConfig()->contact_fax;?>"/>
                            </div>
                        </div>
                        
                        <p></p>
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                อีเมล์ผู้รับ ที่ต้องการให้ผู้ใช้งานส่งหา
                            </label>
                            <div class="col-sm-12">
                                <textarea
                                    name="contact_email" 
                                    class=" form-control " 
                                    cols="10" rows="10" 
                                    maxlength="1024"
                                    placeholder="email1@domain.com,&#10;email2@domain.com,&#10;email3@domain.com,&#10;email4@domain.com,&#10;"><?php echo getAdmin()->getConfig()->contact_email;?></textarea>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            
            
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                            <i class="icon-settings font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Google map</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-sm-12 control-label">
                                Embed map URL (iframe's src)
                            </label>
                            
                            <div class="col-sm-12">
                                <textarea
                                    name="contact_map" 
                                    class=" form-control " 
                                    cols="10" rows="5" 
                                    maxlength="1024"><?php echo getAdmin()->getConfig()->contact_map;?></textarea>
                            </div>
                            <div class="col-sm-12">
                                <img src="../assets/images/admin/tutorial_map.jpg"
                                     style="max-width: 100%;
                                            margin-top: 20px;
                                            border-radius: 6px;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="form-actions noborder pull-right">
                <button type="submit" class="btn blue ">Submit</button>&nbsp;
            </div>
            <div class="clearfix"></div>
            
        </form>
        
    </div>
</div>