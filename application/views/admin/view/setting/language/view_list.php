<link rel="stylesheet" type="text/css" href="../js/chosen_v1.6.2/chosen.min.css" />
<script src="../js/chosen_v1.6.2/chosen.jquery.min.js"></script>

<div class="row">
    <div class="col-md-12 margin-bottom-25">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-language font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Languages</span>
                    <span class="caption-helper">setting</span>
                </div>
            </div>
            <div class="portlet-body">
                <form class="form-setting"
                      role="form"
                      method="post"
                      action="<?php echo $this->admin->url('setting/language_post');?>">
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            You have some form errors. Please check below.
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            Your form validation is successful!
                        </div>

                        <div class="form-group form-md-line-input">
                            <select 
                                class="form-control"
                                name="language[]" 
                                id="language"
                                data-placeholder="Choose language..."
                                multiple="multiple">
                            <?php 
                                $rs         = $this->setting_model->getSystemSupportedLanguages();
                                $supportedLangs = array();
                                if( $rs ){
                                    foreach( $rs AS $supportedLang ){
                                        $supportedLangs[] = $supportedLang['system_languages_code'];
                                    }
                                }
                                $languages  = $this->setting_model->getLanguages();
                                foreach( $languages AS $language){
                                    $disabled = '';
                                    if( is_numeric(array_search($language['code'], $supportedLangs)) ){
                                        $disabled = ' disabled="disabled" ';
                                    }
                                ?>
                                    <option value="<?php echo $language['code'];?>" <?php echo $disabled;?>>
                                        <?php echo $language['name'];?>
                                    </option>
                                <?php
                                }
                            ?>
                            </select>
                            <label class="control-label" for="language">
                                Languages 
                                <span class="required" aria-required="true">*</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-actions noborder">
                        <button type="submit" class="btn blue">Add</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 ">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-language font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Languages</span>
                    <span class="caption-helper">supported</span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample-table-2">
                    <thead>
                        <tr>
                            <th colspan="2" >ภาษา</th>
                            <th class="hidden-xs" width="130">สร้างเมื่อ</th>
                            <th class="hidden-xs" width="80">สถานะ</th>
                            <th  width="60">Tools</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?PHP
                            $len                = count($rs);
                            if( $len ){
                                foreach( $rs    AS  $row ){
                                    @shortThaiDate($row['create_date']);
                                ?>
                                    <tr>
                                        <td width="120">
                                            <?php
                                                $lang = $this->setting_model->getLanguageFromCode($row['system_languages_code']);
                                                echo $lang['code'];
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                echo $lang['name'];
                                            ?>
                                        </td>
                                        <td class="hidden-xs" align="center"><?=@$row['create_date']?></td>
                                        <td class="hidden-xs">
                                            <?PHP
                                                switch(@$row['status']){
                                                    case 1  : 
                                                        echo '<span class="label label-success">เปิดใช้งาน</span>';
                                                        break;
                                                    case 0  :
                                                        echo '<span class="label label-warning">ยังไม่เปิดใช้งาน</span>';
                                                        break;
                                                    default :
                                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                                        break;
                                                }
                                            ?>
                                        </td>
                                        <td class="hidden-xs">
                                            <?php
                                                if( $row['reserved']!='1' ){
                                                ?>
                                                    <div class="btn-group">
                                                        <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                                            <i class="icon-cog"></i> <span class="caret"></span>
                                                        </a>
                                                        <ul role="menu" class="dropdown-menu pull-right">
                                                            <li>
                                                                <?php
                                                                    if( $row['status']==1 ){
                                                                    ?>
                                                                        <a href="javascript:changeStatus('<?php echo $row['id'];?>', '0');">
                                                                            <i class="icon-edit"></i> ปิดการใช้งาน
                                                                        </a>
                                                                    <?php
                                                                    }else{
                                                                    ?>
                                                                        <a href="javascript:changeStatus('<?php echo $row['id'];?>', '1');">
                                                                            <i class="icon-edit"></i> เปิดการใช้งาน
                                                                        </a>
                                                                    <?php
                                                                    }
                                                                ?>
                                                                
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:removeLanguage(<?php echo $row['id'];?>, '<?php echo $lang['name'];?>');">
                                                                    <i class="icon-remove"></i> ลบ
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>








<script>
    $(function(){
        $('select#language').chosen();
        
        var form = $('.form-setting');
        var error2 = $('.alert-danger', form);
        var success2 = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                'language[]': {
                    required: true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success2.hide();
                error2.show();
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight

            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
            },

            submitHandler: function (form) {
                success2.show();
                error2.hide();
                form[0].submit();
            }
        });
        
    });
    
    function changeStatus(supportedLangId, status){
        if( status=='0' ){
            showConfirmDialog("Warning", "ต้องการปิดการใช้งานภาษา​ ?", function(){
                window.location = '<?php echo $this->admin->url('setting/language_changeStatus');?>/'+supportedLangId+"/"+status;
            });
        }else{
            window.location = '<?php echo $this->admin->url('setting/language_changeStatus');?>/'+supportedLangId+"/"+status;
        }
    }
    
    function removeLanguage(supportedLangId, langName){
        showConfirmDialog(
            "Warning"
            ,'<span style="color:red;">ข้อมูลที่เป็นภาษานี้จะถูกลบทันที</span><br />ต้องการลบภาษา ' +
                    langName +
                    " ออกจากระบบหรือไม่​ ?" 
            ,function(){
                window.location = '<?php echo $this->admin->url('setting/removeSupportedLanguage');?>/'+supportedLangId;
            }
        );
    }
    
</script>