<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" action="<?php echo $this->admin->url('setting/system_update');?>" method="post">
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-info"></i> Website
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="website_title"
                                           maxlength="200"
                                           value="<?php echo getAdmin()->getConfig()->website_title;?>"/>
                                    <label for="">Website's title</label>
                                    <span class="help-block">ชื่อเวปไซต์ บน Title bar</span>
                                </div>
                                
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="website_footer_copyright"
                                           maxlength="200"
                                           value="<?php echo getAdmin()->getConfig()->website_footer_copyright;?>"/>
                                    <label for="">Footer's CopyRight</label>
                                    <span class="help-block">Copy right information</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-info"></i> Company Information
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="company"
                                           maxlength="200"
                                           value="<?php echo getAdmin()->getConfig()->company;?>"/>
                                    <label for="">Company name</label>
                                    <span class="help-block">Company name</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="domain"
                                           maxlength="200"
                                           value="<?php echo getAdmin()->getConfig()->domain;?>"/>
                                    <label for="">Domain name</label>
                                    <span class="help-block">Eg. Google.com</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="email"
                                           maxlength="512"
                                           value="<?php echo getAdmin()->getConfig()->email;?>"/>
                                    <label for="">Email</label>
                                    <span class="help-block">Eg. email-name@domain.com</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="portlet box blue-hoki">
                        <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-info"></i> SEO / Other
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="seo_keywords"
                                           maxlength="512"
                                           value="<?php echo getAdmin()->getConfig()->seo_keywords;?>"/>
                                    <label for="">SEO Keywords</label>
                                    <span class="help-block">สามารถใส่ได้หลายคำ และแบ่งคำโดยใช้เครื่องหมายจุลภาค “,”</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="seo_description"
                                           maxlength="512"
                                           value="<?php echo getAdmin()->getConfig()->seo_description;?>"/>
                                    <label for="">SEO Description</label>
                                    <span class="help-block">ใช้สำหรับแสดงรายละเอียดสั้น ๆ ของหน้าเว็บไซต์ที่กำลังแสดงผลอยู่ ไม่ควรเขียนให้สั้น หรือ ยาวจนเกินไป ข้อความที่เขียนควรสัมพันธ์กับเนื้อหาของหน้านั้น ๆ</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="seo_copyright"
                                           maxlength="512"
                                           value="<?php echo getAdmin()->getConfig()->seo_copyright;?>"/>
                                    <label for="">SEO CopyRight</label>
                                    <span class="help-block">ใส่ ชื่อผู้จัดทำ หรือชื่อหน่วยงาน สถาบัน บริษัทที่จัดทำ และอาจใส่ถ้อยความแสดงความเป็นเจ้าของ</span>
                                </div>
                                
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Enter text here"
                                           name="seo_author"
                                           maxlength="512"
                                           value="<?php echo getAdmin()->getConfig()->seo_author;?>"/>
                                    <label for="">SEO Author</label>
                                    <span class="help-block">ชื่อผู้สร้างเว็บเพจและให้ใส่ชื่อผู้แต่ง  สามารถใช้ชื่อจริงหรือชื่อสถาบัน</span>
                                </div>
                                
                                
                                <div class="form-group form-md-line-input">
                                    <textarea
                                        class="form-control" 
                                        placeholder="Enter here"
                                        name="analytic"
                                        rows="5"><?php echo getAdmin()->getConfig()->analytic;?></textarea>
                                    <label for="">Analytics Script</label>
                                    <span class="help-block">Eg. Google analytics</span>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>


                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>