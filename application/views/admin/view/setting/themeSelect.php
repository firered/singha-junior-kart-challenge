<?PHP
    $theme              = $setting_model->getUserConfig('theme');
?>
เลือกธีม : 
<select id="theme_select" onchange="updateTheme(this);">
    <option value="theme_black_and_white" <?=($theme=='theme_black_and_white'?'selected="selected"':'')?> >Black and White</option>
    <option value="theme_dark" <?=($theme=='theme_dark'?'selected="selected"':'')?> >Dark</option>
    <option value="theme_green" <?=($theme=='theme_green'?'selected="selected"':'')?> >Green</option>
    <option value="theme_light" <?=($theme=='theme_light'?'selected="selected"':'')?> >Light</option>
    <option value="theme_navy" <?=($theme=='theme_navy'?'selected="selected"':'')?> >Navy</option>
</select>
<script>
    function updateTheme(obj){
        $.post( '<?=base_url().$uri->segment(1)?>/setting/updateSetting/',
                {
                    field       : 'theme',
                    val         : $(obj).val()
                },function(data){
                    window.location.reload();
                });
    }
</script>