<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" 
                      enctype="multipart/form-data"
                      action="<?php echo $this->admin->url('setting/websiteSettingUpdate');?>" 
                      method="post">
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
<!--                            <div class="caption">
                                    <i class="fa fa-info"></i> Website
                            </div>-->
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="file"
                                           class="form-control" 
                                           name="website_home_logo_icon"
                                           accept="image/*" />
                                    <label for="">Website's Logo</label>
                                    <span class="help-block">ชื่อเวปไซต์ บน Title bar</span>
                                    <?php
                                        $home_logo = getAdmin()->getConfig()->website_home_logo_icon;
                                        if ($home_logo) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($home_logo);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text"
                                           class="form-control" 
                                           name="website_social_facebook"
                                           placeholder="http://domain.com"
                                           value="<?php echo getAdmin()->getConfig()->website_social_facebook;?>"/>
                                    <label for="">ลิ้งค์ Facebook</label>
                                    <span class="help-block">Social url</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text"
                                           class="form-control" 
                                           name="website_social_ig"
                                           placeholder="http://domain.com"
                                           value="<?php echo getAdmin()->getConfig()->website_social_ig;?>"/>
                                    <label for="">ลิ้งค์ Instagram</label>
                                    <span class="help-block">Social url</span>
                                </div>
                                
                                <div class="form-group form-md-line-input">
                                    <input type="text"
                                           class="form-control" 
                                           name="website_social_youtube"
                                           placeholder="http://domain.com"
                                           value="<?php echo getAdmin()->getConfig()->website_social_youtube;?>"/>
                                    <label for="">ลิ้งค์ Youtube</label>
                                    <span class="help-block">Social url</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>