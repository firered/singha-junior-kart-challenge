<div class="col-md-12">
    <!--<textarea id="nestable-output" ></textarea>-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="icon-reorder"></i>
            คลิ๊กและลาก เพื่อจัดเรียง
            <div class="panel-tools">
                <a class="btn btn-xs btn-link panel-collapse collapses" href="#"></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="dd" id="nestable">
                <ol class="dd-list">
                <?PHP
                    $menu_ls                = $this->setting_model->getAllMenu();
                    $len_menu               = count($menu_ls);
                    if( $len_menu ){
                        foreach( $menu_ls       AS $menu ){
                        ?>
                            <li class="dd-item" menu-id="<?=$menu['id']?>" data-id="<?=$menu['id']?>">
                                <div class="dd-handle"><?=$menu['name']?></div>
                                <?PHP
                                    $subMenu_ls         = $this->setting_model->getSubMenu($menu['id']);
                                    $subMenu_ls         = null;
                                    if( $subMenu_ls ){
                                        echo '<ol class="dd-list">';
                                        foreach( $subMenu_ls        AS $subMenu ){
                                        ?>
                                            <li class="dd-item" 
                                                menu-id="<?=$menu['id']?>"  
                                                submenu-id="<?=$subMenu['id']?>"
                                                data-id="<?=$subMenu['id']?>">
                                                <div class="dd-handle">
                                                    <?=$subMenu['name']?>
                                                </div>
                                            </li>
                                        <?
                                        }
                                        echo '</ol>';
                                    }
                                ?>
                            </li>
                        <?
                        }
                    }
                ?>
                </ol>
            </div>
        </div>
    </div>
</div>
<script src="assets/plugins/nestable/jquery.nestable.js"></script>
<script>
    $(function(){
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                data    = window.JSON.stringify(list.nestable('serialize'));
                //output.val(data);
                //, null, 2));
                $.post( '<?=base_url().$uri->segment(1)?>/<?=$uri->segment(2)?>/sorttingMenu_update',
                        {
                            data    : data
                        },function(data){
                            //console.log(data);
                            window.location.reload();
                        });
            } else {
                //output.val('JSON browser support required for this demo.');
            }
        };
        var runNestable = function () {
            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);
        };
        runNestable();
    });
</script>