<?=@$this->session->flashdata('msg');?>
<a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/add/">
    <i class="icon-plus"></i> เพิ่มข้อมูล
</a>&nbsp;&nbsp;&nbsp;&nbsp;
<!--<a href="<?=  base_url().$uri->segment(1).'/'.$uri->segment(2)?>/category/" class="btn btn-blue">
    <i class="icon-circle-arrow-right"></i> จัดการหมวดหมู่
</a>-->
<br /><br />
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th class="center" width="150">Logo</th>
            <th>Menu</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                foreach( $rs    AS  $row ){
                    $url        = base_url().$uri->segment(1).'/'.$uri->segment(2).'/editMenu/'.$row['id'].'/';
                ?>
                    <tr>
                        <td style="text-align:center;">
                            <?php
                                if( $row["logo"] ){
                                ?>
                                    <img src="<?=base_url($row["logo"])?>" width="60" />
                                <?php
                                }
                            ?>
                        </td>
                        <td><?=$row['name']?></td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?=$url?>">
                                            <i class="icon-edit"></i> แก้ไขข้อมูล
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:removeTopic(<?=$row['id']?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?
                }
            }
        ?>
        
    </tbody>
</table>
<br /><br />