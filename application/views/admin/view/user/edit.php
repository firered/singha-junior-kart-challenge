<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post">
    
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="cid"  type="hidden" value="<?=@$_REQUEST['cid']?>" />
    <input name="uid"   type="hidden" value="<?=@$rs['uid']?>" />
    <input name="cid"   type="hidden" value="<?=@$rs['cid']?>" />
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ชื่อ
        </label>
        <div class="col-sm-9">
            <input name="firstname" type="text" id="form-field-7" class="form-control" value="<?=$rs['firstname']?>">
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                นามสกุล
        </label>
        <div class="col-sm-9">
            <input name="lastname" type="text" id="form-field-7" class="form-control" value="<?=$rs['lastname']?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    <br /><br />
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ชื่อเข้าใช้งาน
        </label>
        <div class="col-sm-3">
            <input name="username" type="text" id="form-field-7" class="form-control" value="<?=$rs['username']?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    
    
    <br /><br />
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="1" <?=(@$rs['status']=='1'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?=(@$rs['status']=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    
    
</form>
<div id="sample-vdo-dialog"></div>
<script src="<?=base_url()?>files/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?=  base_url()?>files/jquery-ui-1.10.3/themes/base/jquery-ui.css" />
<script>
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
    function viewSampleVDO(){
        var url             = $('input[name=youtube]').val();
        if(url!=''){
            url             = url.split('v=')[1];
        }
        $('#sample-vdo-dialog').html('<iframe width="560" height="315" src="//www.youtube.com/embed/'+url+'?rel=0" frameborder="0" allowfullscreen></iframe>');
        $('#sample-vdo-dialog').dialog({
            title           : 'ตัวอย่าง VDO',
            modal           : true,
            width           : 600,
            height          : 400
        });
    }
</script>