<?=@$this->session->flashdata('msg');?>
<a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/add/">
    <i class="icon-plus"></i> เพิ่ม Admin
</a>&nbsp;&nbsp;&nbsp;&nbsp;
<!--<a href="<?=  base_url().$uri->segment(1).'/'.$uri->segment(2)?>/category/" class="btn btn-blue">
    <i class="icon-circle-arrow-right"></i> จัดการหมวดหมู่
</a>-->
<br /><br />
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th class="center" width="30">
                <div class="checkbox-table">
                    <label>
                        <input type="checkbox" class="flat-grey">
                    </label>
                </div>
            </th>
            <th class="center" width="30">ID</th>
            <th>ชื่อ</th>
            <th width="130" style="text-align:center;">Username</th>
            <th width="130" style="text-align:center;">Email</th>
            <th class="hidden-xs" width="130" style="text-align:center;">Group</th>
            <th class="hidden-xs" width="80" >สถานะ</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                foreach( $rs    AS  $row ){
                    $url        = $this->admin->url("user/editAdmin/{$row['id']}");
                ?>
                    <tr>
                        <td class="center">
                            <div class="checkbox-table">
                                <label>
                                    <input type="checkbox" class="flat-grey" value="<?=$row['id']?>">
                                </label>
                            </div>
                        </td>
                        <td class="center"><?=$row['id']?></td>
                        <td><?=$row['firstname']?> <?=$row['middlename']?>  <?=$row['lastname']?></td>
                        <td align="center"><?=$row['username']?></td>
                        <td align="center"><?=$row['email']?></td>
                        <td class="hidden-xs" align="center" ><?=@$row['group']?></td>
                        <td class="hidden-xs">
                            <?PHP
                                switch(@$row['status']){
                                    case 1  : 
                                        echo '<span class="label label-success">ใช้งานได้</span>';
                                        break;
                                    case 0  :
                                        echo '<span class="label label-warning">ไม่สามารถใช้งาน</span>';
                                        break;
                                    default :
                                        echo '<span class="label label-danger">ลบแล้ว</span>';
                                        break;
                                }
                            ?>
                        </td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <?PHP
                                        $canEdit            = true;
                                        switch( $row['id'] ){
                                            case 1      :
                                            case 2      :
                                                $canEdit    = false;
                                                break;
                                        }
                                        if( $canEdit ){
                                        ?>
                                            <li>
                                                <a href="<?=$url?>">
                                                    <i class="icon-edit"></i> แก้ไขข้อมูล
                                                </a>
                                            </li>
                                            <li>
                                                <a href="<?=$CI->admin->url('user/changePassword/'.$row['id'])?>">
                                                    <i class="icon-edit"></i> เปลี่ยน Password
                                                </a>
                                            </li>
                                        <?
                                        }
                                        
                                        
                                        $canDelete          = true;
                                        switch($row['id']){
                                            case 1      :
                                            case 2      :
                                                $canDelete  = false;
                                                break;
                                        }
                                        if( $canDelete ){
                                        ?>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="javascript:removeTopic(<?=$row['id']?>);">
                                                    <i class="icon-remove"></i> ลบข้อมูล
                                                </a>
                                            </li>
                                        <?
                                        }
                                    ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?
                }
            }
        ?>
        
    </tbody>
</table>

<br /><div class="paginate"></div><br />
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?=$totalRow?>,
            itemsOnPage : <?=$limit?>,
            cssStyle    : 'light-theme',
            currentPage : <?=(@$_REQUEST['page']?$_REQUEST['page']:1)?>,
            hrefTextPrefix : '<?=current_url()?>/?page='
        });
    });
</script>



<script>
    $(function (){
        $('.iCheck-helper').click(function(){
            var chkBox      = $(this).parent().find('input[type=checkbox]');
            id              = chkBox.attr('topic-id');
            isSticky        = chkBox.prop('checked') ? '1' : '0';
            $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                    {
                        id      : id,
                        sticky  : isSticky
                    },function(data){});
        });
    });
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/removeAdmin/'+id;
        }
    }
    
    function setSticky( obj, id ){
        $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                {
                    id      : id,
                    sticky  : obj.checked ? '1' : '0' 
                },function(data){});
    }
</script>