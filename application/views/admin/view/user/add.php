<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="cid"  type="hidden" value="<?=@$_REQUEST['cid']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ชื่อ
        </label>
        <div class="col-sm-9">
            <input name="firstname" type="text" id="form-field-7" class="form-control" value="<?=set_value('firstname')?>">
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                นามสกุล
        </label>
        <div class="col-sm-9">
            <input name="lastname" type="text" id="form-field-7" class="form-control" value="<?=set_value('lastname')?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    <br /><br />
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ชื่อเข้าใช้งาน
        </label>
        <div class="col-sm-3">
            <input name="username" type="text" id="form-field-7" class="form-control" value="<?=set_value('username')?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รหัสผ่าน
        </label>
        <div class="col-sm-3">
            <input name="password" type="password" id="form-field-7" class="form-control"  />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รหัสผ่านอีกครั้ง
        </label>
        <div class="col-sm-3">
            <input name="password_again" type="password" id="form-field-7" class="form-control" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    
    <br /><br />
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะ
        </label>
        <div class="col-sm-3">
            <select class="form-control search-select" name="status">
                <option value="1">ใช้งานระบบได้</option>
                <option value="0">ไม่สามารถใช้งานได้</option>
            </select>
        </div>
    </div>
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    
    
</form>
<script>
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
</script>