<a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/add/?area_id=<?=@$area_id?>&task=<?=$uri->segment(3)?>">
    <i class="icon-plus"></i> เพิ่มข้อมูล
</a>&nbsp;&nbsp;&nbsp;&nbsp;
<!--<a href="<?=  base_url().$uri->segment(1).'/'.$uri->segment(2)?>/category/" class="btn btn-blue">
    <i class="icon-circle-arrow-right"></i> จัดการหมวดหมู่
</a>-->
<p>&nbsp;</p>
<?php
    $query = "  SELECT      *"
            . " FROM        footballclub_area"
            . " WHERE       status<>'2' "
            . " ORDER BY    seq ASC";
    $areas = $this->db->query($query)->result_array();
    $clsName = !@$_REQUEST['area_id'] ? 'label-default' : 'label-info';
    echo '<a href="'. $this->admin->url('schedule/showlist').'" class="label '. $clsName.'">Show all</a>';
    if ($areas) {
        foreach($areas AS $area) {
            $clsName = 'label-info';
            if (@$_REQUEST['area_id'] == $area['id']) {
                $clsName = 'label-default';
            }
            echo '&nbsp;&nbsp;<a href="'. $this->admin->url('schedule/showlist/?area_id='.$area['id']).'" class="label '.$clsName.'">'. $area['name'].'</a>'; 
        }
    }
?>

<p></p>
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <!--<th class="center" width="30">-->
<!--                <div class="checkbox-table">
                    <label>
                        <input type="checkbox" class="flat-grey">
                    </label>
                </div>-->
            <!--</th>-->
            <!--<th class="center" width="30">ID</th>-->
            <!--<th width="120">หมวดหมู่</th>-->
            <th width="60">ลำดับ</th>
            <th width="120">รูปภาพ</th>
            <th width="400">สโมสร</th>
            <th width="150">วันที่แข่งขัน</th>
            <th class="hidden-xs" width="130">สร้างเมื่อ</th>
            <th class="hidden-xs" width="80">สถานะ</th>
            <th  width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
//                pre($rs);
                foreach( $rs    AS  $row ){
                    $url        = base_url().$uri->segment(1).'/'.$uri->segment(2).'/edit/'.$row['id'].'/';
                    @shortThaiDate($row['create_date']);
                ?>
                    <tr>
                        <th style="text-align:right;"><?=$row['seq']?></th>
                        <td><img src="<?php echo base_url(@$row['display_image']);?>" style="max-width:60px;" /></td>
                        <td>
                            <?php 
                                echo "<div class='area_name label label-info'>{$row['area_name']}</div>"
                                . "<br />{$row['club_name']}"
                            ?>
                        </td>
                        <td><?php echo "{$row['club_date_match']} {$row['club_time_match']}";?></td>
                        <td class="hidden-xs" align="center"><?=@$row['create_date']?></td>
                        <td class="hidden-xs">
                            <?PHP
                                switch(@$row['status']){
                                    case 1  : 
                                        echo '<span class="label label-success">แสดงผล</span>';
                                        break;
                                    case 0  :
                                        echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                        break;
                                    default :
                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                        break;
                                }
                            ?>
                        </td>
                        <td >
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?=$url?>">
                                            <i class="icon-edit"></i> แก้ไขข้อมูล
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:removeTopic(<?=$row['id']?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?
                }
            }
        ?>
        
    </tbody>
</table>


<br /><div class="paginate"></div><br />


<style>
    .area_name{
        font-size: 12px;
        display: inline-block;
    }
    .label {
        font-size: 13px;
    }
</style>
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?=$totalRow?>,
            itemsOnPage : <?=$limit?>,
            cssStyle    : 'light-theme',
            currentPage : <?=(@$_REQUEST['page']?$_REQUEST['page']:1)?>,
            hrefTextPrefix : '<?=current_url()?>/?page='
        });
    });
</script>


<script>
    $(function (){
        $('.iCheck-helper').click(function(){
            var chkBox      = $(this).parent().find('input[type=checkbox]');
            id              = chkBox.attr('topic-id');
            isSticky        = chkBox.prop('checked') ? '1' : '0';
            $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                    {
                        id      : id,
                        sticky  : isSticky
                    },function(data){});
        });
    });
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/remove/'+id;
        }
    }
    
    function setSticky( obj, id ){
        $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                {
                    id      : id,
                    sticky  : obj.checked ? '1' : '0' 
                },function(data){});
    }
//    1805120793043606
</script>