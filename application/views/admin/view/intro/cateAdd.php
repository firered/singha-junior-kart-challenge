<div class="row">
    <div class="col-md-12">
        <?PHP
            if(@$error_message){
                echo $error_message;
            }
        ?>
        <form role="form" 
              class="form-horizontal" 
              action="<?php echo $this->admin->url("{$this->cmd}/addCategory_post");?>"
              method="post"
              enctype="multipart/form-data">
            
            <?php
                $supportedLanguages = $this->setting_model->getSystemSupportedLanguages();
                $_colors = array( 'purple', 'red', 'green', 'yellow', 'blue' );
                $colors = $_colors;
                foreach( $supportedLanguages  AS $supportedLanguage ){
                    $languageCode   = $supportedLanguage['system_languages_code'];
                    $language       = $this->setting_model->getLanguageFromCode($languageCode);
                    $color          = array_pop($colors);
                    if( count($colors)<1 ){
                        $colors = $_colors;
                    }
                ?>
            
                    <div class="portlet box <?php echo $color;?>">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-plus"></i> 
                                <?php echo "{$language['code']} - {$language['name']}";?>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?php
                                if( $supportedLanguage['status']!='1' ){
                                ?>
                                    <div class="note note-info">
                                        <h4>Info!</h4>
                                        <p>
                                            ภาษา 
                                            <span style="font-weight:bold; color: red;"><?php echo "{$language['code']} - {$language['name']}";?> </span>
                                            ยังไม่เปิดใช้งาน ข้อมูลส่วนนี้จะยังไม่แสดงผลผ่านหน้าเวป
                                        </p>
                                    </div>
                                <?php
                                }
                            ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="form-field-7">
                                    หัวข้อ
                                </label>
                                <div class="col-md-10">
                                    <input name="subject[<?php echo $language['code'];?>]" 
                                           type="text" 
                                           id="form-field-7" 
                                           class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                        สถานะการแสดงผล
                                </label>
                                <div class="col-md-10">
                                    <select class="form-control search-select" 
                                            name="status[<?php echo $language['code'];?>]">
                                        <option value="1">แสดงผลหน้าเว็บ</option>
                                        <option value="0">ไม่แสดง</option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                <?php
                }
            ?>
            
            <div style="text-align:right;">
                <button type="button"
                        class="btn btn-primary btnSubmit" 
                        style="min-width:160px;"
                        onclick="$('form').submit();">บันทึก</button>&nbsp;&nbsp;
                <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
            </div>
            <p>&nbsp;</p>
            
        </form>
    </div>
</div>