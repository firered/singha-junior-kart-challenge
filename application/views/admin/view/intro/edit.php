<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <input name="refer_url" type='hidden' value="<?=@$this->agent->referrer()?>" />

    <input name="subject" type="hidden" value="<?php echo @$subject;?>" />
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                Image
        </label>
        <div class="col-sm-9">
            <input type="file" 
                name="display_image"  
                accept="image/*" 
                class="form-control" 
                onchange="onSelectImage();"
                value=""/>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                VDO (Youtube)
        </label>
        <div class="col-sm-9">
            <input type="checkbox" 
                   name="display_vdo_menu_random" 
                   class="form-control" <?php echo @$display_vdo_menu_random==1?'checked="checked"':''?> />
            Random วิดีโอจากเมนู <a href="<?php echo base_url('admin/vdoHomePage/showlist');?>" target="_blank">VDO หน้าแรก</a>
        </div>
    </div>


    
    <?php
        if(!@$display_vdo_menu_random){
        ?>
            <p>&nbsp;</p>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-7">
                        Current Intro
                </label>
                <div class="col-sm-9">
                    <?php
                        if($display_image!=''){
                        ?>
                            <img src="<?php echo base_url($display_image);?>" style="max-width: 100%;" />
                        <?php
                        }
                        /*
                        else if ($display_vdo!=''){
                            $vdoId = explode('?', $display_vdo);
                            $vdoId = @$vdoId[1];
                            if(strstr($vdoId, '&')) {
                                $ls = explode('&', $vdoId);
                                foreach( $ls as $str){
                                    if(strstr($str, 'v=')){
                                        $vdoId = str_replace('v=', '', $str);
                                        break;
                                    }
                                }
                            }else{
                                $vdoId = str_replace('v=', '', $vdoId);
                            }
                        ?>
                            <iframe 
                                style="max-width:100%;"
                                width="560"
                                height="315" 
                                src="https://www.youtube.com/embed/<?php echo $vdoId;?>" 
                                frameborder="0" 
                                allowfullscreen></iframe>
                        <?php
                        }
                         * 
                         */
                    ?>
                </div>
            </div>
        <?php
        }
    ?>
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
        </div>
    </div>
    
    
    
    
    
</form>
<script>
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
    function onSelectImage(){
        $('input[name=display_vdo_menu_random]').attr('checked', false);
        $('input[name=display_vdo_menu_random]').parent().removeClass('checked');
    }
</script>