<a class="btn btn-primary" href="javascript:;" onclick="syncData();">
    <i class="icon-reload"></i> Sync data from Instagram
</a>
<span class="loadingContainer" style="display:none;">  
    &nbsp;&nbsp;
    <img src="../images/loading.gif" style="width: 50px;"/>
    &nbsp;Loading...
</span>
&nbsp;&nbsp;&nbsp;&nbsp;
<br /><br />
<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th class="center" width="30">ID</th>
            <th width="200">Picture</th>
            <th>Hashtag </th>
            <th class="hidden-xs" width="80">สถานะ</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                foreach( $rs    AS  $row ){
                    $data       = json_decode($row['data']);
                ?>
                    <tr>
                        <td class="center"><?php echo $row['id']?></td>
                        <td>
                            <img src="<?php echo $data->images->standard_resolution->url;?>"  style="width: 200px;"/>
                        </td>
                        <td>
                            <?php echo "<b>#{$row['hashtag']}</b>";?><br />
                            <?php
                                if( @$data->caption->text ){
                                     //echo "{$data->caption->text}";
                                }
                            ?>
                        </td>
                        <td class="hidden-xs">
                            <?PHP
                                switch(@$row['status']){
                                    case 1  : 
                                        echo '<span class="label label-success">แสดงผล</span>';
                                        break;
                                    case 0  :
                                        echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                        break;
                                    default :
                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                        break;
                                }
                            ?>
                        </td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:setStatus(<?php echo $row['id'];?>, <?php echo ($row['status']=='1' ? '0':'1');?>);">
                                            <i class="icon-edit"></i> 
                                            <?php
                                                if( $row['status']=='1' ){
                                                    echo "ไม่แสดงผล";
                                                }else{
                                                    echo "แสดง";
                                                }
                                            ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:removeTopic(<?php echo $row['id'];?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?php
                }
            }
        ?>
        
    </tbody>
</table>


<br /><div class="paginate"></div><br />
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?php echo $totalRow;?>,
            itemsOnPage : <?php echo $limit;?>,
            cssStyle    : 'light-theme',
            currentPage : <?php echo (@$_REQUEST['page']?$_REQUEST['page']:1);?>,
            hrefTextPrefix : '<?php echo current_url();?>/?page='
        });
    });
</script>


<script>
    $(function (){
        $('.iCheck-helper').click(function(){
            var chkBox      = $(this).parent().find('input[type=checkbox]');
            id              = chkBox.attr('topic-id');
            isSticky        = chkBox.prop('checked') ? '1' : '0';
            $.post( "<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky';?>",
                    {
                        id      : id,
                        sticky  : isSticky
                    },function(data){});
        });
    });
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2);?>/remove/'+id;
        }
    }
    
    function setSticky( obj, id ){
        $.post( "<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky';?>",
                {
                    id      : id,
                    sticky  : obj.checked ? '1' : '0' 
                },function(data){});
    }
    
    function setStatus( id, status ){
        $.post( "<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2).'/setStatus';?>",
                {
                    id      : id,
                    status  : status
                },function(data){
                    window.location.reload();
                });
    }
    
    function syncData(){
        $(".loadingContainer").show();
        $.post("<?php echo site_url("cron/getHashtag");?>", function(data){
            alert(data);
            window.location.reload();
        });
    }
</script>