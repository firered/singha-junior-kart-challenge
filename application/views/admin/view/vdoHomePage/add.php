<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <input name="refer_url" type='hidden' value="<?=@$this->agent->referrer()?>" />
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                Link URL
        </label>
        <div class="col-sm-9">
            <textarea name="description" 
                      onchange="showThumbnail()"
                      class=" form-control" 
                      cols="1" rows="2"><?=@$description?></textarea>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                VDO's Thumbnail
        </label>
        <div class="col-sm-9">
            <div class="fileupload-new thumbnail" style="width: 250px; height:auto;">
                <img src="<?=base_url()?>image/ratio/?file=<?=@$display_image?>&width=110&height=100&defaultText=Display image" alt="">
            </div>
            <button class="btn btn-default btn-xs" onclick="showThumbnail();" type="button">แสดงรูปจาก ​VDO</button>
        </div>
    </div>
    
    <p>&nbsp;</p>
    
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="1" <?=(@$status==1?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?=(@$status==0?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    
    
</form>

<script>
    $(function(){
        showThumbnail();
    });
    
    function showThumbnail(){
        try{
            url = $('textarea[name=description]').val().trim();
            urls = url.split('?');
            if( urls.length>1 ){
                urls = urls[1].split('&');
                for( index in urls ){
                    if(urls[index].substring(0, 2)==='v='){
                        youtubeId = urls[index].replace('v=','');
                        $('.fileupload-new > img').prop('src', 'https://img.youtube.com/vi/'+youtubeId+'/mqdefault.jpg');
                        break;;
                    }
                }
            }
        }catch(e){
            console.log(e);
        }
    }
</script>
