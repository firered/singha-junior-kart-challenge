<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" 
                      enctype="multipart/form-data"
                      action="<?php echo $this->admin->url("{$this->cmd}/system_update");?>" 
                      method="post">
                    
                    <?php /*
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                Icon VDO (หน้า Home) Hero section
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <input type="file"
                                           class="form-control" 
                                           name="website_home_vdo_icon"
                                           accept="image/*" />
                                    <!--<label for="">Icon VDO (หน้า Home) Hero section</label>-->
                                    <!--<span class="help-block">ชื่อเวปไซต์ บน Title bar</span>-->
                                    <?php
                                        $icon = getAdmin()->getConfig()->website_home_vdo_icon;
                                        if ($icon) {
                                        ?>
                                            <br />
                                            <img src="<?php echo base_url($icon);?>"
                                                style="width: auto;
                                                        max-width: 150px;"/>
                                        <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                     * 
                     */ ?>
                    
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <!--Text ที่อยู่เหนือ VDO ตรง banner-->
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <div>
                                        <div style="color: red; font-weight: bold;">ตัวอย่างการแสดงผล</div>
                                        <img src="../images/example/example_vdo_teaser.jpg"
                                             style="max-width: 60%;"/>
                                    </div>
                                    <p></p>
                                    
                                    <textarea type="text"
                                              rows="10"
                                            class="form-control" 
                                            style="border: solid 1px #c2cad8;
                                                    border-radius: 4px;
                                                    vertical-align: text-top;"
                                            name="website_home_vdo_caption"><?php echo getAdmin()->getConfig()->website_home_vdo_caption;?></textarea>
                                    <!--<label for="">Caption ใต้ Activity</label>-->
                                    <!--<span class="help-block">Caption ใต้ Activity</span>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>