<div class="row">
    <div class="col-md-12">
        <?php echo @$this->session->flashdata('msg');?>
        <a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/addCategory/">
            <i class="icon-plus"></i> เพิ่มหมวดหมู่
        </a>
        <br /><br />
        
        <table class="table table-bordered" >
            <thead>
                <tr>
                    <th class="center" width="90">Content Id</th>
                    <th width="80">ภาษา</th>
                    <th>หัวข้อ</th>
                    <th class="hidden-xs" width="130">สร้างเมื่อ</th>
                    <th class="hidden-xs" width="80">สถานะ</th>
                    <th class="" width="60">Tools</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if( !empty($rs) ){
                        foreach( $rs  AS $content ){
                            $contentId      = $content['content_id'];
                            $languageCodes  = $content['language'];
                            $data_ls        = $content['content'];
                            if( !empty($data_ls) )
                            {
                                $url        = base_url().$uri->segment(1).'/'.$uri->segment(2).'/categoryEdit/'.$contentId.'/';
                                ?>
                                <tr>
                                    <th rowspan="<?php echo count($languageCodes)+1;?>"
                                        style="text-align: right;">
                                        <?php echo $contentId;?>
                                    </th>
                                    <td colspan="4" 
                                        style=" height: 0px;
                                                padding: 0px;
                                                border: 0px;"></td>
                                    <td rowspan="<?php echo count($languageCodes)+1;?>">
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                                                <i class="fa fa-gear"></i> <span class="caret"></span>
                                            </a>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="<?php echo $url;?>">
                                                        <i class="icon-edit"></i> แก้ไขหมวดหมู่
                                                    </a>
                                                </li>
                                                <li class="divider"></li>
                                                <?php foreach($languageCodes AS $_lang){ ?>
                                                <li>
                                                    <a href="javascript:removeCategory(<?php echo $contentId;?>, '<?php echo $_lang;?>');"
                                                       class="font-red-sunglo">
                                                        <i class="fa fa-remove font-red-sunglo"></i> ลบข้อมูล <b><?php echo $_lang;?></b>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                
                                foreach( $data_ls AS $data )
                                {
                                    
                                    @shortThaiDate($data['create_date']);
                                    ?>
                                    <tr>
                                        <th><?php echo $data['system_languages_code'];?></th>
                                        <td><?php echo $data['subject'];?></td>
                                        <td class="hidden-xs" align="center"><?php echo @$data['create_date'];?></td>
                                        <td class="hidden-xs">
                                            <?PHP
                                                switch(@$data['status']){
                                                    case 1  : 
                                                        echo '<span class="label label-success">แสดงผล</span>';
                                                        break;
                                                    case 0  :
                                                        echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                                        break;
                                                    default :
                                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                                        break;
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }
                ?>
            </tbody>
        </table>
        
        
        
        
        
        
        
        
        
        
        
    </div>
</div>


<script>
    function removeCategory(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?php echo base_url().$uri->segment(1).'/'.$uri->segment(2);?>/removeCategory/'+id;
        }
    }
    
    $(function(){
        $("input[name=update-cid-seq]").blur(function(){
            var id = $(this).attr('cid');
            var seq = this.value;
            $.post("<?php echo site_url($uri->segment(1).'/'.$uri->segment(2));?>/updateCateSeq/",{
                cid : id,
                seq : seq
            });
        });
    });
    
</script>