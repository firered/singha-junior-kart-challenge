<a class="btn btn-primary" href="<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/add/?cid=<?=$cid?>&task=<?=$uri->segment(3)?>">
    <i class="icon-plus"></i> เพิ่มข้อมูล
</a>&nbsp;&nbsp;&nbsp;&nbsp;
<!--<a href="<?=  base_url().$uri->segment(1).'/'.$uri->segment(2)?>/category/" class="btn btn-blue">
    <i class="icon-circle-arrow-right"></i> จัดการหมวดหมู่
</a>-->
<p>&nbsp;</p>
<div class="row">
    <form action="<?php echo current_url();?>"
          method="get">
        <div class="col-md-3 hidden-xs"></div>
        <div class="col-md-6 col-xs-8">
            <input type="text" 
                   class="form-control" 
                   placeholder="กรอกคำค้นหา(ชื่อ, เบอร์โทร)"
                   name="search"
                   value="<?php echo @$_REQUEST['search'];?>"/>
        </div>
        <div class="col-md-2 col-xs-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </form>
</div>
<p />

<table class="table table-striped table-bordered table-hover" id="sample-table-2">
    <thead>
        <tr>
            <th width="80">ลำดับ</th>
            <th>ผู้แข่งขัน</th>
            <th width="130">เบอร์</th>
            <th width="130">คะแนน</th>
            <th class="hidden-xs" width="130">สร้างเมื่อ</th>
            <th class="hidden-xs" width="80">สถานะ</th>
            <th class="hidden-xs" width="60">Tools</th>
        </tr>
    </thead>
    <tbody>
        <?PHP
            $len                = count($rs);
            if( $len ){
                $page = @$_REQUEST['page'] ? intval($_REQUEST['page']) : 1;
                $previousCount = ($page - 1) * $limit;
                $count = $previousCount;
                foreach( $rs    AS  $row ){
                    $url        = base_url().$uri->segment(1).'/'.$uri->segment(2).'/edit/'.$row['id'].'/';
                    @shortThaiDate($row['create_date']);
                    $count++;
                ?>
                    <tr>
                        <td align="right">
                            <?php echo $count; ?>
                        </td>
                        <td><?=$row['subject']?></td>
                        <td><?php echo $row['phone'];?></td>
                        <td class="text-center"><?php echo $row['point'];?></td>
                        <td class="hidden-xs" align="center"><?=@$row['create_date']?></td>
                        <td class="hidden-xs">
                            <?PHP
                                switch(@$row['status']){
                                    case 1  : 
                                        echo '<span class="label label-success">แสดงผล</span>';
                                        break;
                                    case 0  :
                                        echo '<span class="label label-warning">ไม่แสดงผล</span>';
                                        break;
                                    default :
                                        echo '<span class="label label-danger">ยังไม่กำหนด</span>';
                                        break;
                                }
                            ?>
                        </td>
                        <td class="hidden-xs">
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" href="#">
                                    <i class="icon-cog"></i> <span class="caret"></span>
                                </a>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li>
                                        <a href="<?=$url?>">
                                            <i class="icon-edit"></i> แก้ไขข้อมูล
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:removeTopic(<?=$row['id']?>);">
                                            <i class="icon-remove"></i> ลบข้อมูล
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                <?
                }
            }
        ?>
        
    </tbody>
</table>


<br /><div class="paginate"></div><br />
<script>
    $(function() {
        $('.paginate').pagination({
            items       : <?=$totalRow?>,
            itemsOnPage : <?=$limit?>,
            cssStyle    : 'light-theme',
            currentPage : <?=(@$_REQUEST['page']?$_REQUEST['page']:1)?>,
            hrefTextPrefix : '<?=current_url()?>/?search=<?php echo @$_REQUEST['search'];?>&page='
        });
    });
</script>


<script>
    $(function (){
        $('.iCheck-helper').click(function(){
            var chkBox      = $(this).parent().find('input[type=checkbox]');
            id              = chkBox.attr('topic-id');
            isSticky        = chkBox.prop('checked') ? '1' : '0';
            $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                    {
                        id      : id,
                        sticky  : isSticky
                    },function(data){});
        });
    });
    function removeTopic(id){
        if( confirm('ต้องการลบข้อมูล ?') ){
            window.location='<?=base_url().$uri->segment(1).'/'.$uri->segment(2)?>/remove/'+id;
        }
    }
    
    function setSticky( obj, id ){
        $.post( "<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/setSticky'?>",
                {
                    id      : id,
                    sticky  : obj.checked ? '1' : '0' 
                },function(data){});
    }
//    1805120793043606
</script>