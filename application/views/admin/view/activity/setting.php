<div class="row">
   
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Setting</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" 
                      enctype="multipart/form-data"
                      action="<?php echo $this->admin->url("{$this->cmd}/system_update");?>" 
                      method="post">
                    
                    
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                    <i class="fa fa-info"></i>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="form-group form-md-line-input">
                                    <textarea type="text"
                                           class="form-control" 
                                           name="website_activity_caption"><?php echo getAdmin()->getConfig()->website_activity_caption;?></textarea>
                                    <label for="">Caption ใต้ Activity</label>
                                    <span class="help-block">Caption ใต้ Activity</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="form-actions noborder pull-right">
                        <button type="submit" class="btn blue ">Submit</button>&nbsp;
                        <button type="button" class="btn default" onclick="window.location = window.history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
        </div>
    </div>
</div>