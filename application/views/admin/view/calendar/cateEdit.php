<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ลำดับ
        </label>
        <div class="col-sm-5">
            <input name="seq" type="number" id="form-field-7" class="form-control" value="<?=@$seq?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="0" <?=(@status=='0'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="1" <?=(@status=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
</form>