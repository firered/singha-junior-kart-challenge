<div class="row">
    <?php
        $query = "  SELECT          *"
                . " FROM            system_menu"
                . " ORDER BY        seq ASC";
        $menus = $this->db->query($query)->result_array();
        foreach($menus AS $menu) {
        ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 text-center"
                     style="padding: 15px;">
                    <div class="display">
                        <h3 class="font-green-sharp">
                            <a href="<?php echo $this->admin->url($menu['url']);?>">
                                <i class="<?php echo $menu['icon'];?>"></i> <?php echo $menu['name'];?>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
        <?php
        }
    ?>
</div>