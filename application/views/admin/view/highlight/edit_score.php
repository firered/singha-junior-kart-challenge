<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <input name="refer_url" type='hidden' value="<?=@$this->agent->referrer()?>" />
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หมวดหมู่
        </label>
        <div class="col-sm-5">
            <select class="form-control" name="cid">
                <option value="">กรุณาเลือกหมวดหมู่</option>
                <?PHP
                    $cate_ls            = $model->categorylist();
                    if( $cate_ls ){
                        foreach( $cate_ls       AS $cate ){
                            $selected   = @$cid==$cate['id']?'selected="selected"':'';
                        ?>
                            <option value="<?=$cate['id']?>" <?=$selected?> ><?=$cate['subject']?></option>
                        <?
                        }
                    }
                ?>
            </select>
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพตารางคะแนน
        </label>
        <div class="col-sm-9">
            <input type="file"
                    class="form-control" 
                    name="image_score"
                    accept="image/*" />
             <?php
                 if (@$image_score) {
                 ?>
                     <br />
                     <img src="<?php echo base_url(@$image_score);?>"
                         style="width: auto;
                                 max-width: 150px;"/>
                     <p />
                 <?php
                 } else {
                 ?>
                     <br /><span class="alert-info">ตัวอย่าง</span><br />
                     <img src="../assets/images/example-score.jpg"
                         style="width: auto;
                                 max-width: 500px;"/>
                     <p />
                 <?php    
                 }
             ?>
        </div>
    </div>
    
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รายละเอียด
        </label>
        <div class="col-sm-9">
            <textarea name="description" class=" form-control tinymce" cols="10" rows="20"><?=@$description?></textarea>
        </div>
    </div>-->
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพประจำหัวข้อ
        </label>
        <div class="col-sm-9">
            <div class="fileupload-new thumbnail" style="width: 110px; height: 113px;">
                <img src="<?=base_url()?>image/ratio/?file=<?=@$display_image?>&width=110&height=100&defaultText=Display image" alt="">
                <input type="hidden" name='display_image' value="<?=@$display_image?>" />                
            </div>
            <button class="btn btn-danger btn-xs" onclick="clearDisplayImage();" type="button">ลบรูปประจำหัวข้อ</button>
        </div>
    </div>
    
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-3">
            <select class="form-control search-select" name="status">
                <option value="1" <?=(@$status=='1'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?=(@$status=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>




    <!--SEO-->
    <br /><br />
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject font-green bold uppercase">SEO</span>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-body">
                <div class="form-group  margin-top-20">
                    <label class="control-label col-md-2">Keyword</label>
                    <div class="col-md-10">
                        <div class="input-icon right" >
                            <i class="fa"></i>
                            <input type="text"
                                   class="form-control"
                                   name="seo_keywords"
                                   value="<?php echo @$seo_keywords;?>"
                                   placeholder="Keyword1, keyword2, keyword3">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-body">
                <div class="form-group  margin-top-20">
                    <label class="control-label col-md-2">Description</label>
                    <div class="col-md-10">
                        <div class="input-icon right" />
                            <i class="fa"></i>
                            <textarea name="seo_description"
                                      class=" form-control"
                                      cols="10"
                                      rows="10"
                                      style="max-width: 100%;"><?php echo @$seo_description;?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END: SEO-->
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    <!--  Upload Zone  -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="icon-cloud-upload"></i>
                    File/Image Upload Tools
                    <div class="panel-tools">
                        <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                    </div>
                </div>
                <div class="panel-body">
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="tabbable">
                                <ul id="myTab" class="nav nav-tabs tab-bricky">
                                        <li class="active">
                                            <a href="#panel-upload-image" data-toggle="tab">
                                                <i class="green clip-images-2"></i> Upload Image
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#panel-upload-file" data-toggle="tab">
                                                <i class="green clip-file"></i> Upload File
                                            </a>
                                        </li>
                                </ul>
                               
                                <div class="tab-content">
                                    <div class="tab-pane in active" id="panel-upload-image">
                                        
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                                        <span class="btn btn-file btn-light-grey">
                                                            <i class="icon-folder-open-alt"></i> 
                                                            Select And Upload Image
                                                            <input type="file" 
                                                                   onchange="uploadImage(this);" 
                                                                   accept="image/*" 
                                                                   multiple="multiple" />
                                                        </span>
                                                    </div>
                                                    <script>
                                                        function uploadImage(obj){
                                                            if( obj.files.length>0 ){
                                                                var keyName         = 'image';
                                                                var data            = new FormData();
                                                                jQuery.each(obj.files, 
                                                                            function(i, f) {
                                                                                data.append(keyName+'[]', f);
                                                                            });
                                                                data.append('table',    '<?=$table?>');
                                                                data.append('uid',      '<?=$uid?>');
                                                                data.append('type',     'i');
                                                                data.append('cmd',      '<?=$cmd?>');
                                                                $.ajax({
                                                                    url         : '<?=base_url().$uri->segment(1)?>/uploadTool/uploadImage/',
                                                                    data        : data,
                                                                    cache       : false,
                                                                    contentType : false,
                                                                    processData : false,
                                                                    type        : 'POST',
                                                                    success     : function(output){
                                                                        output      = $.parseJSON(output);
                                                                        if( output.ok.length>0 ){
                                                                            for( i=0;  i<output.ok.length;  i++ ){
                                                                                $('#panel-uploaded-image').append(output.ok[i].data);
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            
                                                        }
                                                    </script>                                      
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="icon-list"></i>
                                                        ทั้งหมด
                                                        <div class="panel-tools">
                                                            <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                                                        </div>
                                                        <div>ขนาดภาพที่แนะนำ 740 x 430px</div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="bs-example">
                                                            <div class="row" id="panel-uploaded-image">
                                                                <?PHP
                                                                    $image              = $model->getAllImage($uid);
                                                                    $len_img            = count($image);
                                                                    if( $len_img ){
                                                                        foreach( $image     AS $img ){
                                                                            $this->load->view(  'admin/view/uploadtools/imageUploaded',
                                                                                                Array(
                                                                                                    'id'        => $img['id'],
                                                                                                    'url'       => base_url().$img['filepath'],
                                                                                                    'filePath'  => $img['filepath']
                                                                                                ));
                                                                        }
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                     <!-- File -->
                                    <div class="tab-pane" id="panel-upload-file">
                                        
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div data-provides="fileupload" class="fileupload fileupload-new">
                                                        <span class="btn btn-file btn-light-grey">
                                                            <i class="icon-folder-open-alt"></i> 
                                                            Select And Upload File
                                                            <input type="file" 
                                                                   onchange="uploadFile(this);" 
                                                                   accept="application/pdf,application/x-gzip,application/x-rar-compressed,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/zip,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.slideshow,application/vnd.openxmlformats-officedocument.presentationml.presentation" 
                                                                   multiple="multiple" />
                                                        </span>
                                                    </div>
                                                    <script>
                                                        function uploadFile(obj){
                                                            if( obj.files.length>0 ){
                                                                var keyName         = 'file';
                                                                var data            = new FormData();
                                                                jQuery.each(obj.files, 
                                                                            function(i, f) {
                                                                                data.append(keyName+'[]', f);
                                                                            });
                                                                data.append('table',    '<?=$table?>');
                                                                data.append('uid',      '<?=$uid?>');
                                                                data.append('type',     'i');
                                                                data.append('cmd',      '<?=$cmd?>');
                                                                $.ajax({
                                                                    url         : '<?=base_url().$uri->segment(1)?>/uploadTool/uploadFile/',
                                                                    data        : data,
                                                                    cache       : false,
                                                                    contentType : false,
                                                                    processData : false,
                                                                    type        : 'POST',
                                                                    success     : function(output){
                                                                        output      = $.parseJSON(output);
                                                                        if( output.ok.length>0 ){
                                                                            for( i=0;  i<output.ok.length;  i++ ){
                                                                                $('#panel-uploaded-file').append(output.ok[i].data);
                                                                            }
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            
                                                        }
                                                    </script>                                      
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <i class="icon-list"></i>
                                                        ทั้งหมด
                                                        <div class="panel-tools">
                                                            <a class="btn btn-xs btn-link panel-collapse collapses"></a>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="bs-example">
                                                            <div class="row" id="panel-uploaded-file">
                                                                <?PHP
                                                                    $file              = $model->getAllFile($uid);
                                                                    $len_img            = count($file);
                                                                    if( $len_img ){
                                                                        foreach( $file     AS $img ){
                                                                            $this->load->view(  'admin/view/uploadtools/fileUploaded',
                                                                                                Array(
                                                                                                    'id'        => $img['id'],
                                                                                                    'url'       => base_url().$img['filepath'],
                                                                                                    'filePath'  => $img['filepath'],
                                                                                                    'ext'       => $img['ext'],
                                                                                                    'filename'  => $img['filename']
                                                                                                ));
                                                                        }
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                
                                
                                
                                
                                
                            </div>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!--  END Upload Zone  -->
    
    
    
</form>
<script>
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
</script>
