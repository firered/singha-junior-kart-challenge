<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    
    
    <!-- Image Icon -->
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
            Icon
        </label>
        <div class="col-sm-6">
            <div class="fileupload-new thumbnail" 
                 style="width: 40px; 
                        min-height: 20px; 
                        height: auto;
                        margin-bottom: 3px;">
                <?PHP
                    if( $image_icon ){
                    ?>
                        <img src="<?=base_url($image_icon)?>" width="30" />
                    <?
                    }
                ?>
            </div>
            <?PHP
                if( $image_icon ){
                ?>
                    <button type="button" class="btn btn-warning btn-xs">Remove</button>
                <?
                }    
            ?>
            <p class="help-block">
                ขนาด Icon ที่แนะนำ 30x18 Pixel
            </p>
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="input-group">
                    <div class="form-control uneditable-input">
                        <i class="icon-file fileupload-exists"></i>
                        <span class="fileupload-preview"></span>
                    </div>
                    <div class="input-group-btn">
                        <div class="btn btn-light-grey btn-file">
                            <span class="fileupload-new"><i class="icon-folder-open-alt"></i> Select file</span>
                            <span class="fileupload-exists"><i class="icon-folder-open-alt"></i> Change</span>
                            <input type="file" name="image_icon" class="file-input" accept="image/*">
                        </div>
                        <a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
                            <i class="icon-remove"></i> Remove
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="0" <?=(@status=='0'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="1" <?=(@status=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
</form>