<?PHP
    if(@$error_message){
        echo $error_message;
    }
?>
<form role="form" class="form-horizontal" action="" method="post">
    <input name="task"  type="hidden" value="<?=@$_REQUEST['task']?>" />
    <input name="uid"   type="hidden" value="<?=$uid?>" />
    <input name="refer_url" type='hidden' value="<?=@$this->agent->referrer()?>" />
    
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หมวดหมู่
        </label>
        <div class="col-sm-5">
            <select class="form-control" name="cid" onchange="onSelectCategory(this);">
                <option value="">กรุณาเลือกหมวดหมู่</option>
                <?PHP
                    $cate_ls            = $model->categorylist();
                    if( $cate_ls ){
                        foreach( $cate_ls       AS $cate ){
                            $selected   = @$cid==$cate['id']?'selected="selected"':'';
                        ?>
                            <option value="<?=$cate['id']?>" <?=$selected?> ><?=$cate['subject']?></option>
                        <?
                        }
                    }
                ?>
            </select>
        </div>
        <span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>
    </div>-->
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                หัวข้อ
        </label>
        <div class="col-sm-9">
            <input name="subject" type="text" id="form-field-7" class="form-control" value="<?=@$subject?>" />
        </div>
        <!--<span class="help-inline col-sm-2"> <i class="icon-info-sign"></i> Inline help text </span>-->
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                รายละเอียด
        </label>
        <div class="col-sm-9">
            <textarea name="description" class=" form-control tinymce" cols="10" rows="20"><?=@$description?></textarea>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label" for="form-field-7">
                ภาพประจำหัวข้อ
        </label>
        <div class="col-sm-9">
            <div class="fileupload-new thumbnail" style="width: 110px; height: 113px;">
                <img src="<?=base_url()?>image/ratio/?file=<?=@$display_image?>&width=110&height=100&defaultText=Display image" alt="">
                <input type="hidden" name='display_image' value="<?=@$display_image?>" />                
            </div>
            <button class="btn btn-danger btn-xs" onclick="clearDisplayImage();" type="button">ลบรูปประจำหัวข้อ</button>
        </div>
    </div>
    
    
<!--    <div class="form-group">
        <label class="col-sm-2 control-label">
                หัวข้อเด่น
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="sticky">
                <option value="0" <?=(@$sticky=='0'?'selected="selected"':'')?>>ไม่ใช่</option>
                <option value="1" <?=(@$sticky=='1'?'selected="selected"':'')?>>ใช่</option>
            </select>
        </div>
    </div>-->




    <div class="vdo-zone">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-7">
                    Youtube URL
            </label>
            <div class="col-sm-9">
                <input name="vdo_url" 
                          onchange="showThumbnail()"
                          class=" form-control" 
                          cols="1" rows="2" />
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-2 control-label" for="form-field-7">
                    VDO's Thumbnail
            </label>
            <div class="col-sm-9">
                <div class="fileupload-new thumbnail" style="width: 250px; height:auto;">
                    <img src="<?=base_url()?>image/ratio/?file=<?=@$display_image?>&width=110&height=100&defaultText=Display image" alt="">
                </div>
                <button class="btn btn-default btn-xs" onclick="showThumbnail();" type="button">แสดงรูปจาก ​VDO</button>
            </div>
        </div>
    </div>



    
    <div class="form-group">
        <label class="col-sm-2 control-label">
                สถานะการแสดงผล
        </label>
        <div class="col-sm-9">
            <select class="form-control search-select" name="status">
                <option value="1" <?=(@$status=='1'?'selected="selected"':'')?>>แสดงผลหน้าเว็บ</option>
                <option value="0" <?=(@$status=='0'?'selected="selected"':'')?>>ไม่แสดง</option>
            </select>
        </div>
    </div>
    
    
    
    <br /><br />
    <div class="form-group">
        <div class="col-sm-12" style="text-align:right;">
            <button type="submit" class="btn btn-primary" style="min-width:160px;">บันทึก</button>&nbsp;&nbsp;
            <button type="button" class="btn btn-danger" style="min-width:160px;" onclick="window.history.back();">ยกเลิกและย้อนกลับ</button>
        </div>
    </div>
    
    
    
    
    
    
</form>

<script>
    $(function(){
        showThumbnail();
    });
    function setDefaultImage(url, filePath){
        $('input[name=display_image]').val(filePath);
        $('input[name=display_image]').parent().find('img').attr('src', url);
    }
    function clearDisplayImage(){
        $('input[name=display_image]').val('');
        $('input[name=display_image]').parent().find('img').attr('src', '');
    }
    function removeImage(obj, id){
        if( confirm("Remove File ?") ){
            $.post( '<?=base_url().$uri->segment(1).'/'.$uri->segment(2).'/'.'removeFile/'?>', 
                    {
                        id      : id
                    },function(data){
                        if( data=='ok' ){
                            $(obj).parent().parent().parent().remove();
                        }
                    });
        }
    }
   
    
    function showThumbnail(){
        try{
            url = $('input[name=vdo_url]').val().trim();
            urls = url.split('?');
            if( urls.length>1 ){
                urls = urls[1].split('&');
                for( index in urls ){
                    if(urls[index].substring(0, 2)==='v='){
                        youtubeId = urls[index].replace('v=','');
                        $('.fileupload-new > img').prop('src', 'https://img.youtube.com/vi/'+youtubeId+'/mqdefault.jpg');
                        break;;
                    }
                }
            }
        }catch(e){
            console.log(e);
        }
    }
</script>
