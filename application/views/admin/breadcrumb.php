<ul class="page-breadcrumb">
    <li>
        <a href="<?php echo site_url("admin");?>">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <?PHP
        $len_breadcrumb             = count($breadcrumb);
        if( $len_breadcrumb ){
            $count = 0;
            foreach( $breadcrumb    AS $name=>$url ){
                $count++;
            ?>
                <li>
                    <span><a href="<?php echo site_url("admin/{$url}");?>"><?php echo $name;?></a></span>
                    <?php
                        if( $count<$len_breadcrumb ){
                        ?>
                            <i class="fa fa-circle"></i>
                        <?php
                        }
                    ?>
                </li>
            <?php
            }
        }
    ?>
</ul>