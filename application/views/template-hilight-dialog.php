<!doctype html>
<html>
<head>
<meta charset="utf-8">
<base href="<?php echo base_url('files');?>/" />
<title>Singha challenge</title>
	<link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
    <link href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" >
    <link href="assets/css/web.css" type="text/css" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="layout_popup">
<script src="assets/js/jquery.min.js"></script>
<script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>    
<p>&nbsp;</p>

<?php
	echo str_replace('src="files/', 'src="', $rs['description']);
?>

<script>
   $(document).ready(function(){
		$("#conditionMCustomScrollbar").mCustomScrollbar({
			axis:"y",
		});
		$("#prizesMCustomScrollbar").mCustomScrollbar({
			axis:"y",
		});
		$('#btnTabgoto').on('click',function(){
			$('.nav_condition').parent().removeClass('active');
			$('.nav_prizes').parent().addClass('active');
			
			$('#condition').removeClass('active');
			$('#prizes').addClass('active');
			console.log('ggg');
		});
	});
</script>
</body>
</html>
