<!DOCTYPE html>
<html lang="en"  class="<?php echo @$_CONTROLLER_NAME; ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1360; initial-scale=1">
    <?php
        $seoKeyword = "SINGHA KART CUP";
        if (@$_SEO_KEYWORD_PERPAGE) {
            $seoKeyword = $_SEO_KEYWORD_PERPAGE;
        } else if (@getAdmin()->getConfig()->seo_keywords) {
            $seoKeyword = getAdmin()->getConfig()->seo_keywords;
        }
        
        $seoDescription = "SINGHA KART CUP";
        if (@$_SEO_DESCRIPTION_PERPAGE) {
            $seoDescription = $_SEO_DESCRIPTION_PERPAGE;
        } else if (@getAdmin()->getConfig()->seo_description) {
            $seoDescription = getAdmin()->getConfig()->seo_description;
        }
    ?>
    <meta name="keywords" content="<?php echo $seoKeyword; ?>" />
    <meta name="description" content="<?php echo $seoDescription; ?>" />
    <meta property="fb:app_id" content="<?php echo FACEBOOK_APP_ID;?>" />
    <?php
        if(@getAdmin()->getConfig()->seo_copyright){
        ?>
            <meta name="copyright" content="<?php echo getAdmin()->getConfig()->seo_copyright; ?>" />
        <?php
        }

        $resultId = @$_REQUEST['result'];
        if ($resultId) {
            $quizResult = $this->main_model->getQuizResult($resultId);
            $quizResult['data'] = json_decode($quizResult['data']);
        ?>
            <meta property="og:url"                content="<?php echo currentUrl();?>" />
            <meta property="og:type"               content="article" />
            <meta property="og:title"              content="<?php echo $quizResult['data']->result->subject; ?>" />
            <meta property="og:description"        content="<?php echo "{$quizResult['data']->register->name} {$quizResult['data']->result->description}"; ?>" />
            <meta property="og:image"              content="<?php echo base_url($quizResult['data']->result->display_image_relative);?>" />
        <?php
        } else {
            if( @$_META_OG ){ 
                echo $_META_OG;
            }else{
            ?>
                <meta property="og:url"                content="<?php echo currentUrl();?>" />
                <meta property="og:type"               content="article" />
                <meta property="og:title"              content="<?php echo getAdmin()->getConfig()->website_title; ?>" />
                <meta property="og:description"        content="<?php echo $seoDescription; ?>" />
                <meta property="og:image"              content="<?php echo base_url("files/images/share_banner.jpg");?>" />
            <?php
            }
        }
    ?>
    <title><?php echo getAdmin()->getConfig()->website_title;?></title>
    <base href="<?php echo base_url('files');?>/" />
    <link rel="canonical" href="<?php echo base_url();?>">
    
<!--    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/site.webmanifest">
    <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#fcdd00">
    <meta name="theme-color" content="#fcdd00">-->
    
    <link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css">
    <link href="font/CooperBlackStd/stylesheet.css" rel="stylesheet">
    <link href="assets/css/video-js.css" rel="stylesheet">
    <link href="assets/css/web.css" type="text/css" rel="stylesheet">
 
     <!--[if lt IE 9]>
          <script src="assets/js/respond.min.js"></script>
     <![endif]-->
    <!-- If you'd like to support IE8 -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/videojs-ie8.min.js"></script>
    <script src="assets/js/less-1.7.0.min.js"></script>
    <script>
        function shareFacebookViaPage( href, hashtag, quote ){
            FB.ui({
                method      : 'share',
                href        : href,
                hashtag     : hashtag,
                quote       : quote
            }, function(response){
                if (response !== undefined) {
                    $.post('<?php echo site_url('service/shared')?>', {
                        shareUrl: href,
                    });
                }
            });
        }
        function gotoTop() {
            $("html, body").animate({ scrollTop: 0 });
        }
    </script>
    <?php echo getAdmin()->getConfig()->analytic;?>
</head>
<body id="page-top" class="<?php echo @$_CONTROLLER_NAME; ?>">
    <div id="wrapper">
	<?php
            $this->load->view('include/header');
            echo $_BODY;
            $this->load->view('include/footer');
        ?>
    </div>
    <?php $this->load->view('include/modal-dialog'); ?>
    
    
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo FACEBOOK_APP_ID;?>',
          xfbml      : true,
          version    : 'v2.5'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <script src="assets/js/jquery.scrollTo.min.js"></script>
    <script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
    <script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="http://vjs.zencdn.net/5.8.0/video.js"></script>
    <script src="assets/js/main_video.js"></script>
    <script src="assets/js/main_activity.js"></script>
    <script>
        <?php
            $vdos = $this->main_model->getVDO();
            $videoSourc = ($vdos && count($vdos) > 0) ? json_encode($vdos) : "[]";
        ?>
        var video_source = <?php echo $videoSourc;?>;
    </script>
    <script type="text/javascript" src="assets/js/web.js"></script>
</body>
</html>