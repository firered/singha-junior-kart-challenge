<div id="header">
    <div class="container">
        <h1><a href="<?php echo site_url('home');?>">logo</a></h1>
         <div class="socail">
             <label>follow us</label>
             <ul>
                     <li><a href="https://www.facebook.com/SinghaKartCup/" target="_blank" class="fb">fb</a></li>
                 <!--li><a href="#" class="ig">ig</a></li>
                 <li><a href="#" class="email">email</a></li-->
             </ul>
         </div>	
         <div class="wrapToggle">
                     <a class="toggleNav" id="toggleNav" href="#">toggle</a>
         </div>
    </div>
    <div class="container_nav">
         <div id="navigation">
             <div class="wrapNavigation">
                 <?php
                    $menu = $this->uri->segment(1, 'home');
                 ?>
                 <ul>
                     <li class="nav_home">
                         <a href="<?php echo site_url('home');?>" class="<?php echo $menu == 'home' ? 'active ' : '' ;?>">home </a>
                     </li>
                     <li class="nav_aboutus"><a data-toggle="modal"  data-src="<?php echo site_url('aboutus'); ?>"  data-target="#myModal" data-height='1000' data-width='100%'  class="modalButton">about us </a></li>
                     <li class="nav_conprizes"><a  data-toggle="modal"  data-src="<?php echo site_url('hilight/show/1');?>"  data-target="#myModal" data-height='1000' data-width='100%'  class="modalButton"  >condition & prizes</a></li>
                     <li class="nav_news">
                         <a href="<?php echo site_url('news');?>" id="" class="<?php echo $menu == 'news' ? 'active ' : '' ;?>">NEWS</a>
                     </li>
                     <li class="nav_gallery">
                         <a href="<?php echo site_url('gallery');?>" id="" class="<?php echo $menu == 'gallery' ? 'active ' : '' ;?>">GALLERY</a>
                     </li>
                     <li class="nav_latestupdate"><a href="#boxClips" id="btnlatestupdate" data-toggle="modal" >latest update</a></li>
                 </ul>
             </div>
         </div>
    </div>
 </div>