<div id="footer">
    <div class="container">
        <div class="copyright">
            <span><?php echo getAdmin()->getConfig()->website_footer_copyright; ?></span>
        </div>
        <div class="socail">
            <ul>
                <li><a href="https://www.facebook.com/SinghaKartCup/" target="_blank" class="fb">fb</a></li>
                <!--li><a href="#" class="ig">ig</a></li>
                <li><a href="#" class="email">email</a></li-->
            </ul>
        </div>
    </div>
</div>