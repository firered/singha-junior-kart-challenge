<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Singha challenge</title>
    <base href="<?php echo base_url('files');?>/" />
	<link href="assets/include/owl.carousel/assets/owl.carousel.css" type="text/css" rel="stylesheet">
    <link href="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.css"type="text/css" rel="stylesheet" >
	<link href="assets/css/web.css" type="text/css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    

</head>

<body class="layout_popup">
	
    <div class="boxPopup">
           <div class="wrapBox">
                <div class="bgBorderBox">
                     <div class="borderBox">
                          <div class="borderInsetBox">
                                <div class="content content_aboutus">
                                        <div class="wrapContent_aboutus">
                                            <div id="aboutusMCustomScrollbar" class="mCustomScrollbar" style="height: auto;">
                                                <div class="body">
                                                    <ul class="breadcrumb ">
                                                        <li class="breadcrumb-item">GALLERY</li> 
                                                        <li class="breadcrumb-item">PHOTO</li>
                                                        <li class="breadcrumb-item"><?php echo utf8_substr($rs['subject'], 0, 100);?></li>
                                                    </ul>
                                                    
                                                    
                                                    
                                                    <img src="<?php echo base_url(@$_REQUEST['image']);?>"
                                                         style="width: 100%;
                                                                max-height: 276px;
                                                                object-fit: contain;
                                                                object-position: center;"/>
                                                    <p></p>
                                                    <a href="<?php echo site_url('gallery/downloadImage/?file='. @$_REQUEST['image']);?>"
                                                       target="_blank"
                                                       style="float: right; position: relative;">
                                                        <img src="assets/images/btn-download-single.png" style="width: 35px;" />
                                                    </a>
                                                    <p>&nbsp;</p>
                                                    <p>&nbsp;</p>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                     </div>
                </div>
          </div>
    </div>
                    
<script src="assets/js/jquery.min.js"></script>
<script src="assets/include/owl.carousel/owl.carousel.min.js"></script>
<script src="assets/include/malihu_scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/include/bootstrap/dist/js/bootstrap.min.js"></script>
<style>
    .breadcrumb{
        background: none;
        font-family: 'Cooper Std', "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        text-align: left;
    }
    .breadcrumb-item,
    .breadcrumb-item a{
        color: #ffaa00;
        text-shadow:    2px 2px 0 #770303,
                        -1px -1px 0 #770303,
                        1px -1px 0 #770303,
                        -1px 1px 0 #770303,
                        1px 1px 0 #770303;
    }
    .breadcrumb-item a{
        text-decoration: none;
    }
    .grid-item img {
        max-width: 100%;
    }
    .grid-item{
        margin-bottom: 10px;
        border-radius: 3px;
    }
</style>
<script>
   $(document).ready(function(){
		$("#aboutusMCustomScrollbar").mCustomScrollbar({
			axis:"y",
		});
	});
</script>

</body>
</html>
