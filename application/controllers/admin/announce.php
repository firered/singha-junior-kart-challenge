<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announce extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'announce';
    public $model              = '';
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = 'announce_model';
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['table']= $this->cmd.'_file';
        $this->data['cmd']  = $this->cmd;
        $this->data['model']= &$this->{$this->model};
    }
    
    
    
    public function index(){
        redirect($this->admin->url("{$this->cmd}/edit"));
    }
    
    
    public function edit($id=1){
        $rs                 = $this->{$this->model}->getDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = 'แก้ไขข้อมูล';
        foreach( $rs        AS $k=>$v ){
            if(!@$v){ $v=''; }
            $this->data[$k] = $v;
        }
        $this->data['model']    = $this->{$this->model};
        $this->data['uid']      = $rs['uid'];
        $this->admin->addBreadcrumb(strtoupper($this->cmd), "{$this->cmd}/");
//        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("description", "รายละเอียด", "trim|required");
//        $this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( $this->cmd.'/edit', $this->data);
        }else{
            $this->{$this->cmd.'_model'}->update($id);
            redirect($this->admin->url("{$this->cmd}/edit"));
        }
    }
    
    function removeFile(){
        $id                 = $this->input->post('id');
        $this->{$this->model}->removeFile($id);
        exit('ok');
    }
    
    
}