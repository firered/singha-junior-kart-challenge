<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'setting';
    private $model              = '';
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = 'setting_model';
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['cmd']  = $this->cmd;
        $this->data[$this->model]       = &$this->{$this->model};
        $this->admin->addBreadcrumb('Setting', "setting/sortingMenu");
        //$this->data['model']= &$this->{$this->model};
    }
    
    public function index(){
        redirect($this->admin->url('setting/system/'));
    }

    public function system(){
        $this->admin->view( "setting/system_config", $this->data);
    }
    
    public function system_update(){
        if( $this->admin->isSiteRefered() ){
            $data = array(
                'company' => $this->input->post('company'),
                'domain' => $this->input->post('domain'),
                'email' => $this->input->post('email'),
                'seo_keywords' => $this->input->post('seo_keywords'),
                'seo_description' => $this->input->post('seo_description'),
                'seo_copyright' => $this->input->post('seo_copyright'),
                'seo_author' => $this->input->post('seo_author'),
                'analytic' => $this->input->post('analytic'),
                'website_footer_copyright' => $this->input->post('website_footer_copyright'),
                'website_title' => $this->input->post('website_title'),
            );
            $this->setting_model
                    ->updateSettings( $data );
            $this->admin
                    ->setFlashMessageAlert(
                        $this->admin->getAlertView_success("Update complete")
                    );
            redirect( $this->admin->url('setting/system') );
        }else{
            redirect( $this->admin->url('setting/system') );
            return;
        }
    }
   
    
    public function iconMenu(){
        $this->data['menuName']     = 'เลือกธีมระบบจัดการหลังบ้าน';
        $this->data['rs']           = $this->{$this->model}->getAllMenu();
        $this->admin->view('setting/menulist', $this->data);
    }
    
    
    
    public function language(){
        $this->admin->view('setting/language/view_list');
    }
    
    public function language_post(){
        if( getAdmin()->isSiteRefered() ){
            $langs = $this->input->post('language');
            if( $langs
                    && is_array($langs)
                    && count($langs)>0 ){
                $this->setting_model->addSystemSupportedLanguage( $langs );
            }
        }
        redirect( $this->admin->url('setting/language') );
    }
    
    public function language_changeStatus(){
        if( $this->admin->isSiteRefered() ){
            $supportedLanguageId    = $this->uri->segment(4);
            $status                 = $this->uri->segment(5);
            $this->setting_model->changeSupportedLanguageStatus($supportedLanguageId, $status);
        }
        redirect( $this->admin->url('setting/language') );
    }
    
    public function removeSupportedLanguage(){
        if( $this->admin->isSiteRefered() ){
            $supportedLanguageId    = $this->uri->segment(4, 0);
            if( $supportedLanguageId ){
                $data = $this->setting_model->getSupportedLanguageById($supportedLanguageId);
                if( $data && $data['reserved']!='1'){
                    $this->setting_model->removeLanguageFromSupportedSystem($data['system_languages_code']);
                    $this->admin
                            ->setFlashMessageAlert( 
                                $this->admin
                                    ->getAlertView_success("Remove complete") 
                            );
                }else{
                    $this->admin
                            ->setFlashMessageAlert( 
                                $this->admin
                                    ->getAlertView_error(
                                                "Data not found ( Supported language ID: {$supportedLanguageId} )"
                                            ) 
                            );
                }
            }
        }
        redirect( $this->admin->url('setting/language') );
    }
    
    
    public function websiteSetting(){
        $this->admin->view( "{$this->cmd}/website_setting", $this->data);
    }
    
    public function websiteSettingUpdate(){
        if( $this->admin->isSiteRefered() ){
            $data = array();
            
            $key = 'website_home_logo_icon';
            if( @$_FILES[$key] ){
                $this->load->library('upload');
                $dirPath                = "files/upload/{$this->cmd}/";
                @mkdir($dirPath, 0777, true);
                $name                   = $_FILES[$key]['name'];
                $config                 = Array();
                $config['upload_path']  = $dirPath;
                $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= 1024*5;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                $uploadResult = $this->upload->do_upload($key);
                if ($uploadResult) {
                    $uploaded           = $this->upload->data();
                    @chmod($uploaded['full_path'], 0777);
                    $filePath           = "{$dirPath}{$uploaded['file_name']}";
                    $data['website_home_logo_icon'] = $filePath;
                } else {
                    $this->admin->setFlashMessageAlert(
                        $this->admin->getAlertView_error("Can not upload image")
                    );
                }
            }
            $data['website_social_facebook'] = $this->input->post('website_social_facebook');
            $data['website_social_ig'] = $this->input->post('website_social_ig');
            $data['website_social_youtube'] = $this->input->post('website_social_youtube');
            $this->setting_model->updateSettings( $data );
            $this->admin->setFlashMessageAlert(
                $this->admin->getAlertView_success("Update complete")
            );
            redirect( $this->admin->url("setting/websiteSetting") );
        }else{
            redirect( $this->admin->url("setting/websiteSetting") );
            return;
        }
    }
}