<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authen extends CI_Controller {
    
    public function index(){
        
    }
    
    
    
    public function login(){
        $this->admin->loginView();
    }
    
    
    public function signIn(){
        if( @$_SERVER['HTTP_REFERER']   && strstr($_SERVER['HTTP_REFERER'], base_url()) ){
            $username           = $this->input->post('username');
            $password           = md5($this->input->post('password'));
            $this->db->select('*'); 
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            $this->db->where('group', 'admin');
            $this->db->where('is_root', '1');
            $user               = $this->db->get('system_user')->row_array();
            if( $user ){
                $this->session->set_userdata($user);
                redirect(base_url().'admin/');
            }else{
                $this->admin->loginView(array(
                    'msg'=> 'ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบ Username/password อีกครั้ง'
                ));
            }
        }else{
            $this->session->set_flashdata('msg', '');
            header('Location: '.base_url().'admin/authen/login/');
            exit();
        }
    }
    
    
    public function logout(){
        $this->session->set_userdata('id',      null);
        $this->session->set_userdata('username',null);
        redirect($this->admin->url());
    }
    
    
    
}