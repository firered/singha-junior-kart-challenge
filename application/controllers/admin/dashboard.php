<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    
    public function __construct() {
        parent::__construct();
        $this->admin->addBreadcrumb('Dashboard', "dashboard");
    }
    
    
    public function index(){
        $this->admin->view('dashboard/list');
    }
    
    
    
}