<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'banner';
    public $model              = '';
    private $title = array(
        'list' => 'Banner',
        'add' => 'เพิ่มข้อมูล',
        'edit' => 'แก้ไขข้อมูล',
        'category_list' => 'จัดการข้อมูลหมวดหมู่',
        'category_add' => 'เพิ่มข้อมูลหมวดหมู่',
        'category_edit' => 'แก้ไขข้อมูลหมวดหมู่',
    );
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = 'banner_model';
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['table']= $this->cmd.'_file';
        $this->data['cmd']  = $this->cmd;
        $this->data['model']= &$this->{$this->model};
    }
    
    
    
    public function index(){
        redirect($this->admin->url("{$this->cmd}/showlist"));
    }
    
    
    public function showlist($cid=0){
        $this->admin->addBreadcrumb($this->title['list'], "{$this->cmd}/");
        $totalRow               = $this->{$this->model}->getTotalRecord($cid);
        $totalRow               = $totalRow?$totalRow:1;
        $limit                  = 20;
        $currentPage            = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
        $startRecord            = ($currentPage-1)*$limit;   
        $this->data['totalRow'] = $totalRow;
        $this->data['limit']    = $limit;
        
        $cidObj                    = $this->{$this->model}->getCategoryDetail($cid);
        if( $cid ){
            $this->data['menuName'] = $cidObj['subject'];
            $this->admin->addBreadcrumb($cidObj['subject'], "{$this->cmd}/".$this->uri->segment(3));
        }else{
            $this->data['menuName'] = $this->title['list'];
        }
        
        $this->data['cid']      = $cid;
        $this->data['rs']       = $this->{$this->model}->showList($cid, Array($startRecord, $limit));
        $this->admin->view( $this->cmd. '/showlist', $this->data);
    }
    
    public function add(){
        $cid                    = 2;
        $this->data['menuName'] = $this->title['add'];
        $this->data['cid']      = $cid;
        $this->data['uid']      = md5(time());
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
//        $this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( $this->cmd. '/edit', $this->data);
        }else{
            $id                 = $this->{$this->cmd.'_model'}->insert();
            $url                = @$_REQUEST['refer_url'] ? $_REQUEST['refer_url'] : base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->get('task');
            redirect($url);
        }
    }
    
    
    public function edit(){
        $id                 = $this->uri->segment(4);
        $rs                 = $this->{$this->model}->getDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = $this->title['edit'];
        foreach( $rs        AS $k=>$v ){
            if(!@$v){ $v=''; }
            $this->data[$k] = $v;
        }
        $this->data['model']    = $this->{$this->model};
        $this->data['uid']      = $rs['uid'];
        $this->admin->addBreadcrumb('ข่าว', "{$this->cmd}/");
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
//        $this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( $this->cmd. '/edit', $this->data);
        }else{
            $this->{$this->cmd.'_model'}->update($id);
            $url                = @$_REQUEST['refer_url'] ? $_REQUEST['refer_url'] : base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->get('task');
            redirect($url);
        }
    }
    
    
    public function remove(){
        $id             = $this->param(3);
        $this->{$this->model}->remove($id);
        $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url().$this->segment(1).'/'.$this->segment(2));
    }
    
    
    function setSticky(){
        $this->{$this->model}->setSticky(
                $this->input->post('id'),
                $this->input->post('sticky')
        );
        exit();
    }
    
    
    function removeFile(){
        $id                 = $this->input->post('id');
        $this->{$this->model}->removeFile($id);
        exit('ok');
    }
    
    
    
    
    
    public function category(){
        $cid                    = 2;
        $this->data['menuName'] = $this->title['category_list'];
        $this->data['rs']       = $this->{$this->model}->categorylist($cid);
        
//        exit(pre($this->data['rs']));
        $this->admin->addBreadcrumb('ข่าว', "{$this->cmd}/");
        $this->admin->addBreadcrumb('หมวดหมู่', "{$this->cmd}/category/");
        $this->admin->view( $this->cmd. '/cateList', $this->data);
    }
    
    
    public function removeCategory(){
        $id             = $this->uri->segment(4);
        if( $this->{$this->model}->categoryIsCanRemove($id)){
            $this->{$this->model}->categoryRemove($id);
            $msg        = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
        }else{
            $msg        = '<div class="alert alert-danger">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-remove-sign"></i>
                                <strong>Warning</strong> ไม่สามารถข้อมูลได้
                             </div>';
        }
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url().$this->segment(1).'/'.$this->segment(2).'/category');
    }
    
    
    public function addCategory(){
        $cid                    = 2;
        $this->data['menuName'] = $this->title['category_add'];
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->param(1)}/");
        $this->admin->addBreadcrumb('จัดการหมวดหมู่', "{$this->param(1)}/category/");
        $this->admin->view( "{$this->cmd}/cateAdd", $this->data);
        
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
        
        //$this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
    }
    
    public function addCategory_post(){
        if( $this->admin->isSiteRefered() ){
            $data       = array();
            $subjects   = $this->input->post('subject');
            $status     = $this->input->post('status');

            if( !empty($subjects) ){
                $langCodes = array();
                foreach( $subjects  AS $languageCode => $value ){
                    if( !empty( $value ) ){
                        $langCodes[] = $languageCode;
                        $data[] = array(
                            'subject' => $value,
                            'system_languages_code' => $languageCode,
                            'status' => $status[$languageCode]
                        );
                    }
                }
                if( !empty($data) ){
                    $this->{$this->model}->insertCategory($data);
                    $this->admin->setFlashMessageAlert(
                                $this->admin->getAlertView_success("Create category (".  implode(',', $langCodes).") complete")
                            );
                }else{
                    $this->admin->setFlashMessageAlert(
                                $this->admin->getAlertView_error('Can not create category')
                            );
                }
            }
        }
        redirect(base_url().$this->segment(1).'/'.$this->segment(2).'/category/');
    }
    
    
    public function categoryEdit(){
        $id                 = $this->param(3);
        $rs                 = $this->{$this->model}->getCategoryDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = $this->title['category_edit'];
        foreach( $rs        AS $k=>$v ){
            if(!@$v){ $v=''; }
            $this->data[$k] = $v;
        }
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( "{$this->cmd}/cateEdit", $this->data);
        }else{
            $this->{$this->model}->updateCategory($id);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/category/');
        }
    }
    
    
    
    public function updateCateSeq(){
        $id = $_REQUEST["cid"];
        $seq =$_REQUEST['seq'];
        $this->{$this->model}->getCategorySeq($id, $seq);
    }
    
}