<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'user';
    private $model              = '';
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = 'user_model';
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['table']= $this->cmd.'_file';
        $this->data['cmd']  = $this->cmd;
        $this->data['model']= &$this->{$this->model};
        $this->data['CI']   = &$this;
    }
    
    
    
    public function index(){
        $this->showlist();
    }
    
    
    
    public function showlist(){
        $totalRow               = $this->{$this->model}->getTotalAdmin();
        $totalRow               = $totalRow?$totalRow:1;
        $limit                  = 10;
        $currentPage            = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
        $startRecord            = ($currentPage-1)*$limit;   
        $this->data['totalRow'] = $totalRow;
        $this->data['limit']    = $limit;
        
        $this->data['menuName'] = 'จัดการข้อมูล';
        $this->data['rs']       = $this->{$this->model}->getAdminUser( Array($startRecord, $limit));
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->cmd}/");
        $this->admin->view( "{$this->cmd}/showlist", $this->data);
    }
    
    
    public function add(){
        $this->data['menuName'] = 'เพิ่มข้อมูล';
        $this->data['uid']      = md5(time());
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->cmd}/");
        $this->form_validation->set_rules("firstname", "ชื่อ", "trim|required");
        $this->form_validation->set_rules("lastname", "นามสกุล", "trim|required");
        $this->form_validation->set_rules("password", "รหัสผ่าน", "trim|required|min_length[5]");
        $this->form_validation->set_rules("password_again", "รหัสผ่านครั้งที่สอง", "trim|required|min_length[5]");
        $this->form_validation->set_rules("username", "ชื่อเข้าใช้งานระบบ", "trim|required");
        //$this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false || $this->input->post('password')!=$this->input->post('password_again') ){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            if( $this->input->post('password')!=$this->input->post('password_again') ){
                $this->data['error_message'] = '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> รหัสผ่านทั้ง 2 ไม่ตรงกัน</div>';
            }
            $this->admin->view( "{$this->cmd}/add", $this->data);
        }else{
            $id                 = $this->{$this->model}->insertAdmin();
            redirect($this->admin->url($this->cmd));
        }
    }
    
    
    public function editAdmin(){
        $id                 = $this->uri->segment(4);
        if( $id<3  && $id!=$this->session->userdata('id') ){
            exit('Permission denie.');
        }
        $rs                 = $this->{$this->model}->getAdminDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = 'แก้ไขข้อมูล';
        $this->data['rs']       = $rs;
        $this->data['model']    = $this->{$this->model};
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->cmd}/");
        $this->form_validation->set_rules("firstname", "ชื่อ", "trim|required");
        $this->form_validation->set_rules("lastname", "นามสกุล", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( "{$this->cmd}/edit", $this->data);
        }else{
            $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> แก้ไขข้อมูลแล้ว
                             </div>';
            $this->session->set_flashdata('msg', $msg);
            $this->{$this->model}->updateAdmin($id);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->get('task'));
        }
    }
    
    
    public function removeAdmin(){
        $id                 = $this->uri->segment(4);
        if( $id>2 ){
            $this->{$this->model}->removeAdmin($id);
            $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
            $this->session->set_flashdata('msg', $msg);
        }
        redirect($this->admin->url($this->cmd));
    }
    
    
    public function changePassword(){
        $this->data['menuName'] = "เปลี่ยนรหัสผ่าน";
        $id                 = $this->session->userdata('id');
        $rs                 = $this->{$this->model}->getAdminDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['rs']       = $rs;
        $this->data['model']    = $this->{$this->model};
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->cmd}/");
        $this->form_validation->set_rules("password", "รหัสผ่าน", "trim|required|min_length[5]");
        $this->form_validation->set_rules("password_again", "รหัสผ่านครั้งที่สอง", "trim|required|min_length[5]");
        if($this->form_validation->run()===false || $this->input->post('password')!=$this->input->post('password_again')){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            if( $this->input->post('password')!=$this->input->post('password_again') ){
                $this->data['error_message'] = '<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> รหัสผ่านทั้ง 2 ไม่ตรงกัน</div>';
            }
            $this->admin->view( "{$this->cmd}/change_password", $this->data);
        }else{
            $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> แก้ไขรหัสผ่านแล้ว
                             </div>';
            $this->session->set_flashdata('msg', $msg);
            $this->{$this->model}->changePasswordAdmin($id);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->input->get('task'));
        }
    }
    
    
    
    
    
}