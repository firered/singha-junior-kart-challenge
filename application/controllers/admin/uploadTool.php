<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UploadTool extends CI_Controller {
    
    private $dir                = 'files/upload/';
    private $currentUploadDir   = '';
    private $maxFileSize        = 0;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('upload');
        $this->maxFileSize      = 1024*4;
    }
    
    
    public function uploadImage(){
        $fieldName              = 'image';
        $files                   = @$_FILES[$fieldName];
        if( count(@$files['name']) ){
            $output             = Array();
            $uid                = $this->input->post('uid');
            $type               = $this->input->post('type');
            $cmd                = $this->input->post('cmd');
            $table              = $this->input->post('table');
            $supportedLanguages = $this->input->post('supported_languages');
            $this->currentUploadDir= "{$this->dir}{$cmd}/".date('Y-m-d').'/';
            @mkdir($this->currentUploadDir, 0777, true);
            $len                = count($files['name']);
            for( $i=0;  $i<$len;  $i++ ){
                $name                   = $files['name'][$i];
                $tmp_name               = $files['tmp_name'][$i];
                $_FILES[$fieldName]     = Array();
                $_FILES[$fieldName]['name']     = $files['name'][$i];
                $_FILES[$fieldName]['type']     = $files['type'][$i];
                $_FILES[$fieldName]['tmp_name'] = $files['tmp_name'][$i];
                $_FILES[$fieldName]['error']    = $files['error'][$i];
                $_FILES[$fieldName]['size']     = $files['size'][$i]; 
                $name_new               = date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config                 = Array();
                $config['upload_path']  = $this->currentUploadDir;
                $config['file_name']	= $name_new;
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= $this->maxFileSize;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                if ($this->upload->do_upload($fieldName)) {
                    $uploaded               = $this->upload->data();
                    list($width, $height, $_type, $attr) = getimagesize($this->currentUploadDir.$uploaded['orig_name']);
                    $this->db->set('uid',       $uid);
                    $this->db->set('filename',  $name);
                    $this->db->set('filepath',  $this->currentUploadDir.$uploaded['orig_name']);
                    $this->db->set('type',      $type);
                    $this->db->set('ext',       str_replace('.', '', $uploaded['file_ext']));
                    $this->db->set('size',      $uploaded['file_size']*1024);
                    $this->db->set('create_date',   date('y-m-d H:i:s'));
                    $this->db->set('create_ip',     $this->input->ip_address());
                    $this->db->set('create_by',     $this->session->userdata('id'));
                    $this->db->set('sticky',    '0');
                    $this->db->set('status',    '1');
                    $this->db->set('width',    $width);
                    $this->db->set('height',    $height);
                    
                    $this->db->insert($table);
                    $imgId                  = $this->db->insert_id();                     
                    ob_start();
                    $this->admin->getView('uploadtools/imageUploaded', Array(
                        'url'       => base_url().'image/ratio/?file='.$this->currentUploadDir.$uploaded['orig_name'].'&amp;width=160&amp;height=110&amp;defaultText=Image%20not%20found.',
                        'filePath'  => $this->currentUploadDir.$uploaded['orig_name'],
                        'id'        => $imgId,
                        'supportedLanguages' => explode(',', $supportedLanguages)
                    ));
                    $imgHTML                = ob_get_clean();
                    $output['ok'][]    = Array(
                        'filename'          => $name,
                        'filepath'          => $this->currentUploadDir.$uploaded['orig_name'],
                        'sticky'            => 0,
                        'data'              => $imgHTML
                    );   
                } else {
                    $output['error'][]      = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
                }
            }
        }
        exit(json_encode($output));
    }
    
    
    
    public function uploadFile(){
        $maxFileSize      = MAX_FILE_UPLOAD_SIZE / 1024;
        $fieldName              = 'file';
        $files                   = @$_FILES[$fieldName];
        if( count(@$files['name']) ){
            $output             = Array();
            $uid                = $this->input->post('uid');
            $type               = 'f';
            $cmd                = $this->input->post('cmd');
            $table              = $this->input->post('table');
            $supportedLanguages = $this->input->post('supported_languages');
            $this->currentUploadDir= "{$this->dir}{$cmd}/".date('Y-m-d').'/';
            @mkdir($this->currentUploadDir, 0777, true);
            $len                = count($files['name']);
            for( $i=0;  $i<$len;  $i++ ){
                $name                   = $files['name'][$i];
                $tmp_name               = $files['tmp_name'][$i];
                $_FILES[$fieldName]     = Array();
                $_FILES[$fieldName]['name']     = $files['name'][$i];
                $_FILES[$fieldName]['type']     = $files['type'][$i];
                $_FILES[$fieldName]['tmp_name'] = $files['tmp_name'][$i];
                $_FILES[$fieldName]['error']    = $files['error'][$i];
                $_FILES[$fieldName]['size']     = $files['size'][$i]; 
                $name_new               = date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config                 = Array();
                $config['upload_path']  = $this->currentUploadDir;
                $config['file_name']	= $name_new;
                $config['allowed_types']= 'pdf|doc|docx|xls|xlsx|ppt|pptx|zip|rar|bz2|gz|tar|7zip|swf|mp4|webm|ogv|ogg';
                $config['max_size']	= $maxFileSize;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                if ($this->upload->do_upload($fieldName)) {
                    $uploaded               = $this->upload->data();
                    $this->db->set('uid',       $uid);
                    $this->db->set('filename',  $name);
                    $this->db->set('filepath',  $this->currentUploadDir.$uploaded['orig_name']);
                    $this->db->set('type',      $type);
                    $this->db->set('ext',       str_replace('.', '', $uploaded['file_ext']));
                    $this->db->set('size',      $uploaded['file_size']*1024);
                    $this->db->set('create_date',   date('y-m-d H:i:s'));
                    $this->db->set('create_ip',     $this->input->ip_address());
                    $this->db->set('create_by',     $this->session->userdata('id'));
                    $this->db->set('sticky',    '0');
                    $this->db->set('status',    '1');
                    $this->db->insert($table);
                    $imgId                  = $this->db->insert_id();                     
                    ob_start();
                    $this->admin->getView('uploadtools/fileUploaded', Array(
                        'url'       => base_url().$this->currentUploadDir.$uploaded['orig_name'],
                        'filePath'  => $this->currentUploadDir.$uploaded['orig_name'],
                        'id'        => $imgId,
                        'ext'       => str_replace('.', '', $uploaded['file_ext']),
                        'filename'  => $name,
                        'supportedLanguages' => explode(',', $supportedLanguages)
                    ));
                    $imgHTML                = ob_get_clean();
                    $output['ok'][]    = Array(
                        'filename'          => $name,
                        'filepath'          => $this->currentUploadDir.$uploaded['orig_name'],
                        'sticky'            => 0,
                        'data'              => $imgHTML
                    );   
                } else {
                    $output['error'][]      = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
                }
            }
        }
        exit(json_encode($output));
    }
    
    
    public function ajax_getUploadPrepare_image(){
        $this->admin->getView('uploadtools/imagePrepareUpload');
    }
    
    
    
    
    
    public function uploadImage_ballonline(){
        $fieldName              = 'image';
        $files                   = @$_FILES[$fieldName];
        if( count(@$files['name']) ){
            $output             = Array();
            $uid                = $this->input->post('uid');
            $type               = $this->input->post('type');
            $cmd                = $this->input->post('cmd');
            $table              = $this->input->post('table');
            $this->currentUploadDir= "{$this->dir}{$cmd}/".date('Y-m-d').'/';
            @mkdir($this->currentUploadDir, 0777, true);
            $len                = count($files['name']);
            for( $i=0;  $i<$len;  $i++ ){
                $name                   = $files['name'][$i];
                $tmp_name               = $files['tmp_name'][$i];
                $_FILES[$fieldName]     = Array();
                $_FILES[$fieldName]['name']     = $files['name'][$i];
                $_FILES[$fieldName]['type']     = $files['type'][$i];
                $_FILES[$fieldName]['tmp_name'] = $files['tmp_name'][$i];
                $_FILES[$fieldName]['error']    = $files['error'][$i];
                $_FILES[$fieldName]['size']     = $files['size'][$i]; 
                $name_new               = date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config                 = Array();
                $config['upload_path']  = $this->currentUploadDir;
                $config['file_name']	= $name_new;
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= $this->maxFileSize;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                if ($this->upload->do_upload($fieldName)) {
                    $uploaded               = $this->upload->data();
                    $this->db->set('uid',       $uid);
                    $this->db->set('filename',  $name);
                    $this->db->set('filepath',  $this->currentUploadDir.$uploaded['orig_name']);
                    $this->db->set('type',      $type);
                    $this->db->set('ext',       str_replace('.', '', $uploaded['file_ext']));
                    $this->db->set('size',      $uploaded['file_size']*1024);
                    $this->db->set('create_date',   date('y-m-d H:i:s'));
                    $this->db->set('create_ip',     $this->input->ip_address());
                    $this->db->set('create_by',     $this->session->userdata('id'));
                    $this->db->set('sticky',    '0');
                    $this->db->set('status',    '1');
                    $this->db->insert($table);
                    $imgId                  = $this->db->insert_id();                     
                    ob_start();
                    $this->admin->getView('uploadtools/imageUploaded_ballonline', Array(
                        'url'       => base_url().'image/ratio/?file='.$this->currentUploadDir.$uploaded['orig_name'].'&amp;width=128&amp;height=110&amp;defaultText=Image%20not%20found.',
                        'filePath'  => $this->currentUploadDir.$uploaded['orig_name'],
                        'id'        => $imgId
                    ));
                    $imgHTML                = ob_get_clean();
                    $output['ok'][]    = Array(
                        'filename'          => $name,
                        'filepath'          => $this->currentUploadDir.$uploaded['orig_name'],
                        'sticky'            => 0,
                        'data'              => $imgHTML
                    );   
                } else {
                    $output['error'][]      = $this->upload->display_errors('<div class="alert alert-error">', '</div>');                
                }
            }
        }
        exit(json_encode($output));
    }
    
    
    
    
}