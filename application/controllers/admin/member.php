<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'member';
    public $model              = '';
    public $name                = "Member";
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = 'member_model';
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['table']= 'member';
        $this->data['cmd']  = $this->cmd;
        $this->data['model']= &$this->{$this->model};
    }
    
    
    
    public function index(){
        redirect($this->admin->url("{$this->cmd}/showlist"));
    }
    
    
    public function showlist($cid=0){
        $this->admin->addBreadcrumb($this->name, "{$this->cmd}/");
        $totalRow               = $this->{$this->model}->getTotalRecord($cid);
        $totalRow               = $totalRow?$totalRow:1;
        $limit                  = 20;
        $currentPage            = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
        $startRecord            = ($currentPage-1)*$limit;   
        $this->data['totalRow'] = $totalRow;
        $this->data['limit']    = $limit;
        $this->data['rs']       = $this->{$this->model}->showList($cid, Array($startRecord, $limit));
        $this->admin->view( $this->cmd.'/showlist', $this->data);
    }
    
    
    public function remove(){
        $id             = $this->param(3);
        $this->{$this->model}->remove($id);
        $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url().$this->segment(1).'/'.$this->segment(2));
    }
    
    
    function setSticky(){
        $this->{$this->model}->setSticky(
                $this->input->post('id'),
                $this->input->post('sticky')
        );
        exit();
    }
    
    function setStatus(){
        $this->{$this->model}->setStatus(
                $this->input->post('id'),
                $this->input->post('status')
        );
        exit();
    }
    
    
    function export(){
        $query = "  SELECT      * "
                . " FROM        question_result "
                . " WHERE       status='1' "
                . " ORDER BY    create_date DESC";
        $rs = $this->db->query($query)->result_array();
        
        $strExcelFileName="Member-All-".date("Y-m-d").".xls";
        header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
        header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
        header("Pragma:no-cache");
        echo $this->admin->getView("member/export_excel", array( 'rs' => $rs ));
    }
    
    function exportShareFacebook(){
        $query = "  SELECT      * "
                . " FROM        question_result "
                . " WHERE       status='1' AND "
                . "             shared_fb_success='1' "
                . " ORDER BY    create_date DESC";
        $rs = $this->db->query($query)->result_array();
        
        $strExcelFileName="Member-ShareFacebook-".date("Y-m-d").".xls";
        header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
        header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
        header("Pragma:no-cache");
        echo $this->admin->getView("member/export_excel", array( 'rs' => $rs ));
    }
    
}