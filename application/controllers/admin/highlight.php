<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Highlight extends CI_Controller {
    
    private $data               = Array();
    public $cmd                = 'highlight';
    public $model              = '';
    public $menuName            = 'Highlight';
    
    
    public function __construct() {
        parent::__construct();
        $this->model        = "{$this->cmd}_model";
        $this->load->model("{$this->uri->segment(1)}/{$this->model}");
        $this->data['uri']  = $this->uri;
        $this->data['table']= $this->cmd.'_file';
        $this->data['cmd']  = $this->cmd;
        $this->data['model']= &$this->{$this->model};
    }
    
    
    
    public function index(){
        redirect($this->admin->url("{$this->cmd}/showlist"));
    }
    
    
    /*
    private function checkCategory() {
        $categolies = $this->db->query("select * from question_category where status='1' ")->result_array();
        $categolies = [];
        if (!$categolies || count($categolies) < 1) {
            $this->data['menuName'] = "จัดการหมวดหมู่ {$this->menuName}";
            $this->admin->view( "{$this->cmd}/default_category_not_found", $this->data);
            
            exit();
        }
    }
     */
    
    
    public function showlist($cid=0){
        $this->admin->addBreadcrumb($this->menuName, "{$this->cmd}/");
        $totalRow               = $this->{$this->model}->getTotalRecord($cid);
        $totalRow               = $totalRow?$totalRow:1;
        $limit                  = 20;
        $currentPage            = @$_REQUEST['page'] ? $_REQUEST['page'] : 1;
        $startRecord            = ($currentPage-1)*$limit;   
        $this->data['totalRow'] = $totalRow;
        $this->data['limit']    = $limit;
        
        $cidObj                    = $this->{$this->model}->getCategoryDetail($cid);
        if( $cid ){
            $this->data['menuName'] = $cidObj['subject'];
            $this->admin->addBreadcrumb($cidObj['subject'], "{$this->cmd}/".$this->uri->segment(3));
        }else{
            $this->data['menuName'] = $this->menuName;
        }
        
        $this->data['cid']      = $cid;
        $this->data['rs']       = $this->{$this->model}->showList($cid, Array($startRecord, $limit));
        $this->admin->view( "{$this->cmd}/showlist", $this->data);
    }
    
    public function add(){
        $cid                    = 2;
        $this->data['menuName'] = "เพิ่มข้อมูล {$this->menuName}";
        $this->data['cid']      = $cid;
        $this->data['uid']      = md5(time());
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "ผู้แข่งขัน", "trim|required");
        $this->form_validation->set_rules("display_image", "ภาพประจำหัวข้อ", "trim|required");
//        $this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( "{$this->cmd}/edit", $this->data);
        }else{
            $id                 = $this->{$this->cmd.'_model'}->insert();
            redirect($this->admin->url("{$this->cmd}/showlist"));
        }
    }
    
    
    public function edit(){
        $id                 = $this->uri->segment(4);
        $rs                 = $this->{$this->model}->getDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = 'แก้ไขข้อมูล';
        foreach( $rs        AS $k=>$v ){
            if(!@$v){ $v=''; }
            $this->data[$k] = $v;
        }
        $this->data['model']    = $this->{$this->model};
        $this->data['uid']      = $rs['uid'];
        $this->admin->addBreadcrumb('ข่าว', "{$this->cmd}/");
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "ผู้แข่งขัน", "trim|required");
        $this->form_validation->set_rules("display_image", "ภาพประจำหัวข้อ", "trim|required");
//        $this->form_validation->set_rules("cid", "หมวดหมู่", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( "{$this->cmd}/edit", $this->data);
        }else{
            $this->{$this->cmd.'_model'}->update($id);
            redirect($this->admin->url("{$this->cmd}/showlist"));
        }
    }
    
    
    public function remove(){
        $id             = $this->param(3);
        $this->{$this->model}->remove($id);
        $msg            = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url().$this->segment(1).'/'.$this->segment(2));
    }
    
    
    function setSticky(){
        $this->{$this->model}->setSticky(
                $this->input->post('id'),
                $this->input->post('sticky')
        );
        exit();
    }
    
    
    function removeFile(){
        $id                 = $this->input->post('id');
        $this->{$this->model}->removeFile($id);
        exit('ok');
    }
    
    
    
    
    
    public function category(){
        $this->data['menuName'] = "จัดการหมวดหมู่ {$this->menuName}";
        $this->data['rs']       = $this->{$this->model}->categorylist();
//        exit(pre($this->data['rs']));
        $this->admin->addBreadcrumb('ข่าว', "{$this->cmd}/");
        $this->admin->addBreadcrumb('หมวดหมู่', "{$this->cmd}/category/");
        $this->admin->view( "{$this->cmd}/cateList", $this->data);
    }
    
    
    public function removeCategory(){
        $id             = $this->uri->segment(4);
        if( $this->{$this->model}->categoryIsCanRemove($id)){
            $this->{$this->model}->categoryRemove($id);
            $msg        = '<div class="alert alert-success">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-ok-sign"></i>
                                <strong>Success</strong> ลบข้อมูลแล้ว
                             </div>';
        }else{
            $msg        = '<div class="alert alert-danger">
                                <button data-dismiss="alert" class="close">×</button>
                                <i class="icon-remove-sign"></i>
                                <strong>Warning</strong> ไม่สามารถข้อมูลได้
                             </div>';
        }
        $this->session->set_flashdata('msg', $msg);
        redirect(base_url().$this->segment(1).'/'.$this->segment(2).'/category');
    }
    
    
    public function addCategory(){
        $cid                    = 2;
        $this->data['menuName'] = "เพิ่มหมวดหมู่ {$this->menuName}";
        $this->admin->addBreadcrumb('จัดการข้อมูล', "{$this->param(1)}/");
        $this->admin->addBreadcrumb('จัดการหมวดหมู่', "{$this->param(1)}/category/");
        $this->admin->view( "{$this->cmd}/cateAdd", $this->data);
        
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
        
        //$this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
    }
    
    public function addCategory_post(){
        if( $this->admin->isSiteRefered() ){
            $data       = array(
                'subject' => $this->input->post('subject'),
                'description' => $this->input->post('description'),
                'status' => $this->input->post('status'),
            );

            /*
            if( !empty($subjects) ){
                $langCodes = array();
                foreach( $subjects  AS $languageCode => $value ){
                    if( !empty( $value ) ){
                        $langCodes[] = $languageCode;
                        $data[] = array(
                            'subject' => $value,
                            'system_languages_code' => $languageCode,
                            'status' => $status[$languageCode]
                        );
                    }
                }
                if( !empty($data) ){
                    $this->{$this->model}->insertCategory($data);
                    $this->admin->setFlashMessageAlert(
                                $this->admin->getAlertView_success("Create category (".  implode(',', $langCodes).") complete")
                            );
                }else{
                    $this->admin->setFlashMessageAlert(
                                $this->admin->getAlertView_error('Can not create category')
                            );
                }
            }
             * 
             */
            $this->{$this->model}->insertCategory($data);
            $this->admin->setFlashMessageAlert(
                $this->admin->getAlertView_success("Create category complete")
            );
        }
        redirect(base_url().$this->segment(1).'/'.$this->segment(2).'/category/');
    }
    
    
    public function categoryEdit(){
        $id                 = $this->param(3);
        $rs                 = $this->{$this->model}->getCategoryDetail($id);
        if( !$rs ){
            exit('Data not found.');
        }
        $this->data['menuName'] = 'แก้ไขข้อมูล';
        foreach( $rs        AS $k=>$v ){
            if(!@$v){ $v=''; }
            $this->data[$k] = $v;
        }
        $this->admin->addBreadcrumb('Show list', "{$this->cmd}/{$this->input->get('task')}");
        $this->form_validation->set_rules("subject", "หัวข้อ", "trim|required");
        if($this->form_validation->run()===false){
            $this->data['error_message'] = validation_errors('<div class="alert alert-danger"><button class="close" data-dismiss="alert">×</button><i class="icon-remove-sign"></i> ','</div>');
            $this->admin->view( "{$this->cmd}/cateEdit", $this->data);
        }else{
            $this->{$this->model}->updateCategory($id);
            redirect(base_url().$this->uri->segment(1).'/'.$this->uri->segment(2).'/category/');
        }
    }
    
    
    
    public function updateCateSeq(){
        $id = $_REQUEST["cid"];
        $seq =$_REQUEST['seq'];
        $this->{$this->model}->getCategorySeq($id, $seq);
    }
    
    public function setting(){
        $this->admin->view( "{$this->cmd}/setting", $this->data);
    }
    
    public function system_update(){
        $this->load->library('upload');
        if( $this->admin->isSiteRefered() ){
            $data = array(
                'result_score_date' => $this->input->post('result_score_date'),
                'result_score_race' => $this->input->post('result_score_race'),
            );
            $this->setting_model->updateSettings($data);
            $this->updateImageSystemSetting('result_score_aw_desktop');
            $this->updateImageSystemSetting('result_score_aw_mobile');
            redirect( $this->admin->url($this->cmd. '/setting') );
        }else{
            redirect( $this->admin->url($this->cmd. '/setting') );
            return;
        }
    }
    
    private function updateImageSystemSetting($key){
        if( @$_FILES[$key] ){
            $dirPath                = "files/upload/{$this->cmd}/";
            @mkdir($dirPath, 0777, true);
            $name                   = $_FILES[$key]['name'];
            $config                 = Array();
            $config['upload_path']  = $dirPath;
            $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
            $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
            $config['max_size']	= 1024*5;
            $config['remove_spaces']= true;
            $this->upload->initialize($config);
            $uploadResult = $this->upload->do_upload($key);
            if ($uploadResult) {
                $uploaded           = $this->upload->data();
                @chmod($uploaded['full_path'], 0777);
                $filePath           = "{$dirPath}{$uploaded['file_name']}";
                $data = array(
                    "{$key}" => $filePath,
                );
                $this->setting_model->updateSettings( $data );
                $this->admin->setFlashMessageAlert(
                    $this->admin->getAlertView_success("Update complete")
                );
            } else {
                $this->admin->setFlashMessageAlert(
                    $this->admin->getAlertView_error("Can not upload image")
                );
            }
        }
    }
    
}