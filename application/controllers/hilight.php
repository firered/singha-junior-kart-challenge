<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hilight extends CI_Controller {
    
    private $data = array();
    public $cmd = 'home';
    
    public function __construct()
    {
        parent::__construct();
        $this->mobileDetect = new Mobile_Detect();
        $this->data['_CONTROLLER_NAME'] = $this->cmd;
    }
    
    public function show(){
        $id = $this->uri->segment(3, 0);
        $this->data['rs'] = $this->main_model->getBannerById($id);
        $this->load->view("template-hilight-dialog", $this->data);
    }
    
    
    private function getView($viewPath){
        ob_start();
        $this->load->view($viewPath, $this->data);
        return ob_get_clean();
    }
}