<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gallery extends CI_Controller {
    
    private $data = array();
    public $cmd = 'gallery';
    
    public function __construct()
    {
        parent::__construct();
        $this->data['_CONTROLLER_NAME'] = $this->cmd;
    }
    
    public function show(){
        $id = $this->uri->segment(3, 0);
        $this->data['rs'] = $this->main_model->getNewsById($id);
        $this->load->view("template-news-dialog", $this->data);
    }
    
    public function index(){
        $this->data['_BODY'] = $this->getView("gallery/view_gallery_main");
        $this->load->view("template", $this->data);
    }
    
    public function photo(){
        $this->data['_BODY'] = $this->getView("gallery/view_gallery_main");
        $this->load->view("template", $this->data);
    }
    
    public function photoDetail(){
        $id = $this->uri->segment(3, -1);
        $rs = $this->main_model->getPhotoById($id);
        if (!$rs) {
            redirect(site_url('gallery'));
            return;
        }
        $this->data['rs'] = $rs;
        $this->data['_BODY'] = $this->getView("gallery/view_gallery_photo_detail");
        $this->load->view("template", $this->data);
    }
    
    public function photoDetailAjax(){
        $id = $this->uri->segment(3, -1);
        $rs = $this->main_model->getPhotoById($id);
        if (!$rs) {
            exit('');
        }
        $this->data['rs'] = $rs;
        $this->load->view("template-gallery-detail-dialog", $this->data);
    }
    
    function downloadImages() {
        $topicId = $this->uri->segment(3, 0);
        if (!$topicId) {
            exit('-');
        }
        $rs = $this->main_model->getPhotoById($topicId);
        if (!$rs) {
            exit('Data not found');
        }
        
        @mkdir('public/temp/', 0777, true);
        
        $zip = new ZipArchive;
        $filename = "photo-{$topicId}.zip";
        $filepath = "public/temp/{$filename}";
        if ($zip->open($filepath,  ZipArchive::CREATE)) {
            foreach ($rs['_image'] AS $image) {
                $zip->addFile($image['filepath']);
            }
            $zip->close();
       } else {
           exit('Failed!');
       }
        
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-length: " . filesize($filepath));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        flush();
        readfile($filepath);
        exit;
    }
    
    function downloadImage() {
        $filepath = $_REQUEST['file'];
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush();
        readfile($filepath);
        exit;
    }
    
    public function vdo(){
        $this->data['_BODY'] = $this->getView("gallery/view_gallery_main");
        $this->load->view("template", $this->data);
    }
    
    public function vdoDetailAjax(){
        $id = $this->uri->segment(3, -1);
        $rs = $this->main_model->getVDOById($id);
        if (!$rs) {
            exit('--');
        }
        $this->data['rs'] = $rs;
        $this->load->view("template-gallery-vdo-detail-dialog", $this->data);
    }
    
    private function getView($viewPath){
        ob_start();
        $this->load->view($viewPath, $this->data);
        return ob_get_clean();
    }
}