<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
    
    public $mobileDetect;
    public $isMobile = false;
    private $data = array();
    public $cmd = 'home';
    
    
    public function __construct()
    {
        parent::__construct();
        $this->mobileDetect = new Mobile_Detect();
        $this->isMobile = $this->mobileDetect->isMobile()||$this->mobileDetect->isTablet();
        $this->data['_CONTROLLER_NAME'] = $this->cmd;
    }
    
    public function index(){
        $this->data['_BODY'] = $this->getView("home/view_main");
        $this->load->view("template", $this->data);
    }
    
    
    private function getView($viewPath){
        ob_start();
        $this->load->view($viewPath, $this->data);
        return ob_get_clean();
    }
}