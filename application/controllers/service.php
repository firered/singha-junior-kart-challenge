<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Service extends CI_Controller {   

    public function __construct() {
        parent::__construct();
        
        if(!$this->agent->is_referral()){
             exit('-');
        }
        $this->load->model('main_model');
    }
    
    public function getHashtag(){
        $maxQuery       = 4;
        $accessToken    = "1406045013.3a81a9f.7c505432dfd3455ba8e16af5a892b4f7";
        $hashtag        = "lifeismotion";
        
        //$hashtags       = array("lifeismotion", "newlightiscoming");
        $hashtags       = $this->ig_model->getHashtagKey();
        
        if( $hashtags  && is_array($hashtags)  &&  count($hashtags)>0 ){
            foreach( $hashtags AS $hashtag  ){
                $countQuery     = 1;
                $url            = "https://api.instagram.com/v1/tags/{$hashtag}/media/recent?access_token={$accessToken}";
                $output = @file_get_contents($url);
                $rs   = @json_decode($output);
                if( @$rs->data ){
                    $this->ig_model->addData($rs->data, $hashtag);
                    while( @$rs->pagination->next_url && $countQuery<=$maxQuery){
                        $nexturl = $rs->pagination->next_url;
                        $output = @file_get_contents($nexturl);
                        $rs   = @json_decode($output);
                        if( @$rs->data ){
                            $this->ig_model->addData($rs->data, $hashtag);
                        }
                        $countQuery++;
                    }
                }
            }
        }
        
        echo "Complete";
        
    }
    
    function registerWithFacebook() {
        $accessToken = @$_REQUEST['accessToken'];
        $userID = @$_REQUEST['userID'];
        $output = array(
            'code' =>  'Something went wong, please try again.',
            'msg' => 400
        );
        if ($accessToken && $userID) {
            $userInfo = @file_get_contents("https://graph.facebook.com/v2.11/{$userID}?fields=name,email&access_token={$accessToken}");
            if ($userInfo) {
                $userInfo = json_decode($userInfo);
                $name = $userInfo->name;
                $email = $userInfo->email;
                $memberId = $this->main_model->registerWithFacebookAccount($userID, $accessToken, $email, $name);
                if ($memberId) {
                    $output['code'] = 200;
                    $output['msg'] = 'Registed account.';
                    
                }
            }
        }
        
        exit(json_encode($output));
    }
    
    function logout() {
        $this->main_model->clearLogedinCookie();
        redirect(site_url('home'));
    }
    
    function getDomain(){
        $CI =& get_instance();
        return preg_replace("/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/","$1", $CI->config->slash_item('base_url'));
    }
    
    
    
    public function contactus(){
        $this->sendMail();
    }
    private function sendMail(){
        $output     = array(
            'status' =>  403,
            'message' => 'ไม่สามารถส่งอีเมล์ได้ กรุณาลองใหม่อีกครั้ง'
        );
        if( $this->agent->is_referral() ){
            $name       = $this->input->post('name');
            $company    = $this->input->post('company');
            $phone      = $this->input->post('phone');
            $email      = $this->input->post('email');
            $message    = $this->input->post('message');
            $recaptcha  = $this->input->post('captcha');
            
            if( !$recaptcha ){
                $output     = array(
                    'status' =>  500,
                    'message' => 'การยืนยันตัวตนด้วย reCAPTCHA ไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง'
                );
                exit(json_encode($output));
            }
            
            $urlVerify  = "https://www.google.com/recaptcha/api/siteverify?secret=".RECAPTCHA_SECRET_KEY."&response={$recaptcha}&remoteip={$_SERVER['REMOTE_ADDR']}";
            $verified   = @json_decode(file_get_contents($urlVerify));
            
            $emails     = getAdmin()->getConfig()->contact_email;
            $emails     = explode(",", $emails);
            $tmp        = array();
            if( $emails && $emails!='' ){
                foreach( $emails AS $m){
                    if( $m!='' ){
                        $tmp[] = $m;
                    }
                }
                $emails = $tmp;
            }
            
            if( !$emails  ||  count($emails)<1 ){
                $output     = array(
                    'status' =>  500,
                    'message' => 'ไม่สามารถส่งอีเมล์ได้ กรุณาลองใหม่อีกครั้ง',
                    'log' => 'Contact email not found'
                );
                exit(json_encode($output));
            }
            
            
            if( $verified && $verified->success ){
                require 'PHPMailer/class.phpmailer.php';
                $mail = new PHPMailer;
                
                $mail->IsSMTP(); // enable SMTP
                $mail->CharSet="UTF-8";
                $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
                $mail->SMTPAuth = true; // authentication enabled
                $mail->SMTPSecure = 'tls';
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 587; // or 587, 465
                $mail->IsHTML(true);
                $mail->Username = "leo.fan.league.1@gmail.com";
                $mail->Password = "leo.fan.league.1%#";
                
                $mail->setFrom($email);
                foreach( $emails AS $m ){
                    $mail->addAddress($m);
                }
                $mail->Subject  = "Contact email from {$name}";
                $mail->Body     = "Email from "
                        . "&nbsp;&nbsp;คุณ {$name}\n<br/>"
                        . "Email: {$email}\n<br/>"
                        . "เบอร์โทร: {$phone}\n<br/>"
                        . "บริษัท: {$company}\n<br/>"
                        . "รายละเอียด: \n<br/>{$message}\n<br/>\n<br/>"
                        . "วันที่: ". date('Y-m-d H:i:s');
                if(!$mail->send()) {
                    $output     = array(
                        'status' =>  500,
                        'message' => 'ไม่สามารถส่งอีเมล์ได้ ('.$mail->ErrorInfo.') กรุณาลองใหม่อีกครั้ง'
                    );
                    
                } else {
                    $output     = array(
                        'status' =>  200,
                        'message' => 'ส่งอีเมล์เรียบร้อยแล้ว'
                    );
                }
            }
            exit(json_encode($output));
        }
        
        exit(json_encode($output));
    }
    
    
    public function submitQuiz() {
        $output = array(
            'status' => 400,
            'msg' => '',
        );
        $outputForSave = array();
        $data = $this->input->post('data');
        foreach($data AS $k => $v) {
            $outputForSave[$k] = $v;
        }
        
        if (!$data) {
            $output['msg'] = 'Submit data not found';
            exit(json_encode($data));
        }

        
        $_compareData = array();
        foreach ($data['quiz'] AS $item) {
            // $topicId = $item['tid'];
            $choiceId = $item['value'];
            $query = "  SELECT * FROM question_choice where id='{$choiceId}' ";
            $choice = $this->db->query($query)->row_array();
            
            $topicId = $choice['topic_id'];
            $cid = $choice['cid'];
            $index = "cid{$cid}";
            
            if (@$_compareData[$index]) {
                $_compareData[$index]['count']++;
            } else {
                $_compareData[$index] = array(
                    'cid' => $cid,
                    'tid' => $topicId,
                    'count' => 1
                );
            }
        }
        
        $resultData = null;
        foreach ($_compareData AS $v) {
            if ($resultData == null) {
                $resultData = $v;
            } else if ($resultData['count'] < $v['count']) {
                $resultData = $v;
            }
        }
        
        $query = "  SELECT * FROM question_category where id='{$resultData['cid']}' ";
        $resultCategory = $this->db->query($query)->row_array();
        $output['status'] = 200;
        $output['msg'] = 'success';
        $output['data'] = array(
            'subject' => $resultCategory['subject'],
            'description' => $resultCategory['description'],
            'display_image' => base_url($resultCategory['display_image']),
            'display_image_relative' => $resultCategory['display_image']
        );
        $outputForSave['result'] = $output['data'];
        
        /* Save Quiz result */
        $insertData = array(
            'data'          => json_encode($outputForSave),
            'create_date'   => date('Y-m-d H:i:s'),
            'create_ip'     => $this->input->ip_address(),
            'status'        => '1',
        );
        $this->db->insert('question_result', $insertData);
        $insertId = $this->db->insert_id();
        $output['data']['id'] = $insertId;
        /* END: Save Quiz result */
        
        exit(json_encode($output));
    }
    
    
    public function shared() {
        $shareURL = @$_REQUEST['shareUrl'];
        if ($shareURL) {
            $resultId = explode('?result=', $shareURL);
            $resultId = @$resultId[1];
            if ($resultId) {
                $this->db->set('shared_fb_success', '1');
                $this->db->where('id', $resultId);
                $this->db->update('question_result');
            }
        }
    }
}
