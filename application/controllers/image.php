<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Image extends CI_Controller {

    private $_tempDir               = './public/cache/';


    public function __construct() {
        parent::__construct();
        if( !@file_exists($this->_tempDir) ){
            @mkdir($this->_tempDir, 0777, true);
        }
    }


    private function initial(){
        /* Check tmp Directory */
        if(!file_exists($this->_tempDir) ){
            $is_made        = mkdir($this->_tempDir, 0777);
            @chmod($this->_tempDir, 0777);
            if(!$is_made){
                return false;
            }
        }
    }


    public function ratio(){
        $this->initial();
        $this->load->helper('string');
        $file               = $this->input->get('file','');
        $maxWidth           = $this->input->get('width', 100);
        $maxHeight          = $this->input->get('height', 100);
        $extension          = '';
        if(trim($file)=='' || !file_exists($file)){ 
            $this->defaultImage();
        }

        if(strstr($file, 'http')    ||  strstr($file, 'ftp')){
            $fileInfo       = pathinfo($file);
            $filePath       = $this->_tempDir.md5($file).".{$fileInfo['extension']}";
            if(!file_exists($filePath)){
                $extension      = "image/{$fileInfo['extension']}";
                $f              = @fopen($filePath, 'w');
                fwrite($f, file_get_contents($file));
                @fclose($f);
            }        
            $file           = $filePath;
            $filePath       = null;
            $fileInfo       = null;
        }
        if(!$extension){
            $extension      = pathinfo($file);
            $extension      = $extension['extension'];
        }      

        $tmpFile            = $this->_tempDir. md5($file.'ratio'.$maxWidth.$maxHeight). '.' .$extension;
        if(!file_exists($tmpFile)){
            $config['image_library']    = 'gd2';
            $config['source_image']     = $file;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = false;
            $config['maintain_ratio']   = true;
            $config['width']            = $maxWidth;
            $config['height']           = $maxHeight;
            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->resize()){
                exit($this->image_lib->display_errors());
            }  
        }
        /*
        $img                = null;
        switch($extension){
            case 'png'  : {$img=@imagecreatefrompng($tmpFile); break;}
            case 'gif'  : {$img=@imagecreatefromgif($tmpFile); break;}
            case 'jpg'  : 
            default     : {$img=@imagecreatefromjpeg($tmpFile); break;}
        }
        
        header ("Content-type: image/{$extension}");
        switch($extension){
            case 'png'  : {@imagepng($img);break;}
            case 'gif'  : {@imagegif($img);break;}
            case 'jpg'  : 
            default     : {@imagejpeg($img);break;}
        }
        @imagedestroy($img);
         *
         */
        
        $rFP = fopen($tmpFile, 'rb');
        header ("Content-type: image/{$extension}");
        header("Content-Length: " .(string)(filesize($tmpFile)) );
        fpassthru($rFP);
             
        //@unlink($tmpFile);
        exit();
    }


    public function ratiofix(){
        $this->initial();
        $this->load->helper('string');
        $file               = $this->input->get('file','');
        $defaultImage       = $this->input->get('defaultImage', 'images/nopic.jpg');
        $maxWidth           = $this->input->get('width', 100);
        $maxHeight          = $this->input->get('height', 100);
        $extension          = '';
        if(trim($file)=='' || !file_exists($file)){$file= $defaultImage;}
        if(strstr($file, 'http')    ||  strstr($file, 'ftp')){
            $fileInfo       = pathinfo($file);
            $filePath       = $this->_tempDir.md5($file).".{$fileInfo['extension']}";
            if(!file_exists($filePath)){
                $extension      = "image/{$fileInfo['extension']}";
                $f              = @fopen($filePath, 'w');
                fwrite($f, file_get_contents($file));
                @fclose($f);
            }        
            $file           = $filePath;
            $filePath       = null;
            $fileInfo       = null;
        }
        if(!$extension){
            $extension      = pathinfo($file);
            $extension      = $extension['extension'];
        }      

        $tmpFile            = $this->_tempDir. md5($file.'ratio'.$maxWidth.$maxHeight). '.' .$extension;
        if(!file_exists($tmpFile)){
            $config['image_library']    = 'gd2';
            $config['source_image']     = $file;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = true;
            $config['maintain_ratio']   = false;
            $config['width']            = $maxWidth;
            $config['height']           = $maxHeight;

            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->resize()){
                exit($this->image_lib->display_errors());
            }  
        }        
        $img                = null;
        switch($extension){
            case 'png'  : {$img=@imagecreatefrompng($tmpFile); break;}
            case 'gif'  : {$img=@imagecreatefromgif($tmpFile); break;}
            case 'jpg'  : 
            default     : {$img=@imagecreatefromjpeg($tmpFile); break;}
        }
        header ("Content-type: image/{$extension}");
        switch($extension){
            case 'png'  : {@imagepng($img);break;}
            case 'gif'  : {@imagegif($img);break;}
            case 'jpg'  : 
            default     : {@imagejpeg($img);break;}
        }
        @imagedestroy($img);
        //@unlink($tmpFile);
        exit();
    }


    public function fix(){
        $this->initial();
        $this->load->helper('string');
        $file               = $this->input->get('file','');
        $defaultImage       = $this->input->get('defaultImage', 'images/nopic.jpg');
        $maxWidth           = $this->input->get('width', 100);
        $maxHeight          = $this->input->get('height', 100);
        $extension          = '';
        if($file==''){
            $this->defaultImage();
        }
        if(!file_exists($file)){
            $this->defaultImage();
        }
        if(strstr($file, 'http')    ||  strstr($file, 'ftp')){
            $fileInfo       = pathinfo($file);
            $filePath       = $this->_tempDir.md5($file).".{$fileInfo['extension']}";
            if(!file_exists($filePath)){
                $extension      = "image/{$fileInfo['extension']}";
                $f              = @fopen($filePath, 'w');
                fwrite($f, file_get_contents($file));
                @fclose($f);
            }        
            $file           = $filePath;
            $filePath       = null;
            $fileInfo       = null;
        }
        if(!$extension){
            $extension      = pathinfo($file);
            $extension      = $extension['extension'];
        }  
        $tmpFile            = $this->_tempDir. md5($file.'fix'.$maxWidth.$maxHeight). '.'.$extension;
        if(!file_exists($tmpFile)){
            /*
            $config['image_library']    = 'gd2';
            $config['source_image']     = $file;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = false;
            $config['maintain_ratio']   = true;
            $config['width']            = $maxWidth+($maxWidth/2);
            $config['height']           = $maxHeight+($maxHeight/2);
            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->resize()){
                exit($this->image_lib->display_errors());
            } 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $tmpFile;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = false;
            $config['width']            = $maxWidth;
            $config['height']           = $maxHeight;
            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->crop()){
                exit($this->image_lib->display_errors());
            }  
             * 
             */
            $config['x_axis']=0;
            $config['y_axis']=0;
            $imgSize                    = getimagesize($file);
            $width_real                 = $imgSize[0];
            $height_real                = $imgSize[1];
            if( $maxWidth<$width_real ){
                $config['x_axis']       = ($width_real-$maxWidth)/2;
            }
            if( $maxHeight<$height_real ){
                $config['y_axis']       = ($height_real-$maxHeight)/2;
            }
            $config['image_library']    = 'gd2';
            $config['quality']          = 100;
            $config['source_image']     = $file;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = false;
            $config['width']            = $maxWidth;
            $config['height']           = $maxHeight;
            $config['maintain_ratio']   = false;
            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->crop()){
                exit($this->image_lib->display_errors());
            }  
        }        
        $img                = null;
        switch($extension){
            case 'png'  : {$img=@imagecreatefrompng($tmpFile); break;}
            case 'gif'  : {$img=@imagecreatefromgif($tmpFile); break;}
            case 'jpg'  : 
            default     : {$img=@imagecreatefromjpeg($tmpFile); break;}
        }
        header ("Content-type: image/{$extension}");
        switch($extension){
            case 'png'  : {@imagepng($img);break;}
            case 'gif'  : {@imagegif($img);break;}
            case 'jpg'  : 
            default     : {@imagejpeg($img);break;}
        }
        @imagedestroy($img);
        exit();
    }


    public function watermask(){            
        $this->initial();
        $this->load->helper('string');
        $file               = $this->input->get('file','');
        $defaultImage       = $this->input->get('defaultImage', 'images/nopic.jpg');
        $maxWidth           = $this->input->get('width', 100);
        $maxHeight          = $this->input->get('height', 100);
        if($file==''){$file= $defaultImage;}
        if(!file_exists($file)){$file= $defaultImage;}
        $tmpFile            = $this->_tempDir. random_string('alnum', 16). '.png';
        $config['image_library']    = 'gd2';
        $config['source_image']	= $file;
        $config['new_image']        = $tmpFile;
        $config['wm_text'] = 'Copyright Cityvariety';
        $config['wm_type'] = 'text';
        $config['wm_font_path']     = './system/fonts/texb.ttf';
        $config['wm_font_size']	= '16';
        $config['wm_font_color']    = 'ffffff';
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'center';
        //$config['wm_padding'] = '20';
        $config['width']            = $maxWidth;
        $config['height']           = $maxHeight;
        $config['create_thumb']     = false;
        $config['maintain_ratio']   = true;


        $this->load->library('image_lib', $config); 
        $this->image_lib->initialize($config); 
        if ( ! $this->image_lib->watermark()){
            exit($this->image_lib->display_errors());
        }            
        $img                        = imagecreatefrompng($tmpFile);
        header ("Content-type: image/png");
        imagepng($img);
        imagedestroy($img);
        @unlink($tmpFile);
        exit();
    }


    public function orginal(){
        $file               = $this->input->get('file','');
        $defaultImage       = $this->input->get('defaultImage', 'images/nopic.jpg');            
        if($file==''){
            $file           = $defaultImage;
        }
        $extension      = pathinfo($file);
        $extension      = $extension['extension'];
        switch($extension){
            case 'png'  : {$img=@imagecreatefrompng($file); break;}
            case 'gif'  : {$img=@imagecreatefromgif($file); break;}
            case 'jpg'  : 
            default     : {$img=@imagecreatefromjpeg($file); break;}
        }
        header ("Content-type: image/{$extension}");
        switch($extension){
            case 'png'  : {@imagepng($img);break;}
            case 'gif'  : {@imagegif($img);break;}
            case 'jpg'  : 
            default     : {@imagejpeg($img);break;}
        }
        imagedestroy($img);
        exit();




    }


    public function defaultImage(){
        $imgWidth       = $this->input->get('width');
        if( $imgWidth ) { $imgWidth=150;  }
        $imgHeight      = $this->input->get('height');
        if( $imgHeight ){ $imgHeight=150; }
        $defaultText    = $this->input->get('defaultText');
        header("Content-type: image/jpeg");
        $images         = ImageCreate(  $this->input->get('width'), $this->input->get('height'));
        imagecolorallocate($images,246,246,246);
        if($defaultText){
            $color      = imagecolorallocate($images, 181, 181, 181);
            imagestring($images, 3, 5, 5, $defaultText, $color);
        }
        ImageJpeg($images);
        ImageDestroy($images);
        exit();
    }

    
    public function getImageRatio($imgPath, $width, $height){
        $this->_tempDir     = str_replace('./', '', $this->_tempDir);
        $this->initial();
        $this->load->helper('string');
        $file               = $imgPath;
        $maxWidth           = $width;
        $maxHeight          = $height;
        $extension          = '';
        if( strstr($file, 'http')    ||  strstr($file, 'ftp')){
            $fileInfo       = pathinfo($file);
            $filePath       = $this->_tempDir.md5($file).".{$fileInfo['extension']}";
            
            if(!file_exists($filePath)){
                $extension      = "image/{$fileInfo['extension']}";
                $f              = @fopen($filePath, 'w');
                fwrite($f, file_get_contents($file));
                @fclose($f);
            }        
            $file           = $filePath;
        }
        if(!$extension){
            $extension      = pathinfo($file);
            $extension      = $extension['extension'];
        }      

        $tmpFile            = $this->_tempDir. md5($file.'ratio'.$maxWidth.$maxHeight). '.' .$extension;
        if(!file_exists($tmpFile)){
            $config['image_library']    = 'gd2';
            $config['source_image']     = $file;
            $config['new_image']        = $tmpFile;
            $config['create_thumb']     = false;
            $config['maintain_ratio']   = true;
            $config['width']            = $maxWidth;
            $config['height']           = $maxHeight;
            $this->load->library('image_lib', $config); 
            $this->image_lib->initialize($config); 
            if ( ! $this->image_lib->resize()){
                exit($this->image_lib->display_errors());
            }  
        }
        $size           = @getimagesize($tmpFile);
        $output         = array(
            'path'      => $tmpFile,
            'width'     => @$size[0],
            'height'    => @$size[1]
        );
        return $output;
    }
    
    
    public function random(){
        $rootPath = "files/image_random";
        $files = scandir($rootPath);
        $imagesPath = array();
        foreach( $files AS $f  ){
            if( $f!="."  &&  $f!=".." ){
                $imagesPath[] = "{$rootPath}/{$f}";
            }
        }
        
        $imagePath = $imagesPath[rand(0, count($imagesPath)-1)];;
        
        $img=@imagecreatefromjpeg($imagePath);
        
        header ("Content-type: image/jpeg");
        @imagejpeg($img);
        @imagedestroy($img);
        exit();
    }
    
    
    public function profileRandom(){
        $rootPath = "files/image_profile_random";
        $files = scandir($rootPath);
        $imagesPath = array();
        foreach( $files AS $f  ){
            if( $f!="."  &&  $f!=".." ){
                $imagesPath[] = "{$rootPath}/{$f}";
            }
        }
        
        $imagePath = $imagesPath[rand(0, count($imagesPath)-1)];;
        
        $img=@imagecreatefromjpeg($imagePath);
        
        header ("Content-type: image/jpeg");
        @imagejpeg($img);
        @imagedestroy($img);
        exit();
    }
    
    
    
    public function randomCrop(){
        
        $index = @$_REQUEST['i'] ? $_REQUEST['i'] : -1;
        
//        $raw = imagecreatefromjpeg('public/17333294_1882353252053448_4883079663217278976_n.jpg'); 
        //$raw = imagecreatefromstring(file_get_contents("http://beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg"));
        $raw = imagecreatefromstring(file_get_contents(@$_REQUEST['url']));
        //$raw = imagecreatefromjpeg(@$_REQUEST['url']);
        $w = imagesx($raw); 
        $points = array();
        $_points = array(
            array(
                .5 * $w, .067  * $w,
                0, .5   * $w,
                .25 * $w, .933  * $w,
                .75 * $w, .933  * $w,
                $w, .5  * $w,
                .75 * $w, .067  * $w
            ),
            array(
                .25 * $w, .01  * $w,
                0, .8   * $w,
                .25 * $w, .933  * $w,
                .75 * $w, .833  * $w,
                $w, .2  * $w,
                .75 * $w, .067  * $w
            ),
            array(
                .25 * $w, .05  * $w,
                0, .5   * $w,
                .25 * $w, .933  * $w,
                .75 * $w, .833  * $w,
                $w, .5  * $w,
                .35 * $w, .067  * $w 
            ),
            array(
                .3, .05  * $w,
                .3, .05   * $w,
                .25 * $w, .933  * $w,
                .75 * $w, .833  * $w,
                $w, .5  * $w,
                .55 * $w, .05  * $w 
            ),
            array(
                .1, .5* $w,
                .1, .5* $w,
                .25 * $w, .933  * $w,
                .75 * $w, .833  * $w,
                $w, .5  * $w,
                .55 * $w, .05  * $w
            ),
            array(
                .01 * $w, .17  * $w,
                0, .3   * $w,
                .25 * $w, .933  * $w,
                .75 * $w, .933  * $w,
                $w, .2  * $w,
                .25 * $w, .067  * $w
            )
        );
        if( $index<0 ){
            $points = $_points[rand(0, count($_points)-1)];
        }else{
            $points = $_points[ $index % count($_points)-1 ];
        }

        // Create the mask
        $mask = imagecreatetruecolor($w, $w);
        imagefilledpolygon($mask, $points, 6, imagecolorallocate($mask, 255, 0, 0));

        // Create the new image with a transparent bg
        $image = imagecreatetruecolor($w, $w);
        $transparent = @imagecolorallocatealpha($image, 0, 0, 0, 127);
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagefill($image, 0, 0, $transparent);

        // Iterate over the mask's pixels, only copy them when its red.
        // Note that you could have semi-transparent colors by simply using the mask's 
        // red channel as the original color's alpha.
        for($x = 0; $x < $w; $x++) {
            for ($y=0; $y < $w; $y++) { 
                $m = imagecolorsforindex($mask, @imagecolorat($mask, $x, $y));
                if($m['red']) {
                    $color = imagecolorsforindex($raw, @imagecolorat($raw, $x, $y));
                    imagesetpixel($image, $x, $y, @imagecolorallocatealpha($image,
                                      $color['red'], $color['green'], 
                                      $color['blue'], $color['alpha']));
                }
            }
        }

        // Display the result
        header('Content-type: image/png');
        imagepng($image);
        imagedestroy($image);
    }

}