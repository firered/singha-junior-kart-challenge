<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AboutUs extends CI_Controller {
    
    private $data = array();
    public $cmd = 'aboutus';
    
    public function __construct()
    {
        parent::__construct();
        $this->data['_CONTROLLER_NAME'] = $this->cmd;
    }
    
    public function index(){
        $this->load->view("template-aboutus", $this->data);
    }
    
    
    private function getView($viewPath){
        ob_start();
        $this->load->view($viewPath, $this->data);
        return ob_get_clean();
    }
}