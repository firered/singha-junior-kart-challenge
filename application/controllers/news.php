<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller {
    
    private $data = array();
    public $cmd = 'news';
    
    public function __construct()
    {
        parent::__construct();
        $this->data['_CONTROLLER_NAME'] = $this->cmd;
    }
    
    public function show(){
        $id = $this->uri->segment(3, 0);
        $this->data['rs'] = $this->main_model->getNewsById($id);
        $this->load->view("template-news-dialog", $this->data);
    }
    
    public function index(){
        $this->data['_BODY'] = $this->getView("news/view_news_main");
        $this->load->view("template", $this->data);
    }
    
    private function getView($viewPath){
        ob_start();
        $this->load->view($viewPath, $this->data);
        return ob_get_clean();
    }
}