<?php
    class Gallery_model extends CI_Model{
        
        private $table              = Array(
            'topic'         => 'gallery_topic',
            'file'          => 'gallery_file',
            'cate'          => 'gallery_category',
        );
        
        
        public function count($year=0){
            $query = "  SELECT count(*) AS count"
                    . " FROM {$this->table['topic']}"
                    . " WHERE   status='1' ";
            if ($year > 0) {
                $query.=" AND create_date >= '{$year}-00-00 00:00:00' AND create_date <= '{$year}-12-31 23:59:59' ";
            }          
            $output = $this->db->query($query)->row_array();
            if( $output ){
                return $output['count'];
            }
            return 0;
        }
        
        public function get($year=0, $start=0, $limit=20){
            $query = ""
                    . " SELECT      id, cid, uid, subject, description, display_image, create_date"
                    . " FROM        {$this->table['topic']}"
                    . " WHERE       status='1'      ";
            if( $year ){
               $query.=" AND create_date >= '{$year}-00-00 00:00:00' AND create_date <= '{$year}-12-31 23:59:59' ";
            }
            $query  .= "ORDER BY    id DESC"        
                    . " LIMIT       {$start}, {$limit}";
            $rs = $this->db->query($query)->result_array();
            $output = array();
            if( $rs ){
                foreach( $rs AS $row ){
                    if( $row['display_image']!='' ){
                        $row['display_image'] = base_url($row['display_image']);
                    }else{
                        $row['display_image'] = '';
                    }
                    $row['type'] = 'news';
//                    $row['description'] = html_entity_decode($row['description']);
//                    $row['description'] = strip_tags($row['description']);
                    $output[] = $row;
                }
            }
            return $output;
        }
        
        
        public function getByPin($fb_id, $start=0, $limit=20){
            $query = ""
                    . " SELECT      t.id, t.cid, t.uid, t.subject, t.description, "
                    . "             t.display_image, t.create_date"
                    . " FROM        {$this->table['pin']}   AS p,"
                    . "             {$this->table['topic']} AS t"
                    . " WHERE       p.fb_id='{$fb_id}'  AND"
                    . "             p.topic_id=t.id     AND"
                    . "             t.status='1' "
                    . " ORDER BY    t.id DESC "
                    . " LIMIT       {$start}, {$limit}";
            $rs = $this->db->query($query)->result_array();
            $output = array();
            if( $rs ){
                foreach( $rs AS $row ){
                    if( $row['display_image']!='' ){
                        $row['display_image'] = base_url($row['display_image']);
                    }else{
                        $row['display_image'] = '';
                    }
                    $row['type'] = 'news';
//                    $row['description'] = html_entity_decode($row['description']);
//                    $row['description'] = strip_tags($row['description']);
                    $output[] = $row;
                }
            }
            return $output;
        }
        
        
        public function getAllImage($uid){
            $this->db->select("id, filename, filepath, ext, size");
            $this->db->where('status', '1');
            $this->db->where('type', 'i');
            $this->db->where('uid', $uid);
            $rs =  $this->db->get($this->table['file'])->result_array();
            return $rs;
//            if( $rs ){
//                $output = array();
//                foreach( $rs AS  $row ){
//                    $row['filepath'] = base_url($row['filepath']);
//                    $output[] = $row;
//                }
//                return $output;
//            }
//            return null;
        }
        
        
        public function getAllCategory(){
            $this->db->select('*');
            $this->db->where('status', '1');
            $this->db->where('id > 1');
            $this->db->order_by('seq', 'asc');
            return $this->db->get($this->table['cate'])->result_array();
        }
        
        
        public function getHotNews($start=0, $limit=20){
            $query = ""
                    . " SELECT      id, cid, uid, subject, description, display_image, create_date"
                    . " FROM        {$this->table['topic']}"
                    . " WHERE       status='1'      AND"
                            . "     sticky='1' "
                    . " ORDER BY    id DESC"        
                    . " LIMIT       {$start}, {$limit}";
            $rs = $this->db->query($query)->result_array();
            $output = array();
            if( $rs ){
                foreach( $rs AS $row ){
                    if( $row['display_image']!='' ){
                        $row['display_image'] = base_url($row['display_image']);
                    }else{
                        $row['display_image'] = '';
                    }
                    $row['type'] = 'news';
//                    $row['description'] = html_entity_decode($row['description']);
//                    $row['description'] = strip_tags($row['description']);
                    $output[] = $row;
                }
            }
            return $output;
        }
        
        
        public function getNewsDisplayImage($limit=10){
            $query = ""
                    . " SELECT      id, display_image"
                    . " FROM        news_topic"
                    . " WHERE       status='1'  AND"
                    . "             display_image<>'' AND"
                    . "             NOT display_image IS NULL"
                    . " ORDER BY    id DESC"
                    . " LIMIT       {$limit}"
                    . "";
            return $this->db->query($query)->result_array();
        }
        
       
        public function getDetail($id){
            $this->db->select("*");
            $this->db->where('status', '1');
            $this->db->where('id', $id);
            $rs = $this->db->get($this->table['topic'])->row_array();
            if( $rs ){
                if( $rs['display_image']!='' ){
                    $rs['display_image'] = base_url($rs['display_image']);
                }else{
                    $rs['display_image'] = '';
                }
                $rs['type'] = 'news';
//                $rs['description'] = html_entity_decode($rs['description']);
//                $rs['description'] = strip_tags($rs['description'], '<div><span><br><b><a><p><strong>');
                return $rs;
            }
            return null;
        }
        
        
        public function isPin($fbId, $topicId){
            $this->db->select("*");
            $this->db->where("fb_id", $fbId);
            $this->db->where("topic_id", $topicId);
            $rs  = $this->db->get($this->table['pin'])->result_array();
            return $rs && count($rs)>0 ? true : false ;
        }
        
        
        public function getById($id){
            $this->db->select("*");
            $this->db->where('status', '1');
            $this->db->where('id', $id);
            return $this->db->get($this->table['topic'])->row_array();
        }
        
        
        public function post(){
            $uid            = md5(time());
            $cid            = 7;
            $facebookID     = $this->input->post("facebook_id");
            $status         = '1';
            $this->db->set('uid',           $uid);
            $this->db->set('cid',           $cid);
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('description',   trim(strip_tags($this->input->post('description'), '<div><span><br><b><a><p><strong>')) );
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by_fb_id',     $facebookID);
            $this->db->set('view',          0);
            $this->db->set('sticky',        '0');
            $this->db->set('status',        $status);
            $this->db->set('facebook_name', $this->input->post('facebook_name'));
            $this->db->set('facebook_image',$this->input->post('facebook_image'));
            $this->db->insert($this->table['topic']);
            $insert_id      = $this->db->insert_id();
            if( $insert_id ){
                if(@$_FILES['image']){
                    $filePaths = $this->uploadImage('image', $uid);
                    if( $filePaths  && is_array($filePaths)  && count($filePaths)>0 ){
                        $display_image = $filePaths[0];
                        $this->db->set("display_image", $display_image);
                        $this->db->where("id", $insert_id);
                        $this->db->update($this->table['topic']);
                    }
                }
            }
            
            return $this->getDetail($insert_id);
            
        }
        
        
        public function uploadImage($fieldName, $uid){
            $maxFileSize            = 1024*4;
            $files                  = $_FILES[$fieldName];
            $output                 = Array();
            $directory              = "files/upload/";
            $cmd                = 'news';
            if( count($files['name']) ){
                $type               = 'i';
                $table              = 'news_file';
                $this->currentUploadDir= "{$directory}{$cmd}/".date('Y-m-d').'/';
                echo "Upload dir: {$this->currentUploadDir}";
                @mkdir($this->currentUploadDir, 0777, true);
                $len                = count($files['name']);
                for( $i=0;  $i<$len;  $i++ ){
                    $name                   = $files['name'][$i];
                    $tmp_name               = $files['tmp_name'][$i];
                    $_FILES[$fieldName]     = Array();
                    $_FILES[$fieldName]['name']     = $files['name'][$i];
                    $_FILES[$fieldName]['type']     = $files['type'][$i];
                    $_FILES[$fieldName]['tmp_name'] = $files['tmp_name'][$i];
                    $_FILES[$fieldName]['error']    = $files['error'][$i];
                    $_FILES[$fieldName]['size']     = $files['size'][$i]; 
                    $name_new               = date('Y-m-d_').substr(md5(time().$name), 0, 10);
                    $config                 = Array();
                    $config['upload_path']  = $this->currentUploadDir;
                    $config['file_name']	= $name_new;
                    $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                    $config['max_size']	= $maxFileSize;
                    $config['remove_spaces']= true;
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload($fieldName)) {
                        $uploaded               = $this->upload->data();
                        $filePath               = $this->currentUploadDir.$uploaded['orig_name'];
                        $this->db->set('uid',       $uid);
                        $this->db->set('filename',  $name);
                        $this->db->set('filepath',  $filePath);
                        $this->db->set('type',      $type);
                        $this->db->set('ext',       str_replace('.', '', $uploaded['file_ext']));
                        $this->db->set('size',      $uploaded['file_size']*1024);
                        $this->db->set('create_date',   date('y-m-d H:i:s'));
                        $this->db->set('create_ip',     $this->input->ip_address());
                        $this->db->set('create_by',     $this->session->userdata('id'));
                        $this->db->set('sticky',    '0');
                        $this->db->set('status',    '1');
                        $this->db->insert($table);
                        $imgId                  = $this->db->insert_id();         
                        //echo "Insert image id: ". $imgId;
                        $output[] = $filePath;
                    }else{
                        $error = $this->upload->display_errors();
                        pre($error);
                    }
                }
            }
            
            return $output;
        }
    
    
    
    }
?>