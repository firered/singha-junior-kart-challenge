<?php
    class Activity_model extends CI_Model{
        
        private $table              = Array(
            'topic'         => 'activity_topic',
            'file'          => 'activity_file',
            'cate'          => 'activity_category'
        );
        private $modelName          = 'activity';
        
        
        public function getTotalRecord($cid=0){
            $query          = ""
                    . " SELECT      COUNT(id) AS total"
                    . " FROM        {$this->table['topic']}"
                    . " WHERE       status<>'2'"
                    . "";
            if($cid){
                $query      .=" AND cid='{$cid}' ";
            }
            $rs             = $this->db->query($query)->row_array();
            return $rs['total'];
        }
        
        public function showList( $cid=0, $limit=Array(0,10) ){
            $this->db->select('*');
            if( $cid ){
                $this->db->where('cid', $cid);
            }
            $this->db->where('status <> ',  '2');
            $this->db->limit($limit[1], $limit[0]);
            $this->db->order_by('id',   'DESC');
            return $this->db->get($this->table['topic'])->result_array();
        }
        
        
        public function getDetail($id){
            $this->db->where('id',$id);
            $this->db->where('status <>', '2');
            return $this->db->get($this->table['topic'])->row_array();
        }
        
        
        public function insert(){
            $allowedTag = '<div><span><br><b><a><p><strong><img><table><tr><th><td><h1><h2><h3><h4><ul><li><strong><ins><script><tbody><theader><tfooter>';
            $this->db->set('uid',           $this->input->post('uid'));
            $this->db->set('cid',           $this->input->post('cid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('description',   trim(strip_tags($this->input->post('description'), $allowedTag)) );
            if( $this->input->post('display_image') ){
                $this->db->set('display_image',       $this->input->post('display_image'));
            }
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by',     $this->session->userdata('id'));
            $this->db->set('view',          0);
//            $this->db->set('sticky',        $this->input->post('sticky'));
            $this->db->set('status',        $this->input->post('status'));
            $this->db->insert($this->table['topic']);
        }
        
        
        public function update($id){
            $allowedTag = '<div><span><br><b><a><p><strong><img><table><tr><th><td><h1><h2><h3><h4><ul><li><strong><ins><script><tbody><theader><tfooter>';
            $this->db->set('uid',           $this->input->post('uid'));
            $this->db->set('cid',           $this->input->post('cid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('description',   trim(strip_tags($this->input->post('description'), $allowedTag)) );
            $this->db->set('display_image',       $this->input->post('display_image'));
            $this->db->set('update_date',   date('Y-m-d H:i:s'));
            $this->db->set('update_ip',     $this->input->ip_address());
            $this->db->set('update_by',     $this->session->userdata('id'));
//            $this->db->set('sticky',        $this->input->post('sticky'));
            $this->db->set('status',        $this->input->post('status'));
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        public function updateFix($id){
            $this->db->set('description',   $this->input->post('description') );
            $this->db->set('update_date',   date('Y-m-d H:i:s'));
            $this->db->set('update_ip',     $this->input->ip_address());
            $this->db->set('update_by',     $this->session->userdata('id'));
            $this->db->set('status',        '1');
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        
        public function setSticky($id, $isSticky){
            $this->db->set('sticky', $isSticky);
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        
        public function getAllImage($uploadId){
            $this->db->select('*');
            $this->db->where('uid', $uploadId);
            $this->db->where('type', 'i');
            $this->db->where('status', '1');
            return $this->db->get($this->table['file'])->result_array();
        }
        
        
        public function getAllFile($uploadId){
            $this->db->select('*');
            $this->db->where('uid', $uploadId);
            $this->db->where('type', 'f');
            $this->db->where('status', '1');
            return $this->db->get($this->table['file'])->result_array();
        }
        
        
        public function removeFile($id){
            $this->db->set('status', '2');
            $this->db->where('id', $id);
            $this->db->update($this->table['file']);
        }
        
        
        public function remove($id){
            $this->db->set('status',        '2');
            $this->db->set('delete_date',   date('Y-m-d H:i:s'));
            $this->db->set('delete_ip',     $this->input->ip_address());
            $this->db->set('delete_by',     $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        
        /*
         * Category
         */
        public function getCategoryByField($id, $fieldName){
            $this->db->select($fieldName);
            $this->db->where('id', $id);
            $this->db->where('status', '1');
            $rs             = $this->db->get($this->table['cate'])->row_array();
            return @$rs[$fieldName];
        }
        
        
        public function categoryIsCanRemove($id){
            $this->db->select('can_remove');
            $this->db->where('id', $id);
            $rs             =  $this->db->get($this->table['cate'])->row_array();
            if( @$rs['can_remove'] ){
                return true;
            }else{
                return false;
            }
        }
        
        public function categorylist(){
            $outputs = array();
            
            $this->db->select('content_id');
            $this->db->where('status', '1');
            $this->db->where('id <> ', '1');
            $this->db->group_by('content_id');
            $this->db->order_by('seq', 'asc');
            $this->db->order_by('id', 'DESC');
            $contentIdList = $this->db->get($this->table['cate'])->result_array();
            if( !empty($contentIdList) ){
                foreach($contentIdList AS $contentId){
                    $this->db->select('*');
                    $this->db->where('status', '1');
                    $this->db->where('content_id', $contentId['content_id']);
                    $this->db->order_by('id', 'ASC');
                    $datas = $this->db->get($this->table['cate'])->result_array();
                    
                    $output = array(
                        'content_id' => $contentId['content_id'],
                    );
                    foreach($datas AS $data){
                        $output['language'][] = $data['system_languages_code'];
                        $output['content'][] = $data;
                    }
                    $outputs[] = $output;
                }
            }
            return $outputs;
        }
        
        public function categoryRemove($cid){
            $this->db->set('status', '2');
            $this->db->set('delete_date',   date('Y-m-d H:i:s'));
            $this->db->set('delete_ip',     $this->input->ip_address());
            $this->db->set('delete_by',     $this->session->userdata('id'));
            $this->db->where('id', $cid);
            $this->db->where('can_remove',  '1');
            $this->db->update($this->table['cate']);
            
            $this->db->set('status', '2');
            $this->db->where('cid', $cid);
            $this->db->update('system_submenu');
        }
        
        public function insertCategory($datas=array()){
            if( !empty($datas) ){
                $contentId  = $this->getNextContentIdOfCategory();
                foreach( $datas AS $data ){
                    $data['create_date']    = date('Y-m-d H:i:s');
                    $data['create_ip']      = $this->input->ip_address();
                    $data['create_by']      = $this->session->userdata('id');
                    $data['can_remove']     = '1';
                    $data['content_id']     = $contentId;
                    $this->db->insert($this->table['cate'], $data);
                }
            }
            /*
            if( @$_FILES['image_icon'] ){
                $this->load->library('upload');
                $dirPath                = "files/upload/{$this->cmd}/category/";
                @mkdir($dirPath, 0777, true);
                $name                   = $_FILES['image_icon']['name'];
                $config                 = Array();
                $config['upload_path']  = $dirPath;
                $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= 1024*2;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                if ($this->upload->do_upload('image_icon')) {
                    $uploaded           = $this->upload->data();
                    @chmod($uploaded['full_path'], 0777);
                    $filePath           = "{$dirPath}{$uploaded['file_name']}";
                    $this->db->set("image_icon", $filePath);
                    $this->db->where("id", $id);
                    $this->db->update($this->table['cate']);
                }
            }
             * 
             */
            
        }
        
        private function getNextContentIdOfCategory(){
            $query  = "SELECT   max(content_id) AS max "
                    . "FROM     {$this->table['cate']} "
                    . "WHERE    status='1' ";
            $rs = $this->db->query($query)->row_array();
            return @$rs['max'] ? $rs['max']+1 : 1;
        }
        
        
        public function getCategoryDetail($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get($this->table['cate'])->row_array();
        }
        
        
        
        public function updateCategory($id){
            $this->db->set('subject', $this->input->post('subject'));
            $this->db->set('update_date',   date('Y-m-d H:i:s'));
            $this->db->set('update_ip',     $this->input->ip_address());
            $this->db->set('update_by',     $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update($this->table['cate']);
            
            $this->db->set('name',          $this->input->post('subject'));
            $this->db->where('cid',         $id);
            $this->db->update('system_submenu');
            
            
            if( @$_FILES['image_icon'] ){
                $this->load->library('upload');
                $dirPath                = "files/upload/{$this->cmd}/category/";
                @mkdir($dirPath, 0777, true);
                $name                   = $_FILES['image_icon']['name'];
                $config                 = Array();
                $config['upload_path']  = $dirPath;
                $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= 1024*2;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                if ($this->upload->do_upload('image_icon')) {
                    $uploaded           = $this->upload->data();
                    @chmod($uploaded['full_path'], 0777);
                    $filePath           = "{$dirPath}{$uploaded['file_name']}";
                    $this->db->set("image_icon", $filePath);
                    $this->db->where("id", $id);
                    $this->db->update($this->table['cate']);
                    //echo $filePath;
                    //exit(pre($uploaded));
                }
            }
            
            
        }
        
        
        public function getCategorySeq($id, $seq){
            $this->db->set("seq", $seq);
            $this->db->where('id', $id);
            $this->db->update($this->table['cate']);
        }
        
        
    }
?>