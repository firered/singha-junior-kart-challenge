<?php
    class Calendar_model extends CI_Model{
        
        private $table              = Array(
            'topic'         => 'calendar_topic',
            'file'          => 'calendar_file',
            'cate'          => 'calendar_category'
        );
        private $modelName          = 'calendar';
        
        public function getTotalRecord($cid=0){
            $query          = ""
                    . " SELECT      COUNT(id) AS total"
                    . " FROM        {$this->table['topic']}"
                    . " WHERE       status<>'2'"
                    . "";
            if($cid){
                $query      .=" AND cid='{$cid}' ";
            }
            $rs             = $this->db->query($query)->row_array();
            return $rs['total'];
        }
        
        public function showList( $cid=0, $limit=Array(0,10) ){
            $this->db->select('*');
            if( $cid ){
                $this->db->where('cid', $cid);
            }
            $this->db->where('status <> ',  '2');
            $this->db->limit($limit[1], $limit[0]);
             $this->db->order_by('id',   'DESC');
            return $this->db->get($this->table['topic'])->result_array();
        }
        
        
        public function getDetail($id){
            $this->db->where('id',$id);
            $this->db->where('status <>', '2');
            return $this->db->get($this->table['topic'])->row_array();
        }
        
        public function getDetailSubmenu($id){
            $this->db->where('id',$id);
            $this->db->where('status <>', '2');
            return $this->db->get($this->table['submenu'])->row_array();
        }
        
        
        public function insert(){
            $team1Img = $this->uploadImage('team1_img');
            if (!$team1Img) {
                throw new Exception($this->upload->display_errors());
            }
            $team2Img = $this->uploadImage('team2_img');
            if (!$team2Img) {
                throw new Exception($this->upload->display_errors());
            }
            
            $this->db->set('uid',           $this->input->post('uid'));
            $this->db->set('cid',           $this->input->post('cid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('description',   trim(strip_tags($this->input->post('description'), '<div><span><br><b><a><p><strong><img><table><tr><th><td>')) );
            if( $this->input->post('display_image') ){
                $this->db->set('display_image',       $this->input->post('display_image'));
            }
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by',     $this->session->userdata('id'));
            $this->db->set('view',          0);
//            $this->db->set('sticky',        $this->input->post('sticky'));
            $this->db->set('status',        $this->input->post('status'));
            
            $this->db->set('team1_img',     $team1Img);
            $this->db->set('team1',         $this->input->post('team1'));
            $this->db->set('team2_img',     $team2Img);
            $this->db->set('team2',         $this->input->post('team2'));
            $this->db->set('place_match',   $this->input->post('place_match'));
            $this->db->set('date_match',    $this->input->post('date_match'));
            $this->db->set('time_match',    $this->input->post('time_match'));
            $this->db->set('sponsor',    $this->input->post('sponsor'));
            
            $this->db->set('seo_keywords',      $this->input->post('seo_keywords'));
            $this->db->set('seo_description',   $this->input->post('seo_description'));
        
            $this->db->insert($this->table['topic']);
        }
        
        public function insertSubmenu(){
            $this->db->set('uid',           $this->input->post('uid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('url',           strip_tags($this->input->post('url')));
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by',     $this->session->userdata('id'));
            $this->db->set('view',          0);
            $this->db->set('status',        $this->input->post('status'));
            $this->db->insert($this->table['submenu']);
        }
        
        public function update($id){
            $team1Img = null;
            if (@$_FILES['team1_img']['size']) {
                $team1Img = $this->uploadImage('team1_img');
                if (!$team1Img) {
                    throw new Exception($this->upload->display_errors());
                }
            }
            
            $team2Img = null;
            if (@$_FILES['team2_img']['size']) {
                $team2Img = $this->uploadImage('team2_img');
                if (!$team2Img) {
                    throw new Exception($this->upload->display_errors());
                }
            }
            
            
            $this->db->set('uid',           $this->input->post('uid'));
//            $this->db->set('cid',           $this->input->post('cid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('description',   trim(strip_tags($this->input->post('description'), '<div><span><br><b><a><p><strong><img><table><tr><th><td>')) );
            if( $this->input->post('display_image') ){
                $this->db->set('display_image',       $this->input->post('display_image'));
            }
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by',     $this->session->userdata('id'));
//            $this->db->set('sticky',        $this->input->post('sticky'));
            $this->db->set('status',        $this->input->post('status'));
            
            if ($team1Img) {
                $this->db->set('team1_img',     $team1Img);
            }
            $this->db->set('team1',         $this->input->post('team1'));
            if ($team2Img) {
                $this->db->set('team2_img',     $team2Img);
            }
            $this->db->set('team2',         $this->input->post('team2'));
            $this->db->set('place_match',   $this->input->post('place_match'));
            $this->db->set('date_match',    $this->input->post('date_match'));
            $this->db->set('time_match',    $this->input->post('time_match'));
            $this->db->set('sponsor',    $this->input->post('sponsor'));
            
            $this->db->set('seo_keywords',      $this->input->post('seo_keywords'));
            $this->db->set('seo_description',   $this->input->post('seo_description'));
            
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        public function updateSubmenu($id){
            $this->db->set('uid',           $this->input->post('uid'));
            $this->db->set('subject',       $this->input->post('subject'));
            $this->db->set('url',           strip_tags($this->input->post('url')));
            $this->db->set('create_date',   date('Y-m-d H:i:s'));
            $this->db->set('create_ip',     $this->input->ip_address());
            $this->db->set('create_by',     $this->session->userdata('id'));
            $this->db->set('status',        $this->input->post('status'));
            $this->db->where('id', $id);
            $this->db->update($this->table['submenu']);
        }
        
        private function uploadImage($key){
            if( @$_FILES[$key]['size'] ){
                $this->load->library('upload');
                $dirPath                = "files/upload/{$this->modelName}/";
                @mkdir($dirPath, 0777, true);
                $name                   = $_FILES[$key]['name'];
                $config                 = Array();
                $config['upload_path']  = $dirPath;
                $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
                $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
                $config['max_size']	= 1024*2;
                $config['remove_spaces']= true;
                $this->upload->initialize($config);
                $uploadResult = $this->upload->do_upload($key);
                if ($uploadResult) {
                    $uploadedData = $this->upload->data();
                    $filepath = "{$dirPath}{$uploadedData['file_name']}";
                    return $filepath;
                }
            }
            return null;
        }
        
        public function setSticky($id, $isSticky){
            $this->db->set('sticky', $isSticky);
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        
        public function getAllImage($uploadId){
            $this->db->select('*');
            $this->db->where('uid', $uploadId);
            $this->db->where('type', 'i');
            $this->db->where('status', '1');
            return $this->db->get($this->table['file'])->result_array();
        }
        
        
        public function getAllFile($uploadId){
            $this->db->select('*');
            $this->db->where('uid', $uploadId);
            $this->db->where('type', 'f');
            $this->db->where('status', '1');
            return $this->db->get($this->table['file'])->result_array();
        }
        
        
        public function removeFile($id){
            $this->db->set('status', '2');
            $this->db->where('id', $id);
            $this->db->update($this->table['file']);
        }
        
        
        public function remove($id){
            $this->db->set('status',        '2');
            $this->db->set('delete_date',   date('Y-m-d H:i:s'));
            $this->db->set('delete_ip',     $this->input->ip_address());
            $this->db->set('delete_by',     $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update($this->table['topic']);
        }
        
        public function removeSubmenu($id){
            $this->db->set('status',        '2');
            $this->db->set('delete_date',   date('Y-m-d H:i:s'));
            $this->db->set('delete_ip',     $this->input->ip_address());
            $this->db->set('delete_by',     $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update($this->table['submenu']);
        }
        
        
        /*
         * Category
         */
        public function getCategoryByField($id, $fieldName){
            $this->db->select($fieldName);
            $this->db->where('id', $id);
            $this->db->where('status', '1');
            $rs             = $this->db->get($this->table['cate'])->row_array();
            return @$rs[$fieldName];
        }
        
        
        public function categoryIsCanRemove($id){
            $this->db->select('can_remove');
            $this->db->where('id', $id);
            $rs             =  $this->db->get($this->table['cate'])->row_array();
            if( @$rs['can_remove'] ){
                return true;
            }else{
                return false;
            }
        }
        
        /*
        public function categorylist(){
            $outputs = array();
            
            $this->db->select('content_id');
            $this->db->where('status', '1');
            $this->db->where('id <> ', '1');
            $this->db->group_by('content_id');
            $this->db->order_by('seq', 'asc');
            $this->db->order_by('id', 'DESC');
            $contentIdList = $this->db->get($this->table['cate'])->result_array();
            if( !empty($contentIdList) ){
                foreach($contentIdList AS $contentId){
                    $this->db->select('*');
                    $this->db->where('status', '1');
                    $this->db->where('content_id', $contentId['content_id']);
                    $this->db->order_by('id', 'ASC');
                    $datas = $this->db->get($this->table['cate'])->result_array();
                    
                    $output = array(
                        'content_id' => $contentId['content_id'],
                    );
                    foreach($datas AS $data){
                        $output['language'][] = $data['system_languages_code'];
                        $output['content'][] = $data;
                    }
                    $outputs[] = $output;
                }
            }
            return $outputs;
        }
         * 
         */
        
        public function categorylist(){
            $this->db->select('*');
            $this->db->where('status <> ', '2');
            $this->db->order_by('seq', 'asc');
            $this->db->order_by('id', 'DESC');
            $contentIdList = $this->db->get($this->table['cate'])->result_array();
            return $contentIdList;
        }
        
        public function categoryRemove($cid){
            $this->db->set('status', '2');
            $this->db->set('delete_date',   date('Y-m-d H:i:s'));
            $this->db->set('delete_ip',     $this->input->ip_address());
            $this->db->set('delete_by',     $this->session->userdata('id'));
            $this->db->where('id', $cid);
            $this->db->where('can_remove',  '1');
            $this->db->update($this->table['cate']);
            
            $this->db->set('status', '2');
            $this->db->where('cid', $cid);
            $this->db->update('system_submenu');
        }
        
        public function insertCategory($data=array()){
            $data['create_date']    = date('Y-m-d H:i:s');
            $data['create_ip']      = $this->input->ip_address();
            $data['create_by']      = $this->session->userdata('id');
            $data['can_remove']     = '1';
            $this->db->insert($this->table['cate'], $data);
        }
        
        private function getNextContentIdOfCategory(){
            $query  = "SELECT   max(content_id) AS max "
                    . "FROM     {$this->table['cate']} "
                    . "WHERE    status='1' ";
            $rs = $this->db->query($query)->row_array();
            return @$rs['max'] ? $rs['max']+1 : 1;
        }
        
        
        public function getCategoryDetail($id){
            $this->db->select('*');
            $this->db->where('id', $id);
            return $this->db->get($this->table['cate'])->row_array();
        }
        
        
        public function getCategorySeq($id, $seq){
            $this->db->set("seq", $seq);
            $this->db->where('id', $id);
            $this->db->update($this->table['cate']);
        }
        
        
        public function updateCategory($id){
            $this->db->set('subject', $this->input->post('subject'));
            $this->db->set('seq', $this->input->post('seq'));
            $this->db->set('update_date',   date('Y-m-d H:i:s'));
            $this->db->set('update_ip',     $this->input->ip_address());
            $this->db->set('update_by',     $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update($this->table['cate']);
        }
        
        
        public function getTotalRecord_submenu($cid=0){
            $query          = ""
                    . " SELECT      COUNT(id) AS total"
                    . " FROM        {$this->table['submenu']}"
                    . " WHERE       status<>'2'"
                    . "";
            if($cid){
                $query      .=" AND cid='{$cid}' ";
            }
            $rs             = $this->db->query($query)->row_array();
            return $rs['total'];
        }
        
        public function showList_submenu( $limit=Array(0,10) ){
            $this->db->select('*');
            $this->db->where('status <> ',  '2');
            $this->db->limit($limit[1], $limit[0]);
             $this->db->order_by('id',   'asc');
            return $this->db->get($this->table['submenu'])->result_array();
        }
        
    }
?>