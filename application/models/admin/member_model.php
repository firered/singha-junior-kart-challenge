<?php
class Member_model extends CI_Model{
    
    private $table              = Array(
        'topic'         => 'question_result'
    );
    
    public function getTotalRecord($cid=0){
        $query          = ""
                . " SELECT      COUNT(id) AS total"
                . " FROM        {$this->table['topic']}"
                . "";
        if($cid){
            $query      .=" AND cid='{$cid}' ";
        }
        $rs             = $this->db->query($query)->row_array();
        return $rs['total'];
    }
    
    public function showList( $cid=0, $limit=Array(0,10) ){
        $this->db->select('*');
        $this->db->limit($limit[1], $limit[0]);
        $this->db->order_by('id',   'DESC');
        return $this->db->get($this->table['topic'])->result_array();
    }
    
    
    public function remove($id){
        $query = "DELETE FROM member WHERE id='{$id}' ";
        $this->db->query($query);
    }
    
    
    
    public function updateHashtagKey($hashtags=array()){
        if(is_array($hashtags)  && count($hashtags)>0 ){
            $this->db->query("DELETE FROM ig_hashtag_key");
            $batch = array();
            foreach( $hashtags AS $hashtag ){
                $batch[] = array(
                    "name" => $hashtag
                );
            }
            $this->db->insert_batch("ig_hashtag_key", $batch);
        }
    }
    
    public function getHashtagKey(){
        $this->db->select("name");
        $output =  $this->db->get("ig_hashtag_key")->result_array();
        if( $output  &&  count($output)>0 ){
            $keys = array();
            foreach($output AS $hashtag){
                $keys[] = $hashtag['name'];
            }
            return $keys;
        }
        return array();
    }
}