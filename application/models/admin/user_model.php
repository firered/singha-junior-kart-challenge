<?php
class User_model extends CI_Model{
    
    
    
    public function getTotalAdmin(){
        $query              = ""
                . " SELECT          COUNT(id) AS total "
                . " FROM            system_user "
                . " WHERE           is_root='1'     AND"
                . "                 'group'='admin'   AND"
                . "                 status='1'"
                . "";
        $rs                 = $this->db->query($query)->row_array();
        return $rs['total'];
    }
    
    public function getAdminUser($limit=Array(0, 10)){
        $this->db->select("*");
        $this->db->where('is_root',     '1');
        $this->db->where('group',       'admin');
        $this->db->where("status <>",   "2");
        $this->db->order_by('id',       'DESC');
        $this->db->limit($limit[1],     $limit[0]);
        return $this->db->get('system_user')->result_array();
    }
    
    public function insertAdmin(){
        $this->db->set('firstname',     $this->input->post('firstname'));
        $this->db->set('lastname',      $this->input->post('lastname'));
        $this->db->set('username',      $this->input->post('username'));
        $this->db->set('password',      md5($this->input->post('password')));
        $this->db->set('is_root',       '1');
        $this->db->set('group',         'admin');
        $this->db->set('create_date',   date('Y-m-d H:i:s'));
        $this->db->set('create_by',     $this->session->userdata('id'));
        $this->db->set('create_ip',     $this->input->ip_address());
        $this->db->set('status',        $this->input->post('status'));
        $this->db->insert('system_user');
    }
    
    public function updateAdmin($id){
        $this->db->set('firstname',     $this->input->post('firstname'));
        $this->db->set('lastname',      $this->input->post('lastname'));
        $this->db->set('username',      $this->input->post('username'));
        $this->db->set('update_date',   date('Y-m-d H:i:s'));
        $this->db->set('update_by',     $this->session->userdata('id'));
        $this->db->set('update_ip',     $this->input->ip_address());
        $this->db->where('id', $id);
        $this->db->update('system_user');
    }
    
    public function removeAdmin($id){
        $this->db->set('status',        '2');
        $this->db->set('delete_date',   date('Y-m-d H:i:s'));
        $this->db->set('delete_by',     $this->session->userdata('id'));
        $this->db->set('delete_ip',     $this->input->ip_address());
        $this->db->where('id', $id);
        $this->db->update('system_user');
    }
    
    public function getAdminDetail( $id ){
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->where('group', 'admin');
        return $this->db->get('system_user')->row_array();
    }
    
    public function changePasswordAdmin(){
        $this->db->set('password', md5($this->input->post('password')));
        $this->db->where('id',  $this->session->userdata('id'));
        $this->db->update('system_user');
    }
    
    
    
}