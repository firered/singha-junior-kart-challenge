<?php
class Setting_model extends CI_Model{

    public function getAllMenu(){
        $this->db->select('*');
        $this->db->where('seq < ', 500);
        $this->db->order_by('seq');
        return $this->db->get('system_menu')->result_array();
    }


    public function getSubMenu($menuId){
        $this->db->select('*');
        $this->db->where('menu_id', $menuId);
        $this->db->where('status', '1');
        return $this->db->get('system_submenu')->result_array();
    }


    public function setMenuSeq($id, $seq){
        $this->db->set('seq',   $seq);
        $this->db->where('id',  $id);
        $this->db->update('system_menu');
    }

    /**
     * 
     * @param Array $data
     */
    public function updateSettings( $data ){
        if( $data 
                && is_array($data)){
            foreach( $data AS $fieldName => $val ){
                $this->updateSettingByField($fieldName, $val);
            }
        }
    }

    public function updateSettingByField($field, $val){
        $this->db->set($field, $val);
        $this->db->update('system_configuration');
    }


    public function getUserConfig($field){
        $user_id                = $this->session->userdata('id');
        $this->db->select('id');
        $this->db->where('user_id',     $user_id);
        $found                  = $this->db->get('system_config_user')->row_array();
        if(!$found){
            $this->db->set('user_id',   $user_id);
            $this->db->insert('system_config_user');
        }
        $this->db->select($field);
        $this->db->where('user_id',     $user_id);
        $rs                     = $this->db->get('system_config_user')->row_array();
        return $rs[$field];
    }


    public function updateUserConfig($field, $val){
        $user_id                = $this->session->userdata('id');
        $this->db->select('id');
        $this->db->where('user_id',     $user_id);
        $found                  = $this->db->get('system_config_user')->row_array();
        if(!$found){
            $this->db->set('user_id',   $user_id);
            $this->db->insert('system_config_user');
        }
        $this->db->set($field, $val);
        $this->db->where('user_id',     $user_id);
        $this->db->update('system_config_user');
    }


    public function getMenuDetail($menuId){
        $this->db->select("*");
        $this->db->where("id", $menuId);
        return $this->db->get("system_menu")->row_array();
    }


    public function updateMenu($id){
        if( @$_FILES['image_icon'] ){
            $this->load->library('upload');
            $dirPath                = "files/upload/{$this->cmd}/category/";
            @mkdir($dirPath, 0777, true);
            $name                   = $_FILES['image_icon']['name'];
            $config                 = Array();
            $config['upload_path']  = $dirPath;
            $config['file_name']	= date('Y-m-d_').substr(md5(time().$name), 0, 10);
            $config['allowed_types']= 'gif|jpg|png|jpeg|bmp';
            $config['max_size']	= 1024*2;
            $config['remove_spaces']= true;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image_icon')) {
                $uploaded           = $this->upload->data();
                @chmod($uploaded['full_path'], 0777);
                $filePath           = "{$dirPath}{$uploaded['file_name']}";
                $this->db->set("logo", $filePath);
                $this->db->where("id", $id);
                $this->db->update('system_menu');
            }
        }
    }





    public function getLanguages(){
        $this->db->select('*');
        return $this->db->get('system_languages')->result_array();
    }

    public function getLanguageFromCode($code){
        $this->db->select('*');
        $this->db->where('code', $code);
        return $this->db->get('system_languages')->row_array();
    }

    public function getSystemSupportedLanguages(){
        $this->db->select('*');
        $this->db->where('status !=', '2');
        return $this->db->get('system_language_supported')->result_array();
    }

    /**
     * 
     * @param Array $languageCodes
     */
    public function addSystemSupportedLanguage( $languageCodes ){
        if(is_array($languageCodes)
                && count($languageCodes)>0 ){
            $insert = array();

            $languages = $this->getLanguages();
            $availableCodes = array();
            foreach($languages AS $lang){
                $availableCodes[] = $lang['code'];
            }
            $currentUserId = $this->session->userdata('id');
            foreach( $languageCodes AS $languageCode ){
                if(is_numeric(array_search($languageCode, $availableCodes)) ){
                    $insert[] = array(
                        'system_languages_code' => $languageCode,
                        'create_date' => date('Y-m-d H:i:s'),
                        'create_ip'             => $this->input->ip_address(),
                        'create_by'             => $currentUserId,
                        'status'                => '1'
                    );
                    $this->db->insert_batch('system_language_supported', $insert);
                }
            }
        }
    }

    public function changeSupportedLanguageStatus( $id, $status ){
        $this->db->set('status', $status);
        $this->db->set('update_date', date('Y-m-d H:i:s'));
        $this->db->set('update_ip', $this->input->ip_address());
        $this->db->set('update_by', $this->session->userdata('id'));
        $this->db->where('id', $id);
        $this->db->update('system_language_supported');
    }


    public function initialLanguages(){
        $html   = file_get_html('http://www.lingoes.net/en/translator/langcode.htm');
        $tr_ls  = $html->find('body > table > tbody tr');
        $insert_batch = array();
        $codes = array();
        foreach( $tr_ls AS $tr ){
            $td_ls = $tr->find('td');
            $code = trim($td_ls[0]->text());
            $name = trim($td_ls[1]->text());

            if(! is_numeric(array_search($code, $codes))){
                $codes[] = $code;
                $insert_batch[] = array(
                    'code' => $code,
                    'name' => $name
                );
            }

        }
        $this->db->insert_batch('system_languages', $insert_batch);
    }

    public function getSupportedLanguageById($id){
        $this->db->select('*');
        $this->db->where('status', '1');
        $this->db->where('id', $id);
        return $this->db->get('system_language_supported')->row_array();
    }


    public function removeLanguageFromSupportedSystem($languageCode){
        $tbl_ls     = $this->getRegisteredSysLanguage($languageCode);
        /* Remove Contents by Language code */
        if( $tbl_ls ){
            foreach( $tbl_ls AS $table ){
                $this->db->set('status', '2');
                $this->db->set('delete_date', date('Y-m-d H:i:s'));
                $this->db->set('delete_ip', $this->input->ip_address());
                $this->db->set('delete_by', $this->session->userdata('id'));
                $this->db->where('system_languages_code', $languageCode);
                $this->db->update($table['tbl_name']);
                /* Unregister table'name language  */
                $this->unregisterSysLanguageById($table['id']);
            }
        };
        /* Remove supported language from system */
        $this->db->set('status', '2');
        $this->db->set('delete_date', date('Y-m-d H:i:s'));
        $this->db->set('delete_ip', $this->input->ip_address());
        $this->db->set('delete_by', $this->session->userdata('id'));
        $this->db->where('reserved', '0');
        $this->db->where('system_languages_code', $languageCode);
        $this->db->update('system_language_supported');
    }

    private function getRegisteredSysLanguage($languageCode){
        $this->db->select("*");
        $this->db->where('system_languages_code', $languageCode);
        return $this->db->get('system_language_supported_tblregist')->result_array();
    }

    public function registeredSysLangauge($tableName, $languageCode){
        if( !$this->getRegisteredSysLanguage($languageCode) ){
            $this->db->set('tbl_name', $tableName);
            $this->db->set('system_languages_code', $languageCode);
            $this->db->insert('system_language_supported_tblregist');
        }
    }

    private function unregisterSysLanguageById($id){
        $this->db->where('id', $id);
        $this->db->delete('system_language_supported_tblregist');
    }


}