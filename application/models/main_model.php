<?php
class Main_model extends CI_Model{
    public function getBanner() {
        $query = "  SELECT      *
                    FROM        banner_topic
                    WHERE       status='1'
                    ORDER BY    id DESC";
        return $this->db->query($query)->result_array();
    }    
    
    public function getActivityScore() {
        $query = "  SELECT      *
                    FROM        activity_topic
                    WHERE       id='1' ";
        return $this->db->query($query)->row_array();
    }
    
    public function getActivityEvent() {
        $query = "  SELECT      *
                    FROM        activity_topic
                    WHERE       id='2' ";
        return $this->db->query($query)->row_array();
    }

    public function getActivityKartingTrack() {
        $query = "  SELECT      *
                    FROM        activity_topic
                    WHERE       id='3' ";
        return $this->db->query($query)->row_array();
    }

    public function getVDO() {
        $query = "  SELECT      subject, display_image, vdo_mp4, vdo_webm, vdo_ogv 
                    FROM        vdo_topic
                    WHERE       status='1'
                    ORDER BY    id DESC";
        $rs = $this->db->query($query)->result_array();
        for ($i = 0;  $i < count($rs);  $i++) {
            $rs[$i]['display_image'] = base_url($rs[$i]['display_image']);
            if ($rs[$i]['vdo_mp4']) {
                $rs[$i]['vdo_mp4'] = base_url($rs[$i]['vdo_mp4']);
            }
            if ($rs[$i]['vdo_webm']) {
                $rs[$i]['vdo_webm'] = base_url($rs[$i]['vdo_webm']);
            }
            if ($rs[$i]['vdo_ogv']) {
                $rs[$i]['vdo_ogv'] = base_url($rs[$i]['vdo_ogv']);
            }
        }
        return $rs;
    }

    public function getLastNews() {
        $query = "  SELECT      *
                    FROM        news_topic
                    WHERE       status='1'
                    ORDER BY    id DESC
                    LIMIT       1 ";
        return $this->db->query($query)->row_array();
    }

    public function getNewsById($id) {
        $query = "  SELECT      *
                    FROM        news_topic
                    WHERE       id='{$id}' ";
        return $this->db->query($query)->row_array();
    }

    public function getNews($exceptId=0, $year = 0, $start=0, $limit = 20) {
        $query = "  SELECT      *
                    FROM        news_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $query .= " ORDER BY    id DESC "
                . " LIMIT       {$start}, {$limit}";
        return $this->db->query($query)->result_array();
    }
    
    public function countNews($exceptId=0, $year = 0) {
        $query = "  SELECT      COUNT(id) AS total
                    FROM        news_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $rs = $this->db->query($query)->row_array();
        return $rs['total'];
    }
    
    public function getNewsYears() {
        $query  = " SELECT      YEAR(create_date) AS year"
                . " FROM        news_topic"
                . " WHERE       status='1' "
                . " GROUP BY    year"
                . " ORDER BY    year DESC";
        $years = $this->db->query($query)->result_array();
        $output = array();
        foreach ($years AS $year) {
            $output[] = $year['year'];
        }
        if (count($output) > 0 && $output[0] != date('Y')) {
            $output = array_merge(array(date('Y')), $output);
        }
        return $output;
    }

    public function getBannerById($id) {
        $query = "  SELECT      *
                    FROM        banner_topic
                    WHERE       id='{$id}' ";
        return $this->db->query($query)->row_array();
    }

    public function getPhoto($exceptId=0, $year = 0, $start=0, $limit = 20) {
        $query = "  SELECT      *
                    FROM        photo_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $query .= " ORDER BY    id DESC "
                . " LIMIT       {$start}, {$limit}";
        return $this->db->query($query)->result_array();
    }
    
    public function countPhoto($exceptId=0, $year = 0) {
        $query = "  SELECT      COUNT(id) AS total
                    FROM        photo_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $rs = $this->db->query($query)->row_array();
        return $rs['total'];
    }
    
    public function getPhotoById($id) {
        $query = "  SELECT      *
                    FROM        photo_topic
                    WHERE       id='{$id}' ";
        $rs = $this->db->query($query)->row_array();
        if ($rs) {
            $query = "  SELECT      *"
                    . " FROM        photo_file"
                    . " WHERE       uid='{$rs['uid']}' "
                    . "             AND status='1'"
                            . "     AND type='i' "
                    . " ORDER BY    id DESC";
            $images = $this->db->query($query)->result_array();
            $rs['_image'] = $images;
        }
        return $rs;
    }
    
    public function getPhotoYears() {
        $query  = " SELECT      YEAR(create_date) AS year"
                . " FROM        photo_topic"
                . " WHERE       status='1' "
                . " GROUP BY    year"
                . " ORDER BY    year DESC";
        $years = $this->db->query($query)->result_array();
        $output = array();
        foreach ($years AS $year) {
            $output[] = $year['year'];
        }
        if (count($output) > 0 && $output[0] != date('Y')) {
            $output = array_merge(array(date('Y')), $output);
        }
        return $output;
    }
    
    public function getVDOList($exceptId=0, $year = 0, $start=0, $limit = 20) {
        $query = "  SELECT      *
                    FROM        vdo_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $query .= " ORDER BY    id DESC "
                . " LIMIT       {$start}, {$limit}";
        return $this->db->query($query)->result_array();
    }
    
    public function countVDO($exceptId=0, $year = 0) {
        $query = "  SELECT      COUNT(id) AS total
                    FROM        vdo_topic
                    WHERE       status='1' ";
        if ($exceptId) {
            $query .= "   AND id <> '{$exceptId}' ";
        }
        if ($year) {
            $query .= "   AND YEAR(create_date) BETWEEN '{$year}' AND '{$year}' ";
        }
        $rs = $this->db->query($query)->row_array();
        return $rs['total'];
    }
    
    public function getVDOById($id) {
        $query = "  SELECT      *
                    FROM        vdo_topic
                    WHERE       id='{$id}' ";
        $rs = $this->db->query($query)->row_array();
        return $rs;
    }
    
    public function getVDOYears() {
        $query  = " SELECT      YEAR(create_date) AS year"
                . " FROM        vdo_topic"
                . " WHERE       status='1' "
                . " GROUP BY    year"
                . " ORDER BY    year DESC";
        $years = $this->db->query($query)->result_array();
        $output = array();
        foreach ($years AS $year) {
            $output[] = $year['year'];
        }
        if (count($output) > 0 && $output[0] != date('Y')) {
            $output = array_merge(array(date('Y')), $output);
        }
        return $output;
    }
}