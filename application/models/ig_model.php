<?php
class Ig_model extends CI_Model{
    
    public function addData($data, $hashtagName){
        if( is_array($data)  &&  count($data)>0 ){
            foreach( $data AS $item ){
                $ig_id = $item->id;
                if( !$this->isFound($ig_id) ){
                    $this->db->insert("ig_hashtag", array(
                        "ig_id"     => $ig_id,
                        "hashtag"   => $hashtagName,
                        "data"      => json_encode($item),
                        'status'    => "0",
                        'ig_create_date' => date("Y-m-d H:i:s", $item->created_time)
                    ));
                }
            }
        }
    }
    
    
    
    
    private function isFound($IgId){
        $this->db->select('*');
        $this->db->where('ig_id', $IgId);
        return count($this->db->get("ig_hashtag")->result_array())>0 ? true : false;
    }
    
    
    public function showList( $limit=Array(0,10) ){
        $this->db->select('*');
        $this->db->where('status',  '1');
        $this->db->limit($limit[1], $limit[0]);
        $this->db->order_by('ig_create_date',   'DESC');
        return $this->db->get("ig_hashtag")->result_array();
    }
    
    public function getHashtagKey(){
        $this->db->select("name");
        $output =  $this->db->get("ig_hashtag_key")->result_array();
        if( $output  &&  count($output)>0 ){
            $keys = array();
            foreach($output AS $hashtag){
                $keys[] = $hashtag['name'];
            }
            return $keys;
        }
        return array();
    }
    
    
    public function showEventList( $limit=Array(0,10) ){
        $this->db->select('*');
        $this->db->where('status',  '1');
        $this->db->limit($limit[1], $limit[0]);
        $this->db->order_by('id',   'DESC');
        return $this->db->get("event")->result_array();
    }
    
    
}