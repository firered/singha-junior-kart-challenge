
  
  $(document).ready(function(){
		iframeModalOpen();
		
		var viewport = $('meta[name="viewport"]');

		if (screen.width <= 640) {
			viewport.attr("content", "width=640, initial-scale=0.5"); 
		}else if (screen.width > 640 || screen.width < 992 ){
			viewport.attr("content", "width=768, initial-scale=1"); 
		}
	
		
		 else{
			viewport.attr("content", "width=1170, initial-scale=1"); 
		}
		/*
		$(".various").fancybox({
			margin:0,
			padding:0,
			fitToView	: true,
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
		 	autoDimensions: true,

		});*/
		$('#owl-banner').owlCarousel({
			loop:true,
			margin:10,
			nav:false,
			items:1
		
		})
		
		$('#owl-tabSlide').owlCarousel({
			loop:false,
			margin:0,
			nav:true,
			items:1,
			dots:false
		
		})
		
		$('#owl-itemList').owlCarousel({
			loop:true,
			margin:30,
			responsiveClass:true,
			responsive:{
				0:{
					items:2,
					nav:true,
					margin:15,
				},
				600:{
					items:2,
					nav:true,
					margin:15,
				},
				1000:{
					items:3,
					nav:true,
					loop:false
				}
			}
		})
		
		
		$('#toggleNav').on('click', function (e) {
			e.preventDefault();
			$('#navigation').toggleClass('open');
			$('body').toggleClass('overlay');
			$('.wrapToggle').toggleClass('wrapToggleClose');
			
			
		});
		
		$('#btnlatestupdate').on('click',function(){
			 var target = $(this).attr('href');
            $.scrollTo($(target), 800, {axis: 'y',offset:-50});
			
			 $('#navigation').removeClass('open');
			$('body').removeClass('overlay');
			$('.wrapToggle').removeClass('wrapToggleClose');
			
            return false;
		});
		
		
		
		
  });
  $(window).resize(function () {
        var w = $(window).width();

        if (w > 992) {
           $('#navigation').removeClass('open');
			$('body').removeClass('overlay');
			$('.wrapToggle').removeClass('wrapToggleClose');
        }

    });

  function iframeModalOpen(){

		$('.modalButton').on('click', function(e) {
			var src = $(this).attr('data-src');
			var width = $(this).attr('data-width') || 640; 
			var height = $(this).attr('data-height') || 360; 

			var allowfullscreen = $(this).attr('data-video-fullscreen'); 
			$("#myModal iframe").attr({
				'src': src,
				'height': height,
				'width': width,
				'allowfullscreen':''
			});
		});

		
		$('#myModal').on('hidden.bs.modal', function(){
			$(this).find('iframe').html("");
			$(this).find('iframe').attr("src", "");
			
		});
		$('#myModal').on('show.bs.modal', function(){
			$('#navigation').removeClass('open');
			$('body').removeClass('overlay');
			$('.wrapToggle').removeClass('wrapToggleClose');
		});
	}
  
