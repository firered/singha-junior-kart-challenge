$(window).load(function(){
	$("#video_relate").mCustomScrollbar({
		axis:"y",
	});
	// media_init();
	// $("#video_relate").find(".item").click(function(){
	// 	var x = $(this).data("video-index");
	// 	set_media(x);
	// });
});

$(document).ready(function () { 
	// $('div.video-background').html(videoHTML(1)); 
	// player = videojs('#video-js'); 

	video_init();
	$("#video_relate").find(".item").click(function () { 
		var video_number = $(this).data("video-index"); 
		// player.dispose(); 
		$('#video_container').find('.video_panel').html(videoHTML(video_number)); 
		player = videojs('#singha_video'); 
	}); 
});
// =================================[ YOUTUBE ]=================================
var video_id = [
	"8Kf3K0Pd9Do",
	"8Kf3K0Pd9Do",
	"8Kf3K0Pd9Do",
	"8Kf3K0Pd9Do"
];

function media_init(){
	$("#video_watch").attr("src","https://www.youtube.com/embed/"+video_id[0]);

	for(var i in video_id){
		fire_gallery_thumb(video_id[i],i);
	}
	
}

function set_media(e){
	$("#video_watch").attr("src","https://www.youtube.com/embed/"+video_id[e]);
}

function fire_gallery_thumb(e,i){
	var invert = video_id.length -i;
	var item = "<div class='item' data-video-index='"+i+"'></div>";
	$("#items_base").append(item);
	var imageCover = "http://img.youtube.com/vi/"+e+"/maxresdefault.jpg";
	$("#items_base").find(".item").eq(i).css("background-image","url("+imageCover+")");
}

//var video_path = "assets/media/";
var player;

function video_init(){
	$('#video_container').find('.video_panel').html(videoHTML(0));
	make_playlist();
}

function make_playlist(){
	for(var i in video_source){
		make_video_item(i);
	}
}

function make_video_item(i){
	var item = "<div class='item' data-video-index='"+i+"'></div>";
	$("#items_base").append(item);
	$("#items_base").find(".item").eq(i).css("background-image","url("+ video_source[i].display_image +")");
}
function videoHTML(video_number) {
	var video_str = '<video id="singha_video" class="video-js vjs-default-skin" controls preload="auto" width="500" height="500" poster="'+ video_source[video_number].display_image +'" data-setup="{}">';

	video_str += '<source src="'+ video_source[video_number].vdo_mp4 +'" type="video/mp4">';
	video_str += '<source src="'+ video_source[video_number].vdo_webm +'" type="video/webm">';
	video_str += '<source src="'+ video_source[video_number].vdo_ogv +'" type="video/ogg">';
    video_str += '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>';
    video_str += '</video>';
                                                        
	return video_str;
} 


