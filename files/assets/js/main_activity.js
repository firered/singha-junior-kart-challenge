$(document).ready(function() {
    $(".tabs_menu li").click(function(e) {
        e.preventDefault();
        $(this).addClass("current");
        $(this).siblings().removeClass("current");
        var tab = $(this).data("href");
        $(".tab_content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});
$(window).load(function(){
	$("#tab_1").mCustomScrollbar({
		axis:"y",
	});
	$("#tab_2").mCustomScrollbar({
		axis:"y",
	});
	$("#tab_1").mCustomScrollbar({
		axis:"y",
	});
});