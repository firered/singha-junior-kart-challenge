var svg1, svg2, svg3, svg4,
    svg1Props = {
        type: 'delayed',
        duration: 100,
        start: 'manual',
        pathTimingFunction: Vivus.LINEAR,
        animTimingFunction: Vivus.LINEAR
    },
    svg2Props = {
        type: 'delayed',
        duration: 100,
        start: 'manual',
        pathTimingFunction: Vivus.LINEAR,
        animTimingFunction: Vivus.LINEAR
    },
    svg3Props = {
        type: 'delayed',
        duration: 100,
        start: 'manual',
        pathTimingFunction: Vivus.LINEAR,
        animTimingFunction: Vivus.LINEAR
    },
    svg4Props = {
        type: 'delayed',
        duration: 100,
        start: 'manual',
        pathTimingFunction: Vivus.LINEAR,
        animTimingFunction: Vivus.LINEAR
    },
    svg1Conf, svg2Conf, svg3Conf, svg4Conf;
var currentPage = 1;
var winWidth = $(window).width();
var winHeight = $(window).height();


$(document).ready(function(){
    $(".container.root").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: false,
        loop: false,
        updateURL: false,
        pagination: false,
        beforeMove: function(nextIndex) {
            try {
                if (nextIndex > 1) {
                    $('#scrolltotop').show();
                } else {
                    $('#scrolltotop').hide('fast');
                }
                if (nextIndex < 4) {
                    $('#scrolltoBottom').show();
                } else {
                    $('#scrolltoBottom').hide('fast');
                }

                switch (nextIndex) {
                    case 1: { svg2.play(-3); } break;
                    case 2: { svg3.play(-3); } break;
                    case 3: { svg4.play(-3); } break;
                    case 4: { } break;
                }
            } catch (e) {
                console.log(e);
            }
        },
        afterMove: function(nextIndex) {
            try {
                currentPage = nextIndex;
                switch (nextIndex) {
                    case 1: { } break;
                    case 2: { svg2.play(); } break;
                    case 3: { svg3.play(); } break;
                    case 4: { svg4.play(); } break;
                }
            } catch (e) {
                console.log(e);
            }
        }
    });
    $('[data-fancybox]').fancybox({
        youtube : { controls : 0, showinfo : 0 }
    });
    $(".contentScrollbar").mCustomScrollbar({
        autoHideScrollbar: false,
        theme: "rounded"
    });

    $("html, body").bind({'scroll': 
        function(e) {
            if (e.currentTarget.scrollTop >= 150) {
                $('#scrolltotop').show();
            } else {
                $('#scrolltotop').hide('fast');
            }
        }
    });
//    $(".container.root").moveTo(2);

    init();
    $('.navbar-collapse').on('show.bs.collapse', function () {
        console.log('show.bs.collapse');
        $('#svg-highlight').hide();
        $('.page2').addClass('expand');
        $('.page1').addClass('expand');
    });
    $('.navbar-collapse').on('hide.bs.collapse', function () {
        $('.page1').removeClass('expand');
        $('.page2').removeClass('expand');
        $('#svg-highlight').show();
    });
    
    
    $(window).resize(function(){
        $('#svg-highlight').html('');
        $('#svg-vdo').html('');
        $('#svg-rule').html('');
        $('#svg-announce').html('');
        init();
    });
});

function goToTop(){
    $(".main").moveTo(1);
    if ($(window).width() <= 770) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }
}

function goToBottom(){
    var page = currentPage < 4 ? currentPage + 1 : 4;
    currentPage = page;
    console.log('page', page);
    $(".container.root").moveTo(page);
}

function initSVG() {
    initHighlightSVG();
    try { svg1.play(); } catch (e) { console.log(e) }
    initSVGVdo();
    initSVGRule();
    initSVGAnnounce();
}

function facebookGetMe() {
    FB.api('/me?fields=name,email', function(rs) {
        console.log('Successful login: ', rs);
        $('.form-regist[name=name]').val(rs.name);
        if (rs.email) {
            $('.form-regist[name=email]').val(rs.email);
        }
    });
}
function fbLogin() {
    FB.login(function(response) {
        console.log('Login response', response);
        if (response && response.status === "connected") {
            facebookGetMe();
        }
    }, {
        scope: 'public_profile,email',
        return_scopes: true
    });
}
function onClickConditionAndPrize() {
    $(".container.root").moveTo(3);
}
function onClickAnnounce() {
    $(".container.root").moveTo(4);
}
function mbClickPlayQuiz() {
    $.fancybox.close();
    $('#btn-quize-popup').click();
}
function mbClickRulesAndConditions() {
    $.fancybox.close();
    /*
    $('html,body').animate({
        scrollTop: $('.page3').offset().top - $('.navbar-nav.mobile').height(),
    }, 'slow');
    */
   onClickConditionAndPrize();
}
function mbClickAnnounce() {
    $.fancybox.close();
    /*
    $('html,body').animate({
        scrollTop: $('.page4').offset().top - $('.navbar-nav.mobile').height(),
    }, 'slow');
    */
   onClickAnnounce();
}


function IsImageOk(img) {
    // During the onload event, IE correctly identifies any images that
    // weren’t downloaded as not complete. Others should too. Gecko-based
    // browsers act like NS4 in that they report this incorrectly.
    if (!img.complete) {
        return false;
    }

    // However, they do have two very useful properties: naturalWidth and
    // naturalHeight. These give the true size of the image. If it failed
    // to load, either of these should be zero.
    if (img.naturalWidth === 0) {
        return false;
    }

    // No other way of checking: assume it’s ok.
    return true;
}

function init() {
    /*
    var imageTarget = $('img.highlight-image');
    imageTarget.on('load', function() {
        initSVG();
        initHighlightIcon();
    })
    .on('error', function() {
        console.log("error loading image");
    })
    .attr("src", imageTarget.attr("src"));
    */
    var imageTarget = $('img.highlight-image').toArray()[0];
    if (imageTarget) {
        if (!IsImageOk(imageTarget)) {
            setTimeout(function(){ init(); }, 500);
        } else {
            initSVG();
            initHighlightIcon();
        }
    }
}


/* Highlight section */
function getDesktopOrMobile (x, y) {
    return winWidth > 770 ? x : y;
}

function initHighlightIcon() {
    var rootOffset = $('img.highlight-image').offset();
    var targetDiv = {
        offset: { top: rootOffset.top, left: rootOffset.left },
        position: $('img.highlight-image').position(),
        width: $('img.highlight-image').width(),
        height: $('img.highlight-image').height()
    };

    var img1 = {
        w:      targetDiv.width *   getDesktopOrMobile(.18148, .23125),
        h:      targetDiv.height *  getDesktopOrMobile(.3004, .2258),
        left:   targetDiv.position.left + (targetDiv.width * getDesktopOrMobile(.12407, .18)),
        top:    targetDiv.position.top + (targetDiv.height * getDesktopOrMobile(.10328, .35)),
    };
    $('.container_highlight .img1').css({
        'width': img1.w,
        'height': img1.h,
        'left': img1.left,
        'top': img1.top,
        'display': 'block'
    });

    var img2 = {
        w:      targetDiv.width * getDesktopOrMobile(.13148, .16718),
        h:      targetDiv.height * getDesktopOrMobile(.2159, .1582),
        left:   targetDiv.position.left + (targetDiv.width * getDesktopOrMobile(.3194, .4203)),
        top:    targetDiv.position.top + (targetDiv.height * getDesktopOrMobile(.24, .45)),
    };
    $('.container_highlight .img2').css({
        'width': img2.w,
        'height': img2.h,
        'left': img2.left,
        'top': img2.top,
        'display': 'block'
    });

    var img3 = {
        w:      targetDiv.width * getDesktopOrMobile(.10185, .13281),
        h:      targetDiv.height * getDesktopOrMobile(.3129, .13056),
        left:   targetDiv.position.left + (targetDiv.width * getDesktopOrMobile(.47407, .65)),
        top:    targetDiv.position.top + (targetDiv.height * getDesktopOrMobile(.23, .46)),
    };
    $('.container_highlight .img3').css({
        'width': img3.w,
        'height': img3.h,
        'left': img3.left,
        'top': img3.top,
        'display': 'block'
    });

    var img4 = {
        w:      targetDiv.width * getDesktopOrMobile(.1138, .1484),
        h:      targetDiv.height * getDesktopOrMobile(.20031, .149),
        left:   targetDiv.position.left + (targetDiv.width * getDesktopOrMobile(.03981, .059375)),
        top:    targetDiv.position.top + (targetDiv.height * getDesktopOrMobile(.71048, .76)),
    };
    $('.container_highlight .img4').css({
        'width': img4.w,
        'height': img4.h,
        'left': img4.left,
        'top': img4.top,
        'display': 'block'
    });

    var img5 = {
        w:      targetDiv.width * getDesktopOrMobile(.1592, .20625),
        h:      targetDiv.height * getDesktopOrMobile(.27856, .20737),
        left:   targetDiv.position.left + (targetDiv.width * getDesktopOrMobile(.55092, .72)),
        top:    targetDiv.position.top + (targetDiv.height * getDesktopOrMobile(.61658, .7)),
    };
    $('.container_highlight .img5').css({
        'width': img5.w,
        'height': img5.h,
        'left': img5.left,
        'top': img5.top,
        'display': 'block'
    });
    var conf5 = {
        w:      targetDiv.width * getDesktopOrMobile(.14959, .20156),
        h:      targetDiv.height * getDesktopOrMobile(.06475, .052227),
        left:   targetDiv.offset.left + (targetDiv.width * getDesktopOrMobile((winWidth <= 990 ? .65 : .705), .58)),
        top:    targetDiv.offset.top + (targetDiv.height * getDesktopOrMobile(.46699, .34869)),
    };
    $('a.play-quiz-btn').css({
//        'width': conf5.w,
//        'height': conf5.h,
        'left': conf5.left,
        'top': conf5.top,
        'display': 'flex'
    });
}

function initHighlightSVG_desktop() {
    var draw = SVG('svg-highlight');
    var stroke = 6;
    var m0 = winWidth * .78;

    var divOffset = $('img.highlight-image').offset();
    var targetDiv = {
        offset: { top: divOffset.top, left: divOffset.left },
        position: $('img.highlight-image').position(),
        width: $('img.highlight-image').width(),
        height: $('img.highlight-image').height()
    };
    
    var widthMultiplier = .885;
    if (winWidth >= 1600) {
        stroke = 7;
        widthMultiplier = .886;
    } else if (winWidth >= 1100) {
        stroke = 5;
        widthMultiplier = .887;
    } else if (winWidth >= 100) {
        stroke = 5;
        widthMultiplier = .8875;
    }
    var M = {
        x: targetDiv.offset.left + (targetDiv.width * widthMultiplier), 
        y: targetDiv.offset.top + (targetDiv.height * .44333),
    };
    var L1 = {
        x: M.x,
        y: M.y + (targetDiv.height * .7)
    };
    var pathData =  "M "+ M.x+" "+ M.y+" "+
                    "L "+ L1.x+" "+ L1.y+" ";
    svg1Conf = { M, L1 };
    draw.path(pathData).stroke({ width: stroke, color: 'black' });
}

function initHighlightSVG_mobile() {
    var draw = SVG('svg-highlight');
    var stroke = 6;
    var y_add = 30;

    if (winWidth <= 500) {
        stroke = 3.5;
    } else if (winWidth <= 550) {
        stroke = 5;
        y_add = 35;
    } else if (winWidth <= 600) {
        stroke = 5;
        y_add = 43;
    } else if (winWidth <= 650) {
        stroke = 5;
        y_add = 56;
    } else if (winWidth <= 700) {
        stroke = 5;
        y_add = 56;
    } else if (winWidth <= 700) {
        stroke = 5;
        y_add = 56;
    }  else if (winWidth <= 780) {
        stroke = 5.8;
        y_add = 56;
    }

    var m0 = winWidth * .78;
    var divOffset = $('img.highlight-image').offset();
    var targetDiv = {
        offset: { top: divOffset.top, left: divOffset.left },
        position: $('img.highlight-image').position(),
        width: $('img.highlight-image').width(),
        height: $('img.highlight-image').height()
    };
    var containerDimension = {
        width: 640, height: 651
    };

    var M = {
        x: targetDiv.offset.left + (targetDiv.width * .9437),
        // y: targetDiv.height * .24
        y: targetDiv.offset.top + (((targetDiv.width * (containerDimension.height / containerDimension.width)) * .24) + y_add)
    };
    var L1 = {
        x: M.x,
        y: M.y + (targetDiv.height * 1)
    };
    var pathData =  "M "+ M.x+" "+ M.y+" "+
                    "L "+ L1.x+" "+ L1.y+" ";
    svg1Conf = { M, L1, targetDiv };
    draw.path(pathData).stroke({ width: stroke, color: 'black' });
}

function initHighlightSVG() {
    if (SVG.supported) {
        if ($(window).width() > 770) {
            initHighlightSVG_desktop();
        } else {
            initHighlightSVG_mobile();
        }
        svg1 = new Vivus($('#svg-highlight svg').toArray()[0], svg1Props);
    }
}
/* END: Highlight section */







/* VDO Section */
function initSVGVdo_desktop() {
    var draw = SVG('svg-vdo');
    var _w = $('body').width() - $('.page1 #svg-highlight').width();
    var imageDimension = { width: 157, height: 38 };

    var m0 = svg1Conf.L1.x;
    var Q1 = {
        x1: m0 - 10,
        y1: winHeight * .2,
        x2: winWidth * .65
    };
    var L1 = {
        x: winWidth * .6,
        y: Q1.y1
    };
    var M2 = {
        x: (winWidth * .4),
        y: L1.y
    };
    var L2 = {
        x: winWidth * .2,
        y: L1.y
    };
    var Q2 = {
        x1: winWidth * .05,
        y1: winHeight * .2,
        x2: winWidth * .05,
        y2: winHeight * .5
    };
    var L3 = {
        x: Q2.x2,
        y: (Q2.y2) + (winHeight * .27)
    };
    var Q3 = {
        x1: winWidth * .055,
        y1: L3.y + (winHeight * .3),
        x2: winWidth * .2,
        y2: L3.y + (winHeight * .3)
    };

    /* Dynamic position for VDO Title */
    var spaceTextDiff = L1.x - M2.x;
    if (spaceTextDiff > imageDimension.width + 40) {
        var _diff = spaceTextDiff - (imageDimension.width + 40);
        M2.x += _diff / 2;
        L1.x -= _diff / 2;
    }

    svg2Conf = { m0, Q1, L1, Q2, Q3 };
    var pathData =  "M "+ m0+" 0 "+
                    "L "+ m0+" 5 "+
                    "Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y1+" "+
                    "L "+ L1.x+" "+ L1.y+" "+
                    "M "+ M2.x+" "+ M2.y+" "+
                    "L "+ L2.x+" "+ L2.y+" "+
                    "Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    "L "+ L3.x+" "+ L3.y+" "+
                    "Q "+ Q3.x1+" "+ Q3.y1+" "+ Q3.x2+" "+ Q3.y2+" ";
    draw.path(pathData).stroke({ width: 6, color: 'black' });

    var image = draw.image('images/vdo_teaser.png').loaded(function(loader) {
        var diff = L1.x - M2.x;
        var startAtX = M2.x + (diff / 2) - (imageDimension.width / 2);
        var startAtY = Q1.y1 - (imageDimension.height / 2);

        this.size(imageDimension.width, imageDimension.height).move(startAtX, startAtY);
    });
}

function initSVGVdo_mobile() {
    var draw = SVG('svg-vdo');
    var _w = $('body').width() - $('.page1 #svg-highlight').width();
    var imageDimension = { width: 135, height: 28 };
    var stroke = 6;
    if (winWidth <= 500) {
        stroke = 3.5;
    } else if (winWidth <= 550) {
        stroke = 5;
    } else if (winWidth <= 600) {
        stroke = 5;
    } else if (winWidth <= 650) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    }  else if (winWidth <= 780) {
        stroke = 5.8;
    }

    var m0 = {
      x: svg1Conf.L1.x,
      y: 0
    };
    var Q1 = {
        x1: winWidth * .9,
        y1: winHeight * .075,
        x2: m0.x - (winWidth * .2),
        y2: winHeight * .07
    };
    var L1 = {
        x: winWidth * .7,
        y: winHeight * .071
    };
    var M2 = {
        x: (winWidth * .35),
        y: L1.y
    };
    var L2 = {
        x: winWidth * .2,
        y: L1.y
    };
    var Q2 = {
        x1: winWidth * .05,
        y1: winHeight * .07,
        x2: winWidth * .05,
        y2: winHeight * .2
    };
    var L3 = {
        x: Q2.x2,
        y: winHeight * 1
    };

    /* Dynamic position for VDO Title */
    var spaceTextDiff = L1.x - M2.x;
    if (spaceTextDiff > imageDimension.width + 40) {
        var _diff = spaceTextDiff - (imageDimension.width + 40);
        M2.x += _diff / 2;
        L1.x -= _diff / 2;
    }

    svg2Conf = { m0, Q1, L1, Q2, L3 };
    var pathData =  "M "+ m0.x+" "+ m0.y+" "+
                    "Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y2+" "+
                    "L "+ L1.x+" "+ L1.y+" "+
                    "M "+ M2.x+" "+ M2.y+" "+
                    "L "+ L2.x+" "+ L2.y+" "+
                    "Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    "L "+ L3.x+" "+ L3.y+" ";
    draw.path(pathData).stroke({ width: stroke, color: 'black' });

    var image = draw.image('images/vdo_teaser.png').loaded(function(loader) {
        var diff = L1.x - M2.x;
        var startAtX = M2.x + (diff / 2) - (imageDimension.width / 2);
        var startAtY = Q1.y1 - (imageDimension.height / 2);

        this.size(imageDimension.width, imageDimension.height).move(startAtX, startAtY);
    });
}

function initSVGVdo() {
    if (SVG.supported) {
        if ($(window).width() > 770) {
            initSVGVdo_desktop();
        } else {
            initSVGVdo_mobile();
        }
        svg2 = new Vivus($('#svg-vdo svg').toArray()[0], svg2Props);
    }
}
/* END: VDO Section */





/* Prize and Rules section */
function initSVGRule_desktop() {
    var draw = SVG('svg-rule');

    var M  ={
        x: winWidth * .0908,
        y: -2
    };
    var Q1 = {
        x1: winWidth * .14,
        y1: winHeight * .1,
        x2: winWidth * .25,
        y2: winHeight * .11
    };
    var L1 = {
      x: winWidth * .75,
      y: Q1.y2
    };
    var Q2 = {
        x1: winWidth * .96,
        y1: winHeight * .1,
        x2: winWidth * .95,
        y2: winHeight * .65
    };
    var Q3 = {
        x1: winWidth * .95,
        y1: winHeight * .9,
        x2: winWidth * .8,
        y2: winHeight * 1.005
    };

    svg3Conf = { M, Q1, L1, Q2, Q3 };
    var pathData =  " M "+ M.x+" "+ M.y+
                    " Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y2+" "+
                    " L "+ L1.x+" "+ L1.y+" "+
                    " Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    " Q "+ Q3.x1+" "+ Q3.y1+" "+ Q3.x2+" "+ Q3.y2+" ";

    draw.path(pathData).stroke({ width: 6, color: 'black' });
    /* Justify Title to center */
    $('.page-rules .page-title').css('left', (winWidth * .5) - ($('.page-rules .page-title').width() / 2));
    $('.page-rules .page-title').css('top', L1.y - ($('.page-rules .page-title').height() / 2));
}

function initSVGRule_mobile() {
    var draw = SVG('svg-rule');
    var stroke = 6;
    if (winWidth <= 500) {
        stroke = 3.5;
    } else if (winWidth <= 550) {
        stroke = 5;
    } else if (winWidth <= 600) {
        stroke = 5;
    } else if (winWidth <= 650) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    }  else if (winWidth <= 780) {
        stroke = 5.8;
    }

    var M  ={
        x: svg2Conf.L3.x,
        y: -2
    };
    var Q1 = {
        x1: winWidth * .05,
        y1: winHeight * .1,
        x2: winWidth * .2,
        y2: winHeight * .11
    };
    var L1 = {
      x: winWidth * .7,
      y: Q1.y2
    };
    var Q2 = {
        x1: winWidth * .96,
        y1: winHeight * .1,
        x2: winWidth * .95,
        y2: Q1.y2 + (winHeight * .2)
    };
    var L2 = {
      x: Q2.x2,
      y: winHeight * 1.05
    };

    svg3Conf = { M, Q1, L1, Q2, L2 };
    var pathData =  " M "+ M.x+" "+ M.y+
                    " Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y2+" "+
                    " L "+ L1.x+" "+ L1.y+" "+
                    " Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    " L "+ L2.x+" "+ L2.y+" ";

    draw.path(pathData).stroke({ width: stroke, color: 'black' });
    /* Justify Title to center */
    // $('.page-rules .page-title').css('left', (winWidth * .5) - ($('.page-rules .page-title').width() / 2));
    $('.page-rules .page-title').css('top', L1.y - 16);
}

function initSVGRule() {
    if (SVG.supported) {
        if ($(window).width() > 770) {
            initSVGRule_desktop();
        } else {
            initSVGRule_mobile();
        }
        $('.page-rules .page-title').show();
        svg3 = new Vivus($('#svg-rule svg').toArray()[0], svg3Props);
    }
}
/* END: Prize and Rules section */




/* Announce section */
function initSVGAnnounce_desktop() {
    var draw = SVG('svg-announce');

    var M  ={
        x: winWidth * .81135,
        y: -2
    };
    var Q1 = {
        x1: winWidth * .7557,
        y1: winHeight * .035,
        x2: winWidth * .7,
        y2: winHeight * .04
    };
    var L1 = {
      x: winWidth * .28,
      y: Q1.y2
    };
    var Q2 = {
        x1: winWidth * .08,
        y1: winHeight * .06,
        x2: winWidth * .085,
        y2: winHeight * .4
    };
    var L2 = {
      x: Q2.x2,
      y: winHeight * 1.01
    };

    svg4Conf = { M, Q1, L1, Q2, L2 };
    var pathData =  " M "+ M.x+" "+ M.y+
                    " Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y2+" "+
                    " L "+ L1.x+" "+ L1.y+" "+
                    " Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    " L "+ L2.x+" "+ L2.y+" ";

    draw.path(pathData).stroke({ width: 6, color: 'black' });
    /* Justify Title to center */
    $('.page-announce .page-title').css('left', (winWidth * .5) - ($('.page-announce .page-title').width() / 2));
    $('.page-announce .page-title').css('top', L1.y - ($('.page-announce .page-title').height() / 2));
}

function initSVGAnnounce_mobile() {
    var draw = SVG('svg-announce');
    var stroke = 6;
    if (winWidth <= 500) {
        stroke = 3.5;
    } else if (winWidth <= 550) {
        stroke = 5;
    } else if (winWidth <= 600) {
        stroke = 5;
    } else if (winWidth <= 650) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    } else if (winWidth <= 700) {
        stroke = 5;
    }  else if (winWidth <= 780) {
        stroke = 5.8;
    }

    var M = {
        x: svg3Conf.L2.x,
        y: 0
    };
    var Q1 = {
        x1: winWidth * .95,
        y1: winHeight * .077,
        x2: M.x - (winWidth * .15),
        y2: winHeight * .08
    };
    var L1 = {
      x: winWidth * .28,
      y: Q1.y2
    };
    var Q2 = {
        x1: winWidth * .04,
        y1: winHeight * .07,
        x2: winWidth * .05,
        y2: (L1.y) + winHeight * .15
    };
    var L2 = {
      x: Q2.x2,
      y: winHeight * 1.0
    };

    svg4Conf = { M, Q1, L1, Q2, L2 };
    var pathData =  " M "+ M.x+" "+ M.y+
                    " Q "+ Q1.x1+" "+ Q1.y1+" "+ Q1.x2+" "+ Q1.y2+" "+
                    " L "+ L1.x+" "+ L1.y+" "+
                    " Q "+ Q2.x1+" "+ Q2.y1+" "+ Q2.x2+" "+ Q2.y2+" "+
                    " L "+ L2.x+" "+ L2.y+" ";

    draw.path(pathData).stroke({ width: stroke, color: 'black' });
    /* Justify Title to center */
    $('.page-announce .page-title').css('top', L1.y - ($('.page-announce .page-title').height() / 2));
}

function initSVGAnnounce() {
    if (SVG.supported) {
        if ($(window).width() > 770) {
            initSVGAnnounce_desktop();
        } else {
            initSVGAnnounce_mobile();
        }
        $('.page-announce .page-title').show();
        svg4 = new Vivus($('#svg-announce svg').toArray()[0], svg4Props);
    }
}
/* END: Announce section */
