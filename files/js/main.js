
var md = new MobileDetect(window.navigator.userAgent);
if (md.phone()) {
	//document.getElementById("viewport").setAttribute("content", "width=640, user-scalable=no");
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: (target.offset().top - 52)
				}, 1000, "easeInOutExpo");
				return false;
			}
		}
	});

	$('.js-scroll-trigger').click(function() {
		$('.navbar-collapse').collapse('hide');
	});

	$('body').scrollspy({
		target: '#mainNav',
		offset: 52
	});

} else {
	
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: (target.offset().top - 84)
				}, 1000, "easeInOutExpo");
				return false;
			}
		}
	});

	$('.js-scroll-trigger').click(function() {
		$('.navbar-collapse').collapse('hide');
	});

	$('body').scrollspy({
		target: '#mainNav',
		offset: 85
	});
	
}


window.sr = ScrollReveal({ reset: true });
var heading = {
    origin   : "top",
    distance : "20px",
    duration : 1500,
    scale    : 1,
    opacity  : 0,
}
var detail = {
    origin   : "bottom",
    duration : 1000,
    scale    : 1,
}
var table = {
    origin   : "bottom",
    distance : "0",
    duration : 1000,
    scale    : 1,
    opacity  : 0,
}
var carousel = {
    origin   : "bottom",
    duration : 1000,
    opacity  : 0,
    scale    : 1,
}
var images = {
	origin   : "bottom",
    duration : 1000,
    scale    : 0,
}
var imagesleft = {
	origin   : "left",
	distance : "300px",
    duration : 1000,
    scale    : 1,
    opacity  : 0,
}


sr.reveal("h2", heading)
sr.reveal(".owl-carousel-caption", detail)
sr.reveal(".about img.motoGP-logo", images)
sr.reveal(".result-motogp img", imagesleft)
sr.reveal("#activity .table", table)
sr.reveal("#result-motogp .table", table)
sr.reveal(".owl-carousel", carousel)


$('#owl-carousel-banner').owlCarousel({
	items: 1,
	margin: 0,
	loop: true,
	lazyLoad: true,
	center: true,
	dots: false,
	nav: true,
	autoplay: true,
        autoplayTimeout: 3000,
        autoHeight: true,
	navText: ["<i class='icon-prev'></i>","<i class='icon-next'></i>"],
});


$('#owl-carousel').owlCarousel({
	items: 1,
	margin: 10,
	loop: true,
	dots: true,
	lazyLoad: true,
	center: true,
	responsive: {
		576: {
			items: 2,
			margin: 20,
			nav: true,
			dots: false,
			navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
		},
		1200: {
			items: 4,
			margin: 25,
			nav: true,
			dots: false,
			navText: ["<i class='arrow-left'></i>","<i class='arrow-right'></i>"],
		}
	}
});


$('[data-fancybox]').fancybox({
    youtube : {
        controls : 0,
        showinfo : 0
    }
});


$(".contentScrollbar").mCustomScrollbar({
    theme:"light-thick"
});